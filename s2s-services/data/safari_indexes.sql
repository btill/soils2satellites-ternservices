create index site_location_point_point_ix on site_location_point ( point );
create index site_location_name_ix on site_location(site_location_name );
create index site_location_id_ix on site_location_visit ( site_location_id ); 
create index site_location_visit_id_ix on basal_area (site_location_visit_id ) ;
create index site_location_visit_id_ix1 on structural_summary (site_location_visit_id ) ;
create index site_location_visit_id_ix2 on soil_characterisation (site_location_visit_id ) ;
create index site_location_visit_id_ix3 on soil_sample (site_location_visit_id ) ;
create index site_location_visit_id_ix4 on point_intercept_with_herb_id_added (site_location_visit_id ) ;
create index site_location_visit_id_ix5 on genetic_vouchers (site_location_visit_id ) ;
create index site_location_visit_id_ix6 on lai (site_location_visit_id ) ;
create index site_location_visit_id_ix7 on soil_bulk_density (study_location_visit_id ) ;
create index site_location_visit_id_ix8 on soil_structure (site_location_visit_id ) ;



       