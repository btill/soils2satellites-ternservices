package au.org.ecoinformatics.s2s.converter.studylocationvisit;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.VegVoucher;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitVoucher;
import au.org.ecoinformatics.s2s.util.ConvertUtils;

@Component
public class StudyLocationVisitVoucherConverter implements Converter<StudyLocationVisitVoucher, VegVoucher> {

	@Override
	public StudyLocationVisitVoucher convert(VegVoucher value) {
		return convert(new StudyLocationVisitVoucher(), value);
	}

	@Override
	public StudyLocationVisitVoucher convert(StudyLocationVisitVoucher initialValue, VegVoucher value) {
		initialValue.setFieldName(value.getFieldName());
		initialValue.setHerbariumDetermination((String) ConvertUtils.getProperty(value, "herbariumDetermination.herbariumDetermination"));
		initialValue.setStudyLocationVisitId(Integer.parseInt((String) ConvertUtils.getProperty(value, "siteLocationVisit.siteLocationVisitId")));
		initialValue.setVegBarcode(value.getVegBarcode());
		return initialValue;
	}

}
