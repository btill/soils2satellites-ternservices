package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_ibra database table.
 * 
 */
@Entity
@Table(name="lut_ibra")
public class LutIbra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="bioregion_name", unique=true, nullable=false, length=2147483647)
	private String bioregionName;

	@Column(name="bioregion_description", length=2147483647)
	private String bioregionDescription;

	//bi-directional many-to-one association to SiteLocation
	@OneToMany(mappedBy="lutIbra")
	private Set<SiteLocation> siteLocations;

	public LutIbra() {
	}

	public String getBioregionName() {
		return this.bioregionName;
	}

	public void setBioregionName(String bioregionName) {
		this.bioregionName = bioregionName;
	}

	public String getBioregionDescription() {
		return this.bioregionDescription;
	}

	public void setBioregionDescription(String bioregionDescription) {
		this.bioregionDescription = bioregionDescription;
	}

	public Set<SiteLocation> getSiteLocations() {
		return this.siteLocations;
	}

	public void setSiteLocations(Set<SiteLocation> siteLocations) {
		this.siteLocations = siteLocations;
	}

}