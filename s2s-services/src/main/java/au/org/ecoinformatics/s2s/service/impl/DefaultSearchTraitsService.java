package au.org.ecoinformatics.s2s.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.search.SearchTraitDataService;
import au.org.ecoinformatics.s2s.service.SearchTraitsService;

@Service("searchTraitsService")
public class DefaultSearchTraitsService implements SearchTraitsService {

	@Autowired
	private SearchTraitDataService searchTraitDataService;
	
	@Override
	public Set<?> getSearchTraitData(SearchTrait searchTrait) {
		return this.searchTraitDataService.getSearchTraitData(searchTrait);
	}

	@Override
	public List<SearchTrait> getSearchTraits() {
		return SearchTrait.asList();
	}
	
}
