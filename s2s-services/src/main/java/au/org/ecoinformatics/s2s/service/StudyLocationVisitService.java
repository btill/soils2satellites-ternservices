package au.org.ecoinformatics.s2s.service;

import java.util.List;

import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnits;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSpecies;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitVoucher;

public interface StudyLocationVisitService {

	List<StudyLocationVisit> getStudyLocationVisits(Integer studyLocationId);
	
	StudyLocationVisit getStudyLocationVisit(Integer studyLocationVisitId);

	StudyLocationVisit getStudyLocationVisitSamplingUnitDetails(Integer studyLocationVisitId);
	
	StudyLocationVisit getFirstStudyLocationVisit(Integer siteLocationId);
	
	StudyLocationVisit getLastStudyLocationVisit(Integer siteLocationId);
	
	StudyLocationVisitSamplingUnits getStudyLocationVisitSamplingUnits(Integer studyLocationVisitId);
	
	List<StudyLocationVisitVoucher> getStudyLocationVisitVouchers(Integer studyLocationVisitId);
	
	StudyLocationVisitSpecies getStudyLocationVisitSpecies(Integer studyLocationVisitId);

}