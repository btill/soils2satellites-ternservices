package au.org.ecoinformatics.s2s.converter.samplingunit;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.KeyedConverter;
import au.org.ecoinformatics.s2s.json.SoilBulkDensity;

@Component
public class SoilBulkDensityConverter implements KeyedConverter<SoilBulkDensity, au.org.ecoinformatics.s2s.domain.SoilBulkDensity> {
	
	@Override
	public SoilBulkDensity convert(au.org.ecoinformatics.s2s.domain.SoilBulkDensity value) {
		return convert(new SoilBulkDensity(), value);
	}

	@Override
	public SoilBulkDensity convert(SoilBulkDensity initialValue, au.org.ecoinformatics.s2s.domain.SoilBulkDensity value) {
		initialValue.setFineEarthBulkDensity(value.getFineEarthBulkDensity());
		initialValue.setFineEarthVolume(value.getFineEarthVolume());
		initialValue.setFineEarthWeight(value.getFineEarthWeight());
		initialValue.setFineEarthWeightInBag(value.getFineEarthWeightInBag());
		initialValue.setGravelBulkDensity(value.getGravelBulkDensity());
		initialValue.setGravelVolume(value.getGravelVolume());
		initialValue.setGravelWeight(value.getGravelWeight());
		initialValue.setId(value.getId());
		initialValue.setOvenDriedWeightInBag(value.getOvenDriedWeightInBag());
		initialValue.setPaperBagWeight(value.getPaperBagWeight());
		initialValue.setRingVolume(value.getRingVolume());
		initialValue.setRingWeight(value.getRingWeight());
		initialValue.setSampleId(value.getSampleId());
		return initialValue;
	}

	@Override
	public Class<SoilBulkDensity> getOutputClass() {
		return SoilBulkDensity.class;
	}

	@Override
	public Class<au.org.ecoinformatics.s2s.domain.SoilBulkDensity> getInputClass() {
		return au.org.ecoinformatics.s2s.domain.SoilBulkDensity.class;
	}

}
