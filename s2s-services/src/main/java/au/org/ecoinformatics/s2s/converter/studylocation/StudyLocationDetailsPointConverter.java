package au.org.ecoinformatics.s2s.converter.studylocation;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.StudyLocationDetails;
import au.org.ecoinformatics.s2s.json.StudyLocationPoint;

@Component
public class StudyLocationDetailsPointConverter implements Converter<StudyLocationDetails, StudyLocationPoint> {

	@Override
	public StudyLocationDetails convert(StudyLocationPoint value) {
		return convert(new StudyLocationDetails(), value);
	}

	@Override
	public StudyLocationDetails convert(StudyLocationDetails details, StudyLocationPoint point) {
		details.setEasting(point.getEasting());
		details.setLatitude(point.getLatitude());
		details.setLongitude(point.getLongitude());
		details.setNorthing(point.getNorthing());
		details.setMgaZone(point.getZone());
		return details;
	}

}
