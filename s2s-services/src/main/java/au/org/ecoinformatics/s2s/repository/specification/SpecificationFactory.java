package au.org.ecoinformatics.s2s.repository.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class SpecificationFactory {
	
	public <T> Specification<T> like(Class<T> type, final String value, final String ...  fields) { 
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                String likePattern = getLikePattern(value);  
                Expression<String> expression = getExpression(root, fields);
                return cb.like(expression, likePattern);
            }
           
            private String getLikePattern(final String searchTerm) {
                StringBuilder pattern = new StringBuilder();
                pattern.append("%").append(searchTerm).append("%");
                return pattern.toString();
            }
        };
    }
	
	public <T, K> Specification<T> equals(Class<T> type, final K value, final String ...  fields) { 
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            	Expression<K> expression = getExpression(root, fields);
                return cb.equal(expression, value);
            }
        };
    }
	
	public <T, K extends Comparable<? super K>> Specification<T> between(Class<T> type, final K before, final K after, final String ...  fields) { 
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            	Expression<K> expression = getExpression(root, fields);
                return cb.between(expression, before, after);
            }
        };
    }
	
	public <T, K extends Comparable<? super K>> Specification<T> greaterThanOrEqualTo(Class<T> type, final K value, final String ...  fields) { 
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            	Expression<K> expression = getExpression(root, fields);
                return cb.greaterThanOrEqualTo(expression, value);
            }
        };
    }
	
	public <T, K extends Comparable<? super K>> Specification<T> lessThanOrEqualTo(Class<T> type, final K value, final String ...  fields) { 
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            	Expression<K> expression = getExpression(root, fields);
                return cb.lessThanOrEqualTo(expression, value);
            }
        };
    }
	
	private <T> Expression<T> getExpression(Root<? extends Object> root, final String ...fields) {
		Assert.notEmpty(fields);
		int lastIndex = fields.length - 1;
		Path<? extends Object> path = root;
		for(int i = 0; i < lastIndex; i++) {
			path = path.get(fields[i]);
		}
		return path.get(fields[lastIndex]);
	}
	
}
