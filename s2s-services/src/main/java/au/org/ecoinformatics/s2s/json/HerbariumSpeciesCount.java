package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class HerbariumSpeciesCount implements Serializable {
	
	private static final long serialVersionUID = -7331963191990039358L;
	
	private String herbariumDetermination;
	private Long speciesCount = 0L;
	
	public String getHerbariumDetermination() {
		return herbariumDetermination;
	}
	public void setHerbariumDetermination(String herbariumDetermination) {
		this.herbariumDetermination = herbariumDetermination;
	}
	public Long getSpeciesCount() {
		return speciesCount;
	}
	public void setSpeciesCount(Long speciesCount) {
		this.speciesCount = speciesCount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((herbariumDetermination == null) ? 0
						: herbariumDetermination.hashCode());
		result = prime * result
				+ ((speciesCount == null) ? 0 : speciesCount.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HerbariumSpeciesCount other = (HerbariumSpeciesCount) obj;
		if (herbariumDetermination == null) {
			if (other.herbariumDetermination != null)
				return false;
		} else if (!herbariumDetermination.equals(other.herbariumDetermination))
			return false;
		if (speciesCount == null) {
			if (other.speciesCount != null)
				return false;
		} else if (!speciesCount.equals(other.speciesCount))
			return false;
		return true;
	}
	
}
