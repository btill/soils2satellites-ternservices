package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_basal_point database table.
 * 
 */
@Entity
@Table(name="lut_basal_point")
public class LutBasalPoint implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="point_id", unique=true, nullable=false, length=2147483647)
	private String pointId;

	@Column(name="distance_from_sw_corner", nullable=false, length=2147483647)
	private String distanceFromSwCorner;

	@Column(length=2147483647)
	private String notes;

	@Column(name="point_name", nullable=false, length=2147483647)
	private String pointName;

	//bi-directional many-to-one association to BasalArea
	@OneToMany(mappedBy="lutBasalPoint")
	private Set<BasalArea> basalAreas;

	public LutBasalPoint() {
	}

	public String getPointId() {
		return this.pointId;
	}

	public void setPointId(String pointId) {
		this.pointId = pointId;
	}

	public String getDistanceFromSwCorner() {
		return this.distanceFromSwCorner;
	}

	public void setDistanceFromSwCorner(String distanceFromSwCorner) {
		this.distanceFromSwCorner = distanceFromSwCorner;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPointName() {
		return this.pointName;
	}

	public void setPointName(String pointName) {
		this.pointName = pointName;
	}

	public Set<BasalArea> getBasalAreas() {
		return this.basalAreas;
	}

	public void setBasalAreas(Set<BasalArea> basalAreas) {
		this.basalAreas = basalAreas;
	}

}