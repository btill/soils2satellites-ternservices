package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class DistinctSpeciesCount implements Serializable {

	private static final long serialVersionUID = -6762324125025113663L;
	
	private Integer siteLocationId;
	private String siteLocationName;
	private Long distinctSpeciesCount = 0L;
	
	public Integer getSiteLocationId() {
		return siteLocationId;
	}

	public void setSiteLocationId(Integer siteLocationId) {
		this.siteLocationId = siteLocationId;
	}

	public String getSiteLocationName() {
		return siteLocationName;
	}

	public void setSiteLocationName(String siteLocationName) {
		this.siteLocationName = siteLocationName;
	}

	public Long getDistinctSpeciesCount() {
		return distinctSpeciesCount;
	}

	public void setDistinctSpeciesCount(Long distinctSpeciesCount) {
		this.distinctSpeciesCount = distinctSpeciesCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((distinctSpeciesCount == null) ? 0 : distinctSpeciesCount
						.hashCode());
		result = prime * result
				+ ((siteLocationName == null) ? 0 : siteLocationName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DistinctSpeciesCount other = (DistinctSpeciesCount) obj;
		if (distinctSpeciesCount == null) {
			if (other.distinctSpeciesCount != null)
				return false;
		} else if (!distinctSpeciesCount.equals(other.distinctSpeciesCount))
			return false;
		if (siteLocationName == null) {
			if (other.siteLocationName != null)
				return false;
		} else if (!siteLocationName.equals(other.siteLocationName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DistinctSpeciesCount [siteLocationId=" + siteLocationId
				+ ", siteLocationName=" + siteLocationName
				+ ", distinctSpeciesCount=" + distinctSpeciesCount + "]";
	}

}
