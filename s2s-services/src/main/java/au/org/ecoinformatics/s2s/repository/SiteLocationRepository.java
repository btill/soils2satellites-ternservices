package au.org.ecoinformatics.s2s.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import au.org.ecoinformatics.s2s.domain.SiteLocation;

@Repository("siteLocationRepository")
public interface SiteLocationRepository extends JpaRepository<SiteLocation, Integer> {

	@Query(value="select a from SiteLocation a where a.siteLocationId = ?1")
	List<SiteLocation> findSiteLocations(Integer siteLocationId);
	
	@Query(value="select a.siteLocationId, a.siteLocationName from SiteLocation a ")
	List<Object[]> findSiteLocationNameList();
	
	@Query(value="select a.siteLocationId from SiteLocation a")
	List<Integer> findSiteLocationIdList();
	
	@Query(value="select a.siteLocationId from SiteLocation a where a.siteLocationName = ?1")
	List<Integer> findSiteLocationId(String identifier);
}
