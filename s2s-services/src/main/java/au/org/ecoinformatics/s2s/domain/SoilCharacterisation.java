package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the soil_characterisation database table.
 * 
 */
@Entity
@Table(name="soil_characterisation")
public class SoilCharacterisation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(name="coarse_frags_lithology", length=2147483647)
	private String coarseFragsLithology;

	@Column(name="collected_by", length=2147483647)
	private String collectedBy;

	@Column(name="colour_when_dry", length=2147483647)
	private String colourWhenDry;

	@Column(name="colour_when_moist", length=2147483647)
	private String colourWhenMoist;

	@Column(length=2147483647)
	private String comments;

	private Double ec;

	@Column(length=2147483647)
	private String horizon;

	@Column(name="layer_number", length=2147483647)
	private String layerNumber;

	@Column(name="lower_depth")
	private Double lowerDepth;

	@Column(name="next_size_1")
	private Integer nextSize1;

	@Column(name="next_size_2")
	private Integer nextSize2;

	@Column(name="next_size_type_1", length=2147483647)
	private String nextSizeType1;

	@Column(name="next_size_type_2", length=2147483647)
	private String nextSizeType2;

	private Double ph;

	@Column(name="smallest_size_1")
	private Integer smallestSize1;

	@Column(name="smallest_size_2")
	private Integer smallestSize2;

	@Column(name="smallest_size_type_1", length=2147483647)
	private String smallestSizeType1;

	@Column(name="smallest_size_type_2", length=2147483647)
	private String smallestSizeType2;

	@Column(name="upper_depth")
	private Double upperDepth;

	//bi-directional many-to-one association to LutCoarseFragAbund
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="coarse_frags_abundance")
	private LutCoarseFragAbund lutCoarseFragAbund;

	//bi-directional many-to-one association to LutCoarseFragShape
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="coarse_frags_shape")
	private LutCoarseFragShape lutCoarseFragShape;

	//bi-directional many-to-one association to LutCoarseFragSize
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="coarse_frags_size")
	private LutCoarseFragSize lutCoarseFragSize;

	//bi-directional many-to-one association to LutEffervescence
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="effervescence")
	private LutEffervescence lutEffervescence;

	//bi-directional many-to-one association to LutMottleAbund
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="mottles_abundance")
	private LutMottleAbund lutMottleAbund;

	//bi-directional many-to-one association to LutMottleColour
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="mottles_colour")
	private LutMottleColour lutMottleColour;

	//bi-directional many-to-one association to LutMottleSize
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="mottles_size")
	private LutMottleSize lutMottleSize;

	//bi-directional many-to-one association to LutPedalityFabric
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pedality_fabric", nullable=false)
	private LutPedalityFabric lutPedalityFabric;

	//bi-directional many-to-one association to LutPedalityGrade
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pedality_grade", nullable=false)
	private LutPedalityGrade lutPedalityGrade;

	//bi-directional many-to-one association to LutPedalityType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pedality_type")
	private LutPedalityType lutPedalityType;

	//bi-directional many-to-one association to LutSegAbundance
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="segregations_abundance")
	private LutSegAbundance lutSegAbundance;

	//bi-directional many-to-one association to LutSegForm
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="segregations_form")
	private LutSegForm lutSegForm;

	//bi-directional many-to-one association to LutSegNature
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="segregations_nature")
	private LutSegNature lutSegNature;

	//bi-directional many-to-one association to LutSegSize
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="segregations_size")
	private LutSegSize lutSegSize;

	//bi-directional many-to-one association to LutSoilTexGrade
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="texture_grade")
	private LutSoilTexGrade lutSoilTexGrade;

	//bi-directional many-to-one association to LutSoilTexMod
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="texture_modifier")
	private LutSoilTexMod lutSoilTexMod;

	//bi-directional many-to-one association to LutSoilTexQual
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="texture_qualifier")
	private LutSoilTexQual lutSoilTexQual;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public SoilCharacterisation() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCoarseFragsLithology() {
		return this.coarseFragsLithology;
	}

	public void setCoarseFragsLithology(String coarseFragsLithology) {
		this.coarseFragsLithology = coarseFragsLithology;
	}

	public String getCollectedBy() {
		return this.collectedBy;
	}

	public void setCollectedBy(String collectedBy) {
		this.collectedBy = collectedBy;
	}

	public String getColourWhenDry() {
		return this.colourWhenDry;
	}

	public void setColourWhenDry(String colourWhenDry) {
		this.colourWhenDry = colourWhenDry;
	}

	public String getColourWhenMoist() {
		return this.colourWhenMoist;
	}

	public void setColourWhenMoist(String colourWhenMoist) {
		this.colourWhenMoist = colourWhenMoist;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Double getEc() {
		return this.ec;
	}

	public void setEc(Double ec) {
		this.ec = ec;
	}

	public String getHorizon() {
		return this.horizon;
	}

	public void setHorizon(String horizon) {
		this.horizon = horizon;
	}

	public String getLayerNumber() {
		return this.layerNumber;
	}

	public void setLayerNumber(String layerNumber) {
		this.layerNumber = layerNumber;
	}

	public Double getLowerDepth() {
		return this.lowerDepth;
	}

	public void setLowerDepth(Double lowerDepth) {
		this.lowerDepth = lowerDepth;
	}

	public Integer getNextSize1() {
		return this.nextSize1;
	}

	public void setNextSize1(Integer nextSize1) {
		this.nextSize1 = nextSize1;
	}

	public Integer getNextSize2() {
		return this.nextSize2;
	}

	public void setNextSize2(Integer nextSize2) {
		this.nextSize2 = nextSize2;
	}

	public String getNextSizeType1() {
		return this.nextSizeType1;
	}

	public void setNextSizeType1(String nextSizeType1) {
		this.nextSizeType1 = nextSizeType1;
	}

	public String getNextSizeType2() {
		return this.nextSizeType2;
	}

	public void setNextSizeType2(String nextSizeType2) {
		this.nextSizeType2 = nextSizeType2;
	}

	public Double getPh() {
		return this.ph;
	}

	public void setPh(Double ph) {
		this.ph = ph;
	}

	public Integer getSmallestSize1() {
		return this.smallestSize1;
	}

	public void setSmallestSize1(Integer smallestSize1) {
		this.smallestSize1 = smallestSize1;
	}

	public Integer getSmallestSize2() {
		return this.smallestSize2;
	}

	public void setSmallestSize2(Integer smallestSize2) {
		this.smallestSize2 = smallestSize2;
	}

	public String getSmallestSizeType1() {
		return this.smallestSizeType1;
	}

	public void setSmallestSizeType1(String smallestSizeType1) {
		this.smallestSizeType1 = smallestSizeType1;
	}

	public String getSmallestSizeType2() {
		return this.smallestSizeType2;
	}

	public void setSmallestSizeType2(String smallestSizeType2) {
		this.smallestSizeType2 = smallestSizeType2;
	}

	public Double getUpperDepth() {
		return this.upperDepth;
	}

	public void setUpperDepth(Double upperDepth) {
		this.upperDepth = upperDepth;
	}

	public LutCoarseFragAbund getLutCoarseFragAbund() {
		return this.lutCoarseFragAbund;
	}

	public void setLutCoarseFragAbund(LutCoarseFragAbund lutCoarseFragAbund) {
		this.lutCoarseFragAbund = lutCoarseFragAbund;
	}

	public LutCoarseFragShape getLutCoarseFragShape() {
		return this.lutCoarseFragShape;
	}

	public void setLutCoarseFragShape(LutCoarseFragShape lutCoarseFragShape) {
		this.lutCoarseFragShape = lutCoarseFragShape;
	}

	public LutCoarseFragSize getLutCoarseFragSize() {
		return this.lutCoarseFragSize;
	}

	public void setLutCoarseFragSize(LutCoarseFragSize lutCoarseFragSize) {
		this.lutCoarseFragSize = lutCoarseFragSize;
	}

	public LutEffervescence getLutEffervescence() {
		return this.lutEffervescence;
	}

	public void setLutEffervescence(LutEffervescence lutEffervescence) {
		this.lutEffervescence = lutEffervescence;
	}

	public LutMottleAbund getLutMottleAbund() {
		return this.lutMottleAbund;
	}

	public void setLutMottleAbund(LutMottleAbund lutMottleAbund) {
		this.lutMottleAbund = lutMottleAbund;
	}

	public LutMottleColour getLutMottleColour() {
		return this.lutMottleColour;
	}

	public void setLutMottleColour(LutMottleColour lutMottleColour) {
		this.lutMottleColour = lutMottleColour;
	}

	public LutMottleSize getLutMottleSize() {
		return this.lutMottleSize;
	}

	public void setLutMottleSize(LutMottleSize lutMottleSize) {
		this.lutMottleSize = lutMottleSize;
	}

	public LutPedalityFabric getLutPedalityFabric() {
		return this.lutPedalityFabric;
	}

	public void setLutPedalityFabric(LutPedalityFabric lutPedalityFabric) {
		this.lutPedalityFabric = lutPedalityFabric;
	}

	public LutPedalityGrade getLutPedalityGrade() {
		return this.lutPedalityGrade;
	}

	public void setLutPedalityGrade(LutPedalityGrade lutPedalityGrade) {
		this.lutPedalityGrade = lutPedalityGrade;
	}

	public LutPedalityType getLutPedalityType() {
		return this.lutPedalityType;
	}

	public void setLutPedalityType(LutPedalityType lutPedalityType) {
		this.lutPedalityType = lutPedalityType;
	}

	public LutSegAbundance getLutSegAbundance() {
		return this.lutSegAbundance;
	}

	public void setLutSegAbundance(LutSegAbundance lutSegAbundance) {
		this.lutSegAbundance = lutSegAbundance;
	}

	public LutSegForm getLutSegForm() {
		return this.lutSegForm;
	}

	public void setLutSegForm(LutSegForm lutSegForm) {
		this.lutSegForm = lutSegForm;
	}

	public LutSegNature getLutSegNature() {
		return this.lutSegNature;
	}

	public void setLutSegNature(LutSegNature lutSegNature) {
		this.lutSegNature = lutSegNature;
	}

	public LutSegSize getLutSegSize() {
		return this.lutSegSize;
	}

	public void setLutSegSize(LutSegSize lutSegSize) {
		this.lutSegSize = lutSegSize;
	}

	public LutSoilTexGrade getLutSoilTexGrade() {
		return this.lutSoilTexGrade;
	}

	public void setLutSoilTexGrade(LutSoilTexGrade lutSoilTexGrade) {
		this.lutSoilTexGrade = lutSoilTexGrade;
	}

	public LutSoilTexMod getLutSoilTexMod() {
		return this.lutSoilTexMod;
	}

	public void setLutSoilTexMod(LutSoilTexMod lutSoilTexMod) {
		this.lutSoilTexMod = lutSoilTexMod;
	}

	public LutSoilTexQual getLutSoilTexQual() {
		return this.lutSoilTexQual;
	}

	public void setLutSoilTexQual(LutSoilTexQual lutSoilTexQual) {
		this.lutSoilTexQual = lutSoilTexQual;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}