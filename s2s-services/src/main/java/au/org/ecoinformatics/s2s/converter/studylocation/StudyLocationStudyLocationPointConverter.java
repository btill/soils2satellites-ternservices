package au.org.ecoinformatics.s2s.converter.studylocation;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.StudyLocation;
import au.org.ecoinformatics.s2s.json.StudyLocationPoint;

@Component
public class StudyLocationStudyLocationPointConverter implements Converter<StudyLocation, StudyLocationPoint> {

	@Override
	public StudyLocation convert(StudyLocationPoint point) {
		return convert(new StudyLocation(), point);
	}

	@Override
	public StudyLocation convert(StudyLocation studyLocation, StudyLocationPoint point) {
		studyLocation.setEasting(point.getEasting());
		studyLocation.setMgaZone(point.getZone());
		studyLocation.setNorthing(point.getNorthing());
		studyLocation.setLatitude(point.getLatitude());
		studyLocation.setLongitude(point.getLongitude());
		return studyLocation;
	}

}
