package au.org.ecoinformatics.s2s.web;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.org.ecoinformatics.s2s.json.DistinctSpeciesCount;
import au.org.ecoinformatics.s2s.json.HerbariumPointIntercept;
import au.org.ecoinformatics.s2s.json.HerbariumPointInterceptCount;
import au.org.ecoinformatics.s2s.json.HerbariumSpeciesCount;
import au.org.ecoinformatics.s2s.json.LandformElement;
import au.org.ecoinformatics.s2s.json.LandformElementTotal;
import au.org.ecoinformatics.s2s.service.StudyLocationService;
import au.org.ecoinformatics.s2s.service.VisualisationService;
import au.org.ecoinformatics.s2s.util.JsonUtilsBean;

@Controller(value="visualisationController")
public class VisualisationController {

	@Autowired
	private JsonUtilsBean jsonUtilsBean;
	@Autowired
	private StudyLocationService studyLocationService;
	@Autowired
	private VisualisationService visualisationService;

	@RequestMapping(value = "/getDistinctSpeciesCountList", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getDistinctSpeciesCountList(@RequestParam(value="studyLocations", required=true) final String[] studyLocations) throws IOException {
		List<Integer> studyLocationIdList = this.studyLocationService.getStudyLocationIdList(studyLocations);
		List<DistinctSpeciesCount> dscList = this.visualisationService.getDistinctSpeciesCountList(studyLocationIdList);
		return this.jsonUtilsBean.writeValueAsString(dscList);
	}
	
	@RequestMapping(value = "/getHerbariumPointIntercepts", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getHerbariumPointIntercepts(@RequestParam(value="studyLocation", required=true) final String studyLocation, 
			@RequestParam(value="visitStartDate", required=true) @DateTimeFormat(pattern="ddMMyyyy") final Date visitStartDate) throws IOException {
		Integer studyLocationId = this.studyLocationService.getStudyLocationId(studyLocation);
		List<HerbariumPointIntercept> hpiList = this.visualisationService.getHerbariumPointIntercepts(studyLocationId, visitStartDate);
		return this.jsonUtilsBean.writeValueAsString(hpiList);
	}
	
	@RequestMapping(value = "/getHerbariumPointInterceptCount", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getHerbariumPointInterceptCount() throws IOException {
		List<HerbariumPointInterceptCount> hpicList = this.visualisationService.getHerbariumPointInterceptCount();
		return this.jsonUtilsBean.writeValueAsString(hpicList);
	}
	
	@RequestMapping(value = "/getHerbariumSpeciesCount/{studyLocationVisitId}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getHerbariumSpeciesCount(@PathVariable Integer studyLocationVisitId) throws IOException {
		List<HerbariumSpeciesCount> hscList = this.visualisationService.getHerbariumSpeciesCount(studyLocationVisitId);
		return this.jsonUtilsBean.writeValueAsString(hscList);
	}
	
	@RequestMapping(value = "/getLandFormElements", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getLandFormElements(@RequestParam(value="studyLocations", required=true) final String[] studyLocations) throws IOException {
		List<Integer> studyLocationIdList = this.studyLocationService.getStudyLocationIdList(studyLocations);
		List<LandformElement> lfeList = this.visualisationService.getLandFormElements(studyLocationIdList);
		return this.jsonUtilsBean.writeValueAsString(lfeList);
	}
	
	@RequestMapping(value = "/getLandFormElementTotals", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getLandFormElementTotals(@RequestParam(value="studyLocations", required=true) final String[] studyLocations) throws IOException {
		List<Integer> studyLocationIdList = this.studyLocationService.getStudyLocationIdList(studyLocations);
		List<LandformElementTotal> lfetList = this.visualisationService.getLandFormElementTotals(studyLocationIdList);
		return this.jsonUtilsBean.writeValueAsString(lfetList);
	}
	
}
