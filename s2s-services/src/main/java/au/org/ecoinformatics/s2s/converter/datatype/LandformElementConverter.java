package au.org.ecoinformatics.s2s.converter.datatype;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.LandformElement;

@Component
public class LandformElementConverter implements Converter<LandformElement, Object[]> {

	@Override
	public LandformElement convert(Object[] value) {
		return convert(new LandformElement(), value);
	}

	@Override
	public LandformElement convert(LandformElement initialValue, Object[] value) {
		initialValue.setStudyLocationId((Integer) value[0]);
		initialValue.setStudyLocationName((String) value[1]);
		initialValue.setLandFormElement((String) value[2]);
		return initialValue;
	}

}
