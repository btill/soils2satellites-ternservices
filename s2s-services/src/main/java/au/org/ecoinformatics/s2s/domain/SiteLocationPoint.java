package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the site_location_point database table.
 * 
 */
@Entity
@Table(name="site_location_point")
public class SiteLocationPoint implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	private Double easting;
	private Double latitude;
	private Double longitude;
	
	private Double northing;

	@Column(length=2147483647)
	private String point;

	private Double threedcq;

	private Integer zone;

	//bi-directional many-to-one association to SiteLocation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_id", nullable=false)
	private SiteLocation siteLocation;

	public SiteLocationPoint() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getEasting() {
		return this.easting;
	}

	public void setEasting(Double easting) {
		this.easting = easting;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getNorthing() {
		return this.northing;
	}

	public void setNorthing(Double northing) {
		this.northing = northing;
	}

	public String getPoint() {
		return this.point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public Double getThreedcq() {
		return this.threedcq;
	}

	public void setThreedcq(Double threedcq) {
		this.threedcq = threedcq;
	}

	public Integer getZone() {
		return this.zone;
	}

	public void setZone(Integer zone) {
		this.zone = zone;
	}

	public SiteLocation getSiteLocation() {
		return this.siteLocation;
	}

	public void setSiteLocation(SiteLocation siteLocation) {
		this.siteLocation = siteLocation;
	}

	@Override
	public String toString() {
		return "SiteLocationPoint [id=" + id + ", easting=" + easting
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", northing=" + northing + ", point=" + point + ", threedcq="
				+ threedcq + ", zone=" + zone + ", siteLocation="
				+ siteLocation + "]";
	}

}