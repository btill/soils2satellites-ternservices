package au.org.ecoinformatics.s2s.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;

@Repository("siteLocationVisitRepository")
public interface SiteLocationVisitRepository extends JpaRepository<SiteLocationVisit, Integer> {

	@Query(value="SELECT slv " +
            	 "FROM SiteLocation sl, SiteLocationVisit slv " +
		         "WHERE sl.siteLocationId = slv.siteLocation.siteLocationId " +
		         "AND sl.siteLocationId = ?1 " +
		         "AND slv.visitStartDate = " +
		         "(SELECT MIN(slv1.visitStartDate) " +
		         " FROM SiteLocation sl1, SiteLocationVisit slv1 " +
		         " WHERE sl1.siteLocationId = slv1.siteLocation.siteLocationId " +
		         " AND sl1.siteLocationId = ?1 )")
	List<SiteLocationVisit> findFirstSiteLocationVisit(Integer siteLocationId);
	
	@Query(value="SELECT slv " +
	             "FROM SiteLocation sl, SiteLocationVisit slv " +
		         "WHERE sl.siteLocationId = slv.siteLocation.siteLocationId " +
		         "AND sl.siteLocationId = ?1 " +
		         "AND slv.visitStartDate = " +
		         "(SELECT MAX(slv1.visitStartDate) " +
		         " FROM SiteLocation sl1, SiteLocationVisit slv1 " +
		         " WHERE sl1.siteLocationId = slv1.siteLocation.siteLocationId " +
		         " AND sl1.siteLocationId = ?1 )")
	List<SiteLocationVisit> findLastSiteLocationVisit(Integer siteLocationId);
	
	@Query(value="select slv from SiteLocationVisit slv where slv.siteLocation.siteLocationId = ?1")
	List<SiteLocationVisit> findSiteLocationVisits(Integer siteLocationId);
	
	@Query(value="select distinct slv " +
	             "from SiteLocationVisit slv, HerbariumDetermination hd, VegVoucher vv " +
			     "where vv.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
	             "and hd.vegBarcode = vv.vegBarcode " +
	             "and hd.herbariumDetermination LIKE ?1")
	List<SiteLocationVisit> findSiteLocationVisitsHD(String herbariumDetermination);
}
