package au.org.ecoinformatics.s2s.service;

import java.util.List;

import au.org.ecoinformatics.s2s.json.StudyLocationPoint;

public interface StudyLocationPointService {

	StudyLocationPoint getStudyLocationMarkerPoint(Integer siteLocationId);

	List<StudyLocationPoint> getStudyLocationMarkers(String bboxString);

	List<StudyLocationPoint> getAllStudyLocationMarkers();

}