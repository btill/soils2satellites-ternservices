package au.org.ecoinformatics.s2s.converter.studylocationvisit;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.SamplingUnit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnits;

@Component
public class StudyLocationVisitSamplingUnitConverter implements Converter<StudyLocationVisitSamplingUnits, SiteLocationVisit> {

	@Override
	public StudyLocationVisitSamplingUnits convert(SiteLocationVisit value) {
		return convert(new StudyLocationVisitSamplingUnits(), value);
	}

	@Override
	public StudyLocationVisitSamplingUnits convert(StudyLocationVisitSamplingUnits initialValue, SiteLocationVisit value) {
		initialValue.setStudyLocationName(value.getSiteLocation().getSiteLocationName());
		initialValue.setStudyLocationVisitId(value.getSiteLocationVisitId());
		Set<SamplingUnit> suSet = new HashSet<SamplingUnit>();
		for(SamplingUnit su : SamplingUnit.values()) {
			if(su.hasSamplingUnitData(value)) {
				suSet.add(su);
			}
		}
		initialValue.setSamplingUnitTypes(suSet);
		return initialValue;
	}

}
