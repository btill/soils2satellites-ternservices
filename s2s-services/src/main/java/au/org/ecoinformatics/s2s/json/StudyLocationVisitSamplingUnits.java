package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocationVisitSamplingUnits implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer studyLocationVisitId;
	private String studyLocationName;
	private Set<SamplingUnit> samplingUnitTypes = new HashSet<SamplingUnit>();

	public StudyLocationVisitSamplingUnits() {}

	public Integer getStudyLocationVisitId() {
		return this.studyLocationVisitId;
	}

	public void setStudyLocationVisitId(Integer studyLocationVisitId) {
		this.studyLocationVisitId = studyLocationVisitId;
	}

	public String getStudyLocationName() {
		return studyLocationName;
	}

	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}

	public Set<SamplingUnit> getSamplingUnitTypes() {
		return samplingUnitTypes;
	}

	public void setSamplingUnitTypes(Set<SamplingUnit> samplingUnitTypes) {
		this.samplingUnitTypes = samplingUnitTypes;
	}

}