package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder(alphabetic=true)
public class StructuralSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String description;
	private String ground1Dominant;
	private String ground2Dominant;
	private String ground3Dominant;
	private Integer massFloweringEvent;
	private String mid1Dominant;
	private String mid2Dominant;
	private String mid3Dominant;
	private String phenologyComment;
	private String upper1Dominant;
	private String upper2Dominant;
	private String upper3Dominant;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getGround1Dominant() {
		return ground1Dominant;
	}
	public void setGround1Dominant(String ground1Dominant) {
		this.ground1Dominant = ground1Dominant;
	}
	public String getGround2Dominant() {
		return ground2Dominant;
	}
	public void setGround2Dominant(String ground2Dominant) {
		this.ground2Dominant = ground2Dominant;
	}
	public String getGround3Dominant() {
		return ground3Dominant;
	}
	public void setGround3Dominant(String ground3Dominant) {
		this.ground3Dominant = ground3Dominant;
	}
	public Integer getMassFloweringEvent() {
		return massFloweringEvent;
	}
	public void setMassFloweringEvent(Integer massFloweringEvent) {
		this.massFloweringEvent = massFloweringEvent;
	}
	public String getMid1Dominant() {
		return mid1Dominant;
	}
	public void setMid1Dominant(String mid1Dominant) {
		this.mid1Dominant = mid1Dominant;
	}
	public String getMid2Dominant() {
		return mid2Dominant;
	}
	public void setMid2Dominant(String mid2Dominant) {
		this.mid2Dominant = mid2Dominant;
	}
	public String getMid3Dominant() {
		return mid3Dominant;
	}
	public void setMid3Dominant(String mid3Dominant) {
		this.mid3Dominant = mid3Dominant;
	}
	public String getPhenologyComment() {
		return phenologyComment;
	}
	public void setPhenologyComment(String phenologyComment) {
		this.phenologyComment = phenologyComment;
	}
	public String getUpper1Dominant() {
		return upper1Dominant;
	}
	public void setUpper1Dominant(String upper1Dominant) {
		this.upper1Dominant = upper1Dominant;
	}
	public String getUpper2Dominant() {
		return upper2Dominant;
	}
	public void setUpper2Dominant(String upper2Dominant) {
		this.upper2Dominant = upper2Dominant;
	}
	public String getUpper3Dominant() {
		return upper3Dominant;
	}
	public void setUpper3Dominant(String upper3Dominant) {
		this.upper3Dominant = upper3Dominant;
	}

}