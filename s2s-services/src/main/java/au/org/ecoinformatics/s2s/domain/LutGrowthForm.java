package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_growth_form database table.
 * 
 */
@Entity
@Table(name="lut_growth_form")
public class LutGrowthForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="growth_form", unique=true, nullable=false, length=2147483647)
	private String growthForm;

	@Column(length=2147483647)
	private String definition;

	//bi-directional many-to-one association to PointIntercept
	@OneToMany(mappedBy="lutGrowthForm")
	private Set<PointIntercept> pointIntercepts;

	public LutGrowthForm() {
	}

	public String getGrowthForm() {
		return this.growthForm;
	}

	public void setGrowthForm(String growthForm) {
		this.growthForm = growthForm;
	}

	public String getDefinition() {
		return this.definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Set<PointIntercept> getPointIntercepts() {
		return this.pointIntercepts;
	}

	public void setPointIntercepts(Set<PointIntercept> pointIntercepts) {
		this.pointIntercepts = pointIntercepts;
	}

}