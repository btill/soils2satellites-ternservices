package au.org.ecoinformatics.s2s.converter.samplingunit;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.KeyedConverter;
import au.org.ecoinformatics.s2s.domain.SoilSubsiteObservation;
import au.org.ecoinformatics.s2s.json.SoilSamples;

@Component
public class SoilSamplesSoilSubiteObservationConverter implements KeyedConverter<SoilSamples, SoilSubsiteObservation> {

	@Override
	public SoilSamples convert(SoilSubsiteObservation value) {
		return convert(new SoilSamples(), value);
	}

	@Override
	public SoilSamples convert(SoilSamples initialValue, SoilSubsiteObservation value) {
		initialValue.setComments(value.getComments());
		initialValue.setEasting(value.getEasting());
		initialValue.setId(value.getId());
		initialValue.setMetagenomicBarcode(value.getMetagenomicBarcode());
		initialValue.setMetagenomicsAreAvailable(value.getMetagenomicsAreAvailable());
		initialValue.setNorthing(value.getNorthing());
		initialValue.setObserver(value.getObserver());
		initialValue.setPhotoLink(value.getPhotoLink());
		initialValue.setPhotoNo(value.getPhotoNo());
		initialValue.setSampleId(value.getSubsiteId());
		initialValue.setTenToTwentyBarcode(value.getTenToTwentyBarcode());
		initialValue.setTwentyToThirtyBarcode(value.getTwentyToThirtyBarcode());
		initialValue.setZeroToTenBarcode(value.getZeroToTenBarcode());
		initialValue.setZone(value.getZone());
		return initialValue;
	}

	@Override
	public Class<SoilSamples> getOutputClass() {
		return SoilSamples.class;
	}

	@Override
	public Class<SoilSubsiteObservation> getInputClass() {
		return SoilSubsiteObservation.class;
	}

}
