package au.org.ecoinformatics.s2s.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationDetailsConverter;
import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationDetailsObserverConverter;
import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationDetailsPointConverter;
import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationDetailsSiteLocationVisitConverter;
import au.org.ecoinformatics.s2s.domain.Observer;
import au.org.ecoinformatics.s2s.domain.SiteLocation;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationDetails;
import au.org.ecoinformatics.s2s.json.StudyLocationPoint;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;
import au.org.ecoinformatics.s2s.repository.ObserverRepository;
import au.org.ecoinformatics.s2s.repository.SiteLocationRepository;
import au.org.ecoinformatics.s2s.repository.SiteLocationVisitRepository;
import au.org.ecoinformatics.s2s.service.StudyLocationDetailsService;
import au.org.ecoinformatics.s2s.service.StudyLocationPointService;
import au.org.ecoinformatics.s2s.service.StudyLocationVisitService;

@Service("studyLocationDetailsService")
public class DefaultStudyLocationDetailsService implements StudyLocationDetailsService {

	@Autowired
	private StudyLocationPointService studyLocationPointService;
	@Autowired
	private StudyLocationVisitService studyLocationVisitService;

	@Autowired
	private SiteLocationRepository siteLocationRepository;
	@Autowired
	private SiteLocationVisitRepository siteLocationVisitRepository;
	@Autowired
	private ObserverRepository observerRepository;
	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	
	@Autowired
	private StudyLocationDetailsConverter studyLocationDetailsConverter;
	@Autowired
	private StudyLocationDetailsObserverConverter studyLocationDetailsObserverConverter;
	@Autowired
	private StudyLocationDetailsPointConverter studyLocationDetailsPointConverter;
	@Autowired
	private StudyLocationDetailsSiteLocationVisitConverter studyLocationDetailsSiteLocationVisitConverter;


	@Override
	@Transactional
	public StudyLocationDetails getStudyLocationDetails(Integer siteLocationId) {
		SiteLocation sl = this.siteLocationRepository.findOne(siteLocationId);
		Assert.notNull(sl, "No Site Location exists for siteLocationId: " + siteLocationId);
		List<Observer> observers = this.observerRepository.findDistinctObservers(siteLocationId);
		List<SiteLocationVisit> visits = this.siteLocationVisitRepository.findSiteLocationVisits(siteLocationId);
		StudyLocationVisit slvFirst = this.studyLocationVisitService.getFirstStudyLocationVisit(siteLocationId);
		StudyLocationVisit slvLast = this.studyLocationVisitService.getLastStudyLocationVisit(siteLocationId);
		StudyLocationPoint markerPoint = this.studyLocationPointService.getStudyLocationMarkerPoint(siteLocationId);
		Long distinctSpeciesCount = this.herbariumDeterminationRepository.findDistinctSpeciesCount(siteLocationId);
		
		StudyLocationDetails studyLocationDetails = this.studyLocationDetailsConverter.convert(sl);
		studyLocationDetails.setFirstVisitDate(slvFirst.getVisitStartDate());
		studyLocationDetails.setLastVisitDate(slvLast.getVisitStartDate());
		studyLocationDetails.setNumberOfDistinctPlantSpecies(distinctSpeciesCount);
		studyLocationDetails = this.studyLocationDetailsPointConverter.convert(studyLocationDetails, markerPoint);
		studyLocationDetails = this.studyLocationDetailsObserverConverter.convert(studyLocationDetails, observers);
		studyLocationDetails = this.studyLocationDetailsSiteLocationVisitConverter.convert(studyLocationDetails, visits);
		return studyLocationDetails;
	}	

}
