package au.org.ecoinformatics.s2s.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.org.ecoinformatics.s2s.json.SamplingUnit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitDetails;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnitDetails;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnits;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSpecies;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitVoucher;
import au.org.ecoinformatics.s2s.service.StudyLocationService;
import au.org.ecoinformatics.s2s.service.StudyLocationVisitDetailsService;
import au.org.ecoinformatics.s2s.service.StudyLocationVisitService;
import au.org.ecoinformatics.s2s.util.JsonUtilsBean;

@Controller(value="studyLocationVisitController")
public class StudyLocationVisitController {
	
	@Autowired
	private JsonUtilsBean jsonUtilsBean;
	@Autowired
	private StudyLocationService studyLocationService;
	@Autowired
	private StudyLocationVisitService studyLocationVisitService;
	@Autowired
	private StudyLocationVisitDetailsService studyLocationVisitDetailsService;
	
	@RequestMapping(value="/getStudyLocationVisits/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationVisits(@PathVariable String identifier, HttpServletResponse response) throws IOException {
		Integer studyLocationId = this.studyLocationService.getStudyLocationId(identifier);
		List<StudyLocationVisit> slvList = this.studyLocationVisitService.getStudyLocationVisits(studyLocationId);
		return this.jsonUtilsBean.writeValueAsString(slvList);
	}
	
	@RequestMapping(value="/getStudyLocationVisit/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationVisit(@PathVariable Integer identifier, HttpServletResponse response) throws IOException {
		StudyLocationVisit slv = this.studyLocationVisitService.getStudyLocationVisit(identifier);
		return this.jsonUtilsBean.writeValueAsString(slv);
	}
	
	@RequestMapping(value="/getStudyLocationVisitDetails/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationVisitDetails(@PathVariable Integer identifier, HttpServletResponse response) throws IOException {
		StudyLocationVisitDetails slvd = this.studyLocationVisitDetailsService.getStudyLocationVisitDetails(identifier);
		return this.jsonUtilsBean.writeValueAsString(slvd);
	}
	
	@RequestMapping(value = "/getStudyLocationVisitVouchers/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationVisitVouchers(@PathVariable Integer identifier, HttpServletResponse response) throws IOException {
		List<StudyLocationVisitVoucher> vouchers = this.studyLocationVisitService.getStudyLocationVisitVouchers(identifier);
		return this.jsonUtilsBean.writeValueAsString(vouchers);
	}
	
	@RequestMapping(value = "/getStudyLocationVisitSpecies/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationVisitSpecies(@PathVariable Integer identifier, HttpServletResponse response) throws IOException {
		StudyLocationVisitSpecies species = this.studyLocationVisitService.getStudyLocationVisitSpecies(identifier);
		return this.jsonUtilsBean.writeValueAsString(species);
	}
	
	@RequestMapping(value="/getSamplingUnits/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getSamplingUnits(@PathVariable Integer identifier, HttpServletResponse response) throws IOException {
		StudyLocationVisitSamplingUnits slvsu = this.studyLocationVisitService.getStudyLocationVisitSamplingUnits(identifier);
		return this.jsonUtilsBean.writeValueAsString(slvsu);
	}
	
	@RequestMapping(value="/getSamplingUnits/{identifier}/getDetails/{samplingUnitId}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationVisitSamplingUnitDetails(@PathVariable Integer identifier, @PathVariable Integer samplingUnitId, HttpServletResponse response) throws IOException {
		SamplingUnit samplingUnit = SamplingUnit.valueOf(samplingUnitId);
		StudyLocationVisitSamplingUnitDetails slvsud = this.studyLocationVisitDetailsService.getStudyLocationSamplingUnitDetails(identifier, samplingUnit);
		return this.jsonUtilsBean.writeValueAsString(slvsud);
	}
	
}
