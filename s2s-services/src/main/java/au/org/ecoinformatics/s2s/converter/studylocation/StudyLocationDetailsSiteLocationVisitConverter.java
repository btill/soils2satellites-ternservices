package au.org.ecoinformatics.s2s.converter.studylocation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.SamplingUnit;
import au.org.ecoinformatics.s2s.json.StudyLocationDetails;

@Component
public class StudyLocationDetailsSiteLocationVisitConverter implements Converter<StudyLocationDetails, List<SiteLocationVisit>> {

	@Override
	public StudyLocationDetails convert(List<SiteLocationVisit> visits) {
		return convert(new StudyLocationDetails(), visits);
	}

	@Override
	public StudyLocationDetails convert(StudyLocationDetails details, List<SiteLocationVisit> visits) {
		details.setNumberOfVisits(visits.size());
		Set<SamplingUnit> suSet = new HashSet<SamplingUnit>();
		for(SamplingUnit su : SamplingUnit.values()) {
			if(suSet.contains(su)) {
				continue;
			}
			for(SiteLocationVisit slv : visits) {
				if(su.hasSamplingUnitData(slv)) {
					suSet.add(su);
					break;
				}
			}
		}
		details.setSamplingUnits(suSet);
		return details;
	}

}
