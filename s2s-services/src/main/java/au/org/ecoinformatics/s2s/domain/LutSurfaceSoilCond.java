package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_surface_soil_cond database table.
 * 
 */
@Entity
@Table(name="lut_surface_soil_cond")
public class LutSurfaceSoilCond implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String condition;

	//bi-directional many-to-one association to VisitSurfaceSoilCondition
	@OneToMany(mappedBy="lutSurfaceSoilCond")
	private Set<VisitSurfaceSoilCondition> visitSurfaceSoilConditions;

	public LutSurfaceSoilCond() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCondition() {
		return this.condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Set<VisitSurfaceSoilCondition> getVisitSurfaceSoilConditions() {
		return this.visitSurfaceSoilConditions;
	}

	public void setVisitSurfaceSoilConditions(Set<VisitSurfaceSoilCondition> visitSurfaceSoilConditions) {
		this.visitSurfaceSoilConditions = visitSurfaceSoilConditions;
	}

}