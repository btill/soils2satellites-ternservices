package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_drainage database table.
 * 
 */
@Entity
@Table(name="lut_drainage")
public class LutDrainage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(nullable=false, length=2147483647)
	private String description;

	@Column(nullable=false, length=2147483647)
	private String drainage;

	//bi-directional many-to-one association to SiteLocationVisit
	@OneToMany(mappedBy="lutDrainage")
	private Set<SiteLocationVisit> siteLocationVisits;

	public LutDrainage() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDrainage() {
		return this.drainage;
	}

	public void setDrainage(String drainage) {
		this.drainage = drainage;
	}

	public Set<SiteLocationVisit> getSiteLocationVisits() {
		return this.siteLocationVisits;
	}

	public void setSiteLocationVisits(Set<SiteLocationVisit> siteLocationVisits) {
		this.siteLocationVisits = siteLocationVisits;
	}

}