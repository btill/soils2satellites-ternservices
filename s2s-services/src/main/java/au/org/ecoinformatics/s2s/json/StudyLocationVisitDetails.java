package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.org.ecoinformatics.s2s.domain.Observer;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocationVisitDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer studyLocationId;
	private Integer studyLocationVisitId;
	private String studyLocationName;
	private String climaticCondition;
	private String locationDescription;
	private String erosionType;
	private String erosionState;
	private String erosionAbundance;
	private String microrelief;
	private String drainageType;
	private String disturbance;
	private String soilObservationType;
	private String pitMarkerDatum;
	private Double pitMarkerEasting;
	private String pitMarkerLocationMethod;
	private Integer pitMarkerMgaZones;
	private Double pitMarkerNorthing;
	private String surfaceCoarseFragsAbundance;
	private String surfaceCoarseFragsLithology;
	private String surfaceCoarseFragsSize;
	private String surfaceCoarseFragsType;
	private String vegetationCondition;
	private Date visitEndDate;
	private String visitNotes;
	private Date visitStartDate;
	private List<SamplingUnit> samplingUnits = new ArrayList<SamplingUnit>();
	private List<Observer> observers = new ArrayList<Observer>();

	public StudyLocationVisitDetails() {
	}

	public Integer getStudyLocationId() {
		return studyLocationId;
	}

	public void setStudyLocationId(Integer studyLocationId) {
		this.studyLocationId = studyLocationId;
	}

	public Integer getStudyLocationVisitId() {
		return this.studyLocationVisitId;
	}

	public void setStudyLocationVisitId(Integer studyLocationVisitId) {
		this.studyLocationVisitId = studyLocationVisitId;
	}

	public String getStudyLocationName() {
		return studyLocationName;
	}

	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}

	public String getClimaticCondition() {
		return this.climaticCondition;
	}

	public void setClimaticCondition(String climaticCondition) {
		this.climaticCondition = climaticCondition;
	}

	public String getLocationDescription() {
		return this.locationDescription;
	}

	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}

	public String getErosionType() {
		return erosionType;
	}

	public void setErosionType(String erosionType) {
		this.erosionType = erosionType;
	}

	public String getErosionState() {
		return erosionState;
	}

	public void setErosionState(String erosionState) {
		this.erosionState = erosionState;
	}

	public String getErosionAbundance() {
		return erosionAbundance;
	}

	public void setErosionAbundance(String erosionAbundance) {
		this.erosionAbundance = erosionAbundance;
	}

	public String getMicrorelief() {
		return microrelief;
	}

	public void setMicrorelief(String microrelief) {
		this.microrelief = microrelief;
	}

	public String getDrainageType() {
		return drainageType;
	}

	public void setDrainageType(String drainageType) {
		this.drainageType = drainageType;
	}

	public String getDisturbance() {
		return disturbance;
	}

	public void setDisturbance(String disturbance) {
		this.disturbance = disturbance;
	}

	public String getSoilObservationType() {
		return soilObservationType;
	}

	public void setSoilObservationType(String soilObservationType) {
		this.soilObservationType = soilObservationType;
	}

	public String getPitMarkerDatum() {
		return this.pitMarkerDatum;
	}

	public void setPitMarkerDatum(String pitMarkerDatum) {
		this.pitMarkerDatum = pitMarkerDatum;
	}

	public Double getPitMarkerEasting() {
		return this.pitMarkerEasting;
	}

	public void setPitMarkerEasting(Double pitMarkerEasting) {
		this.pitMarkerEasting = pitMarkerEasting;
	}

	public String getPitMarkerLocationMethod() {
		return this.pitMarkerLocationMethod;
	}

	public void setPitMarkerLocationMethod(String pitMarkerLocationMethod) {
		this.pitMarkerLocationMethod = pitMarkerLocationMethod;
	}

	public Integer getPitMarkerMgaZones() {
		return this.pitMarkerMgaZones;
	}

	public void setPitMarkerMgaZones(Integer pitMarkerMgaZones) {
		this.pitMarkerMgaZones = pitMarkerMgaZones;
	}

	public Double getPitMarkerNorthing() {
		return this.pitMarkerNorthing;
	}

	public void setPitMarkerNorthing(Double pitMarkerNorthing) {
		this.pitMarkerNorthing = pitMarkerNorthing;
	}

	public String getSurfaceCoarseFragsAbundance() {
		return this.surfaceCoarseFragsAbundance;
	}

	public void setSurfaceCoarseFragsAbundance(String surfaceCoarseFragsAbundance) {
		this.surfaceCoarseFragsAbundance = surfaceCoarseFragsAbundance;
	}

	public String getSurfaceCoarseFragsLithology() {
		return this.surfaceCoarseFragsLithology;
	}

	public void setSurfaceCoarseFragsLithology(String surfaceCoarseFragsLithology) {
		this.surfaceCoarseFragsLithology = surfaceCoarseFragsLithology;
	}

	public String getSurfaceCoarseFragsSize() {
		return this.surfaceCoarseFragsSize;
	}

	public void setSurfaceCoarseFragsSize(String surfaceCoarseFragsSize) {
		this.surfaceCoarseFragsSize = surfaceCoarseFragsSize;
	}

	public String getSurfaceCoarseFragsType() {
		return this.surfaceCoarseFragsType;
	}

	public void setSurfaceCoarseFragsType(String surfaceCoarseFragsType) {
		this.surfaceCoarseFragsType = surfaceCoarseFragsType;
	}

	public String getVegetationCondition() {
		return this.vegetationCondition;
	}

	public void setVegetationCondition(String vegetationCondition) {
		this.vegetationCondition = vegetationCondition;
	}

	public Date getVisitEndDate() {
		return this.visitEndDate;
	}

	public void setVisitEndDate(Date visitEndDate) {
		this.visitEndDate = visitEndDate;
	}

	public String getVisitNotes() {
		return this.visitNotes;
	}

	public void setVisitNotes(String visitNotes) {
		this.visitNotes = visitNotes;
	}

	public Date getVisitStartDate() {
		return this.visitStartDate;
	}

	public void setVisitStartDate(Date visitStartDate) {
		this.visitStartDate = visitStartDate;
	}

	public List<SamplingUnit> getSamplingUnits() {
		return samplingUnits;
	}

	public void setSamplingUnits(List<SamplingUnit> samplingUnits) {
		this.samplingUnits = samplingUnits;
	}

	public List<Observer> getObservers() {
		return observers;
	}

	public void setObservers(List<Observer> observers) {
		this.observers = observers;
	}

}