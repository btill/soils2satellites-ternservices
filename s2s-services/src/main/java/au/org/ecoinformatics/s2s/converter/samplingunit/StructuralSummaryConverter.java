package au.org.ecoinformatics.s2s.converter.samplingunit;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.KeyedConverter;
import au.org.ecoinformatics.s2s.json.StructuralSummary;

@Component
public class StructuralSummaryConverter implements KeyedConverter<StructuralSummary, au.org.ecoinformatics.s2s.domain.StructuralSummary> {

	@Override
	public StructuralSummary convert(au.org.ecoinformatics.s2s.domain.StructuralSummary value) {
		return convert(new StructuralSummary(), value);
	}

	@Override
	public StructuralSummary convert(StructuralSummary initialValue, au.org.ecoinformatics.s2s.domain.StructuralSummary value) {
		initialValue.setDescription(value.getDescription());
		initialValue.setGround1Dominant(value.getGround1Dominant());
		initialValue.setGround2Dominant(value.getGround2Dominant());
		initialValue.setGround3Dominant(value.getGround3Dominant());
		initialValue.setId(value.getId());
		initialValue.setMassFloweringEvent(value.getMassFloweringEvent());
		initialValue.setMid1Dominant(value.getMid1Dominant());
		initialValue.setMid2Dominant(value.getMid2Dominant());
		initialValue.setMid3Dominant(value.getMid3Dominant());
		initialValue.setPhenologyComment(value.getPhenologyComment());
		initialValue.setUpper1Dominant(value.getUpper1Dominant());
		initialValue.setUpper2Dominant(value.getUpper2Dominant());
		initialValue.setUpper3Dominant(value.getUpper3Dominant());
		return initialValue;
	}

	@Override
	public Class<StructuralSummary> getOutputClass() {
		return StructuralSummary.class;
	}

	@Override
	public Class<au.org.ecoinformatics.s2s.domain.StructuralSummary> getInputClass() {
		return au.org.ecoinformatics.s2s.domain.StructuralSummary.class;
	}

}
