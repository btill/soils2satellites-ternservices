package au.org.ecoinformatics.s2s.config;

import java.util.ArrayList;
import java.util.List;

/**
 * Used for configuration of traits available for searching,
 * exposed via the SearchTraitsService
 * @author btill
 */
public class TraitConfig {
	
	private String name;
	private String description;
	private SearchType searchType;
	private String lookupTableName;
	private String lookupTableColumn;
	private String table;
	private String column;
	private Boolean active = Boolean.TRUE;
	private Boolean count = Boolean.FALSE;
	private Boolean availableForVisitSearch = Boolean.TRUE;
	private List<String> allowedValueList = new ArrayList<String>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLookupTableName() {
		return lookupTableName;
	}
	public void setLookupTableName(String lookupTableName) {
		this.lookupTableName = lookupTableName;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public void setSearchType(SearchType searchType) {
		this.searchType = searchType;
	}
	public SearchType getSearchType() {
		return searchType;
	}
	public List<String> getAllowedValueList() {
		return allowedValueList;
	}
	public void setAllowedValueList(List<String> allowedValueList) {
		this.allowedValueList = allowedValueList;
	}
	public Boolean getCount() {
		return count;
	}
	public void setCount(Boolean count) {
		this.count = count;
	}
	public Boolean getAvailableForVisitSearch() {
		return availableForVisitSearch;
	}
	public void setAvailableForVisitSearch(Boolean availableForVisitSearch) {
		this.availableForVisitSearch = availableForVisitSearch;
	}
	public String getLookupTableColumn() {
		return lookupTableColumn;
	}
	public void setLookupTableColumn(String lookupTableColumn) {
		this.lookupTableColumn = lookupTableColumn;
	}
	

}
