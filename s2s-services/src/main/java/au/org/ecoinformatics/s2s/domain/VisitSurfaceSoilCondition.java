package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the visit_surface_soil_conditions database table.
 * 
 */
@Entity
@Table(name="visit_surface_soil_conditions")
public class VisitSurfaceSoilCondition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	//bi-directional many-to-one association to LutSurfaceSoilCond
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="surface_soil_condition_id", nullable=false)
	private LutSurfaceSoilCond lutSurfaceSoilCond;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public VisitSurfaceSoilCondition() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LutSurfaceSoilCond getLutSurfaceSoilCond() {
		return this.lutSurfaceSoilCond;
	}

	public void setLutSurfaceSoilCond(LutSurfaceSoilCond lutSurfaceSoilCond) {
		this.lutSurfaceSoilCond = lutSurfaceSoilCond;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}