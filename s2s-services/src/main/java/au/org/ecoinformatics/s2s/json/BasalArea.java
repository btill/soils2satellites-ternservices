package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder(alphabetic=true)
public class BasalArea implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Double basalArea;
	private Double basalAreaFactor;
	private Integer hits;
	private String pointId;
	private String vegBarcode;
	private String herbariumDeterrmination;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getBasalArea() {
		return basalArea;
	}

	public void setBasalArea(Double basalArea) {
		this.basalArea = basalArea;
	}

	public Double getBasalAreaFactor() {
		return basalAreaFactor;
	}

	public void setBasalAreaFactor(Double basalAreaFactor) {
		this.basalAreaFactor = basalAreaFactor;
	}

	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}

	public String getPointId() {
		return pointId;
	}

	public void setPointId(String pointId) {
		this.pointId = pointId;
	}

	public String getVegBarcode() {
		return vegBarcode;
	}

	public void setVegBarcode(String vegBarcode) {
		this.vegBarcode = vegBarcode;
	}

	public String getHerbariumDeterrmination() {
		return herbariumDeterrmination;
	}

	public void setHerbariumDeterrmination(String herbariumDeterrmination) {
		this.herbariumDeterrmination = herbariumDeterrmination;
	}
	

}