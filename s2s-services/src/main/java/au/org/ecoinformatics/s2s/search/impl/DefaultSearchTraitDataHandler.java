package au.org.ecoinformatics.s2s.search.impl;

import java.util.Collections;
import java.util.Set;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.search.SearchTraitDataHandler;

@Component
public class DefaultSearchTraitDataHandler implements SearchTraitDataHandler<Object> {

	@Override
	public Set<Object> getSearchTraitData() {
		return Collections.emptySet();
	}

	@Override
	public SearchTrait getSearchTrait() {
		return SearchTrait.NULL;
	}

}
