package au.org.ecoinformatics.s2s.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.DistinctSpeciesCount;
import au.org.ecoinformatics.s2s.json.HerbariumPointIntercept;
import au.org.ecoinformatics.s2s.json.HerbariumPointInterceptCount;
import au.org.ecoinformatics.s2s.json.HerbariumSpeciesCount;
import au.org.ecoinformatics.s2s.json.LandformElement;
import au.org.ecoinformatics.s2s.json.LandformElementTotal;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;
import au.org.ecoinformatics.s2s.repository.LutLandformElementRepository;
import au.org.ecoinformatics.s2s.service.VisualisationService;

@Service("visualisationService")
public class DefaultVisualisationService implements VisualisationService {

	@Autowired
	private Converter<DistinctSpeciesCount, Object[]> distinctSpeciesCountConverter;
	@Autowired
	private Converter<HerbariumPointIntercept, Object[]> herbariumPointInterceptConverter;
	@Autowired
	private Converter<HerbariumPointInterceptCount, Object[]> herbariumPointInterceptCountConverter;
	@Autowired
	private Converter<HerbariumSpeciesCount, Object[]> herbariumSpeciesCountConverter;
	@Autowired
	private Converter<LandformElement, Object[]> landformElementConverter;
	@Autowired
	private Converter<LandformElementTotal, Object[]> landformElementTotalConverter;
	
	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	@Autowired
	private LutLandformElementRepository lutLandformElementRepository;
	
	@Override
	public List<DistinctSpeciesCount> getDistinctSpeciesCountList(List<Integer> studyLocationIdList) {
		List<Object[]> dscList = this.herbariumDeterminationRepository.findDistinctSpeciesCountList(studyLocationIdList);
		return convert(dscList, this.distinctSpeciesCountConverter);
	}

	@Override
	public List<HerbariumPointIntercept> getHerbariumPointIntercepts(Integer studyLocationId, Date visitStartDate) {
		List<Object[]> hpiList = this.herbariumDeterminationRepository.findHerbariumSiteLocationPointIntercepts(studyLocationId, visitStartDate);
		return convert(hpiList, this.herbariumPointInterceptConverter);
	}

	@Override
	public List<HerbariumPointInterceptCount> getHerbariumPointInterceptCount() {
		List<Object[]> hpicList = this.herbariumDeterminationRepository.findHerbariumPointInterceptCount();
		return convert(hpicList, this.herbariumPointInterceptCountConverter);
	}

	@Override
	public List<HerbariumSpeciesCount> getHerbariumSpeciesCount(Integer studyLocationVisitId) {
		List<Object[]> hscList = this.herbariumDeterminationRepository.findHerbariumSpeciesCount(studyLocationVisitId);
		return convert(hscList, this.herbariumSpeciesCountConverter);
	}

	@Override
	public List<LandformElement> getLandFormElements(List<Integer> studyLocationIdList) {
		List<Object[]> lfeList = this.lutLandformElementRepository.findLutLandFormElements(studyLocationIdList);
		return convert(lfeList, this.landformElementConverter);
	}

	@Override
	public List<LandformElementTotal> getLandFormElementTotals(List<Integer> studyLocationIdList) {
		List<Object[]> lfetList = this.lutLandformElementRepository.findLutLandFormElementTotals(studyLocationIdList);
		return convert(lfetList, this.landformElementTotalConverter);
	}
	
	private <T> List<T> convert(List<Object[]> data, Converter<T, Object[]> converter) {
		List<T> output = new ArrayList<T>();
		for(Object[] dataElement : data) {
			T typedData = converter.convert(dataElement);
			output.add(typedData);
		}
		return output;
	}

}
