package au.org.ecoinformatics.s2s.json;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum SearchTrait {

	STUDY_LOCATION_NAME(0, "Study Location Name", "studyLocationName", SearchDataType.String, "siteLocation.siteLocationName"),
	ASPECT(1, "Aspect", "aspect", SearchDataType.Integer, "siteLocation.siteAspect"),
	SLOPE(2, "Slope", "slope", SearchDataType.Integer, "siteLocation.siteSlope"),
	VISIT_DATE(3, "Visit Date", "visitDate", SearchDataType.Date, "visitStartDate"),
	SPECIES_NAME(4, "Species Name", "speciesName", SearchDataType.String, "vegVouchers.herbariumDetermination.herbariumDetermination"),
	SOIL_TEXTURE(5, "Soil Texture", "soilTexture", SearchDataType.String, "soilCharacterisations.lutSoilTexGrade.code"),
	METAGENOMICS(6, "Metagenomics", "hasMetagenomics", SearchDataType.Boolean, "soilSubsiteObservations.metagenomicsAreAvailable"),
	LATITUDE(7, "Latitude", "latitude", SearchDataType.Decimal, "siteLocation.siteLocationPoints.latitude"),
	LONGITUDE(8, "Longitude", "longitude", SearchDataType.Decimal, "siteLocation.siteLocationPoints.longitude"),
	NULL(-1, "Null", null, SearchDataType.Null, "");
	
	private static Map<Integer, SearchTrait> stMap = new LinkedHashMap<Integer, SearchTrait>();
	
	private final Integer id;
	private final String name;
	private final String criteria;
	private final SearchDataType searchDataType;
	private final String fieldName;
	
	private SearchTrait(Integer id, String name, String criteria, SearchDataType searchDataType, String fieldName) {
		this.id = id;
		this.name = name;
		this.criteria = criteria;
		this.searchDataType = searchDataType;
		this.fieldName = fieldName;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCriteria() {
		return criteria;
	}

	public SearchDataType getSearchDataType() {
		return searchDataType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void appendFieldMapping(Map<String, String> fieldMap) {
		fieldMap.put(getCriteria(), getFieldName());
		fieldMap.put(getFieldName().toLowerCase(), getFieldName());
	}
	
	public static List<SearchTrait> asList() {
		List<SearchTrait> stList = new ArrayList<SearchTrait>();
		stList.addAll(stMap.values());
		return stList;
	}
	
	public static SearchTrait valueOf(Integer id) {
		SearchTrait st = stMap.get(id);
		if(null == st) {
			throw new IllegalArgumentException("No SearchTrait for ID: " + id);
		}
		return st;
	}
	
	static {
		for(SearchTrait st : SearchTrait.values()) {
			if(st != SearchTrait.NULL) {
				stMap.put(st.getId(), st);
			}
		}
	}
}
