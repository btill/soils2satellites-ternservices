package au.org.ecoinformatics.s2s.util;

import java.io.IOException;

import au.org.ecoinformatics.s2s.json.SamplingUnit;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

public class SamplingUnitSerializer extends com.fasterxml.jackson.databind.ser.std.StdSerializer<SamplingUnit> {

	public SamplingUnitSerializer() {
		super(SamplingUnit.class);
	}

	@Override
	public void serialize(SamplingUnit su, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeStringField("description", su.getDescription());
		jgen.writeNumberField("id", su.getId());
		jgen.writeEndObject();
	}

}
