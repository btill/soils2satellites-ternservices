package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder(alphabetic=true)
public class Lai implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Timestamp completionDateTime;
	private String locationOfLaiData;
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getCompletionDateTime() {
		return this.completionDateTime;
	}

	public void setCompletionDateTime(Timestamp completionDateTime) {
		this.completionDateTime = completionDateTime;
	}

	public String getLocationOfLaiData() {
		return this.locationOfLaiData;
	}

	public void setLocationOfLaiData(String locationOfLaiData) {
		this.locationOfLaiData = locationOfLaiData;
	}

}