package au.org.ecoinformatics.s2s.json;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.org.ecoinformatics.s2s.domain.Observer;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocationDetails {

	private Integer studyLocationId;
	private String studyLocationName;
	private String bioregionName;
	private Double longitude; 
	private Double latitude;
	private Double easting;
	private Double northing;
	private Integer mgaZone;
	private Long numberOfDistinctPlantSpecies;
	private String landformPattern;
	private String landformElement;
	private Integer numberOfVisits;
	private Date firstVisitDate;
	private Date lastVisitDate;
	private List<Observer> observers = new ArrayList<Observer>();
	private Set<SamplingUnit> samplingUnits = new HashSet<SamplingUnit>();
	
	public Integer getStudyLocationId() {
		return studyLocationId;
	}
	public void setStudyLocationId(Integer studyLocationId) {
		this.studyLocationId = studyLocationId;
	}
	public String getStudyLocationName() {
		return studyLocationName;
	}
	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}
	public String getBioregionName() {
		return bioregionName;
	}
	public void setBioregionName(String bioregionName) {
		this.bioregionName = bioregionName;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Integer getMgaZone() {
		return mgaZone;
	}
	public void setMgaZone(Integer mgaZone) {
		this.mgaZone = mgaZone;
	}
	public Long getNumberOfDistinctPlantSpecies() {
		return numberOfDistinctPlantSpecies;
	}
	public void setNumberOfDistinctPlantSpecies(Long numberOfDistinctPlantSpecies) {
		this.numberOfDistinctPlantSpecies = numberOfDistinctPlantSpecies;
	}
	public String getLandformPattern() {
		return landformPattern;
	}
	public void setLandformPattern(String landformPattern) {
		this.landformPattern = landformPattern;
	}
	public String getLandformElement() {
		return landformElement;
	}
	public void setLandformElement(String landformElement) {
		this.landformElement = landformElement;
	}
	public Integer getNumberOfVisits() {
		return numberOfVisits;
	}
	public void setNumberOfVisits(Integer numberOfVisits) {
		this.numberOfVisits = numberOfVisits;
	}
	public Date getFirstVisitDate() {
		return firstVisitDate;
	}
	public void setFirstVisitDate(Date firstVisitDate) {
		this.firstVisitDate = firstVisitDate;
	}
	public Date getLastVisitDate() {
		return lastVisitDate;
	}
	public void setLastVisitDate(Date lastVisitDate) {
		this.lastVisitDate = lastVisitDate;
	}
	public List<Observer> getObservers() {
		return observers;
	}
	public void setObservers(List<Observer> observers) {
		this.observers = observers;
	}
	public Set<SamplingUnit> getSamplingUnits() {
		return samplingUnits;
	}
	public void setSamplingUnits(Set<SamplingUnit> samplingUnits) {
		this.samplingUnits = samplingUnits;
	}
	public Double getEasting() {
		return easting;
	}
	public void setEasting(Double easting) {
		this.easting = easting;
	}
	public Double getNorthing() {
		return northing;
	}
	public void setNorthing(Double northing) {
		this.northing = northing;
	}
	
}
