package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder(alphabetic=true)
public class SoilBulkDensity implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Double fineEarthBulkDensity;
	private Double fineEarthVolume;
	private Double fineEarthWeight;
	private Double fineEarthWeightInBag;
	private Double gravelBulkDensity;
	private Double gravelVolume;
	private Double gravelWeight;
	private Double ovenDriedWeightInBag;
	private Double paperBagWeight;
	private Double ringVolume;
	private Double ringWeight;
	private String sampleId;

	public SoilBulkDensity() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getFineEarthBulkDensity() {
		return this.fineEarthBulkDensity;
	}

	public void setFineEarthBulkDensity(Double fineEarthBulkDensity) {
		this.fineEarthBulkDensity = fineEarthBulkDensity;
	}

	public Double getFineEarthVolume() {
		return this.fineEarthVolume;
	}

	public void setFineEarthVolume(Double fineEarthVolume) {
		this.fineEarthVolume = fineEarthVolume;
	}

	public Double getFineEarthWeight() {
		return this.fineEarthWeight;
	}

	public void setFineEarthWeight(Double fineEarthWeight) {
		this.fineEarthWeight = fineEarthWeight;
	}

	public Double getFineEarthWeightInBag() {
		return this.fineEarthWeightInBag;
	}

	public void setFineEarthWeightInBag(Double fineEarthWeightInBag) {
		this.fineEarthWeightInBag = fineEarthWeightInBag;
	}

	public Double getGravelBulkDensity() {
		return this.gravelBulkDensity;
	}

	public void setGravelBulkDensity(Double gravelBulkDensity) {
		this.gravelBulkDensity = gravelBulkDensity;
	}

	public Double getGravelVolume() {
		return this.gravelVolume;
	}

	public void setGravelVolume(Double gravelVolume) {
		this.gravelVolume = gravelVolume;
	}

	public Double getGravelWeight() {
		return this.gravelWeight;
	}

	public void setGravelWeight(Double gravelWeight) {
		this.gravelWeight = gravelWeight;
	}

	public Double getOvenDriedWeightInBag() {
		return this.ovenDriedWeightInBag;
	}

	public void setOvenDriedWeightInBag(Double ovenDriedWeightInBag) {
		this.ovenDriedWeightInBag = ovenDriedWeightInBag;
	}

	public Double getPaperBagWeight() {
		return this.paperBagWeight;
	}

	public void setPaperBagWeight(Double paperBagWeight) {
		this.paperBagWeight = paperBagWeight;
	}

	public Double getRingVolume() {
		return this.ringVolume;
	}

	public void setRingVolume(Double ringVolume) {
		this.ringVolume = ringVolume;
	}

	public Double getRingWeight() {
		return this.ringWeight;
	}

	public void setRingWeight(Double ringWeight) {
		this.ringWeight = ringWeight;
	}

	public String getSampleId() {
		return this.sampleId;
	}

	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}

}