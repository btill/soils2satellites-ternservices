package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_mga_zone database table.
 * 
 */
@Entity
@Table(name="lut_mga_zone")
public class LutMgaZone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="mga_zone", unique=true, nullable=false)
	private Integer mgaZone;

	@Column(name="eastern_boundary", nullable=false, length=2147483647)
	private String easternBoundary;

	@Column(nullable=false, length=2147483647)
	private String projection;

	@Column(name="western_boundary", nullable=false, length=2147483647)
	private String westernBoundary;

	//bi-directional many-to-one association to LutDatum
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="datum", nullable=false)
	private LutDatum lutDatum;

	//bi-directional many-to-one association to SiteLocation
	@OneToMany(mappedBy="lutMgaZone")
	private Set<SiteLocation> siteLocations;

	public LutMgaZone() {
	}

	public Integer getMgaZone() {
		return this.mgaZone;
	}

	public void setMgaZone(Integer mgaZone) {
		this.mgaZone = mgaZone;
	}

	public String getEasternBoundary() {
		return this.easternBoundary;
	}

	public void setEasternBoundary(String easternBoundary) {
		this.easternBoundary = easternBoundary;
	}

	public String getProjection() {
		return this.projection;
	}

	public void setProjection(String projection) {
		this.projection = projection;
	}

	public String getWesternBoundary() {
		return this.westernBoundary;
	}

	public void setWesternBoundary(String westernBoundary) {
		this.westernBoundary = westernBoundary;
	}

	public LutDatum getLutDatum() {
		return this.lutDatum;
	}

	public void setLutDatum(LutDatum lutDatum) {
		this.lutDatum = lutDatum;
	}

	public Set<SiteLocation> getSiteLocations() {
		return this.siteLocations;
	}

	public void setSiteLocations(Set<SiteLocation> siteLocations) {
		this.siteLocations = siteLocations;
	}

}