package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_seg_abundance database table.
 * 
 */
@Entity
@Table(name="lut_seg_abundance")
public class LutSegAbundance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String abundance;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutSegAbundance")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutSegAbundance() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAbundance() {
		return this.abundance;
	}

	public void setAbundance(String abundance) {
		this.abundance = abundance;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}