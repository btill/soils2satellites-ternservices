package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_landform_element database table.
 * 
 */
@Entity
@Table(name="lut_landform_element")
public class LutLandformElement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String code;

	@Column(length=2147483647)
	private String description;

	@Column(name="landform_element", length=2147483647)
	private String landformElement;

	//bi-directional many-to-one association to SiteLocation
	@OneToMany(mappedBy="lutLandformElement")
	private Set<SiteLocation> siteLocations;

	public LutLandformElement() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLandformElement() {
		return this.landformElement;
	}

	public void setLandformElement(String landformElement) {
		this.landformElement = landformElement;
	}

	public Set<SiteLocation> getSiteLocations() {
		return this.siteLocations;
	}

	public void setSiteLocations(Set<SiteLocation> siteLocations) {
		this.siteLocations = siteLocations;
	}

}