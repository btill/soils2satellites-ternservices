package au.org.ecoinformatics.s2s.util;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.NestedNullException;

public class ConvertUtils {
	
	public static Object getProperty(Object bean, String property) {
		try {
			return BeanUtils.getProperty(bean, property);
		}
		catch(NestedNullException nne) {
			return null;
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
