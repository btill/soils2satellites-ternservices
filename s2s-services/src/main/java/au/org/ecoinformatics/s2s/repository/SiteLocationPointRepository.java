package au.org.ecoinformatics.s2s.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import au.org.ecoinformatics.s2s.domain.SiteLocationPoint;

@Repository("siteLocationPointRepository")
public interface SiteLocationPointRepository extends JpaRepository<SiteLocationPoint, Integer>, JpaSpecificationExecutor<SiteLocationPoint> {

	@Query("select slp " +
           "from SiteLocationPoint slp " +
           "where slp.point = 'SW' " +
           "order by slp.siteLocation.siteLocationName, slp.point ")
	List<SiteLocationPoint> findSiteLocationMarkerPoints();
	
	@Query("select slp " +
           "from SiteLocationPoint slp " +
           "where slp.point = 'SW' " +
           "and slp.siteLocation.siteLocationId = ?1 ")
	List<SiteLocationPoint> findSiteLocationMarkerPoint(Integer siteLocationId);
}
