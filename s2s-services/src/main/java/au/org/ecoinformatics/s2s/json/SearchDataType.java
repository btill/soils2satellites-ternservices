package au.org.ecoinformatics.s2s.json;

public enum SearchDataType {

	String,
	Date,
	Boolean,
	Integer,
	Decimal,
	Null;
	
}
