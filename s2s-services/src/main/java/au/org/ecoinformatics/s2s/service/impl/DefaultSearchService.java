package au.org.ecoinformatics.s2s.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.cxf.jaxrs.ext.search.SearchCondition;
import org.apache.cxf.jaxrs.ext.search.SearchParseException;
import org.apache.cxf.jaxrs.ext.search.jpa.JPATypedQueryVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.search.fiql.CustomFiqlParser;
import au.org.ecoinformatics.s2s.service.SearchService;

@Service("searchService")
public class DefaultSearchService implements SearchService {

	@Autowired
	private Converter<StudyLocationVisit, SiteLocationVisit> studyLocationVisitConverter;
	@PersistenceContext
    private EntityManager entityManager;
	private Map<String, String> fieldMapping = new HashMap<String, String>();
	
	private CustomFiqlParser<SiteLocationVisit> fiqlParser;
	
	@PostConstruct
	public void init() throws Exception {
		for(SearchTrait searchTrait : SearchTrait.values()) {
			searchTrait.appendFieldMapping(fieldMapping);
		}
		this.fiqlParser = new CustomFiqlParser<SiteLocationVisit>(SiteLocationVisit.class, null, this.fieldMapping);
	}
	
	@Override
	public List<SearchTrait> getSearchTraits() {
		return SearchTrait.asList();
	}

	@Override
	public Map<String, String> getFieldMappings() {
		return Collections.unmodifiableMap(this.fieldMapping);
	}

	@Override
	@Transactional
	public List<StudyLocationVisit> searchSiteVisits(String searchExpression) {
		SearchCondition<SiteLocationVisit> sc = getSearchCondition(searchExpression);
		Assert.notNull(sc, "SearchCondition is null");
		JPATypedQueryVisitor<SiteLocationVisit> visitor = new JPATypedQueryVisitor<SiteLocationVisit>(entityManager, SiteLocationVisit.class, fieldMapping);
		sc.accept(visitor);
		Collection<SiteLocationVisit> slvList = distinct(visitor.getQuery().getResultList());
		return convert(slvList, this.studyLocationVisitConverter);
	}
	
	private SearchCondition<SiteLocationVisit> getSearchCondition(String searchExpression) {
		try {
			return this.fiqlParser.parse(searchExpression);
		}
		catch(SearchParseException spe) {
			throw new IllegalArgumentException(spe.getMessage() + " - " + searchExpression);
		}
	}
	
	private Collection<SiteLocationVisit> distinct(Collection<SiteLocationVisit> values) {
		Map<Integer, SiteLocationVisit> slvMap = new HashMap<Integer, SiteLocationVisit>();
		for(SiteLocationVisit slv : values) {
			if(!slvMap.containsKey(slv.getSiteLocationVisitId())) {
				slvMap.put(slv.getSiteLocationVisitId(), slv);
			}
		}
		return slvMap.values();
	}
	
	private <K, T> List<T> convert(Collection<K> data, Converter<T, K> converter) {
		List<T> output = new ArrayList<T>();
		for(K dataElement : data) {
			T typedData = converter.convert(dataElement);
			output.add(typedData);
		}
		return output;
	}

}
