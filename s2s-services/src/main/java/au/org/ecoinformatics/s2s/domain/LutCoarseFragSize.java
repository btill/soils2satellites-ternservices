package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_coarse_frag_size database table.
 * 
 */
@Entity
@Table(name="lut_coarse_frag_size")
public class LutCoarseFragSize implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String size;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutCoarseFragSize")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutCoarseFragSize() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}