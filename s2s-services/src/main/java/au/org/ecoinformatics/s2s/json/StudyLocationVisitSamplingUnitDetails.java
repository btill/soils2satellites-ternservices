package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
@SuppressWarnings("rawtypes")
public class StudyLocationVisitSamplingUnitDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer studyLocationVisitId;
	private SamplingUnit samplingUnit = SamplingUnit.NULL;
	private String studyLocationName;
	private Set samplingUnitData = new HashSet();

	public StudyLocationVisitSamplingUnitDetails() {}

	public Integer getStudyLocationVisitId() {
		return this.studyLocationVisitId;
	}

	public void setStudyLocationVisitId(Integer studyLocationVisitId) {
		this.studyLocationVisitId = studyLocationVisitId;
	}

	public SamplingUnit getSamplingUnit() {
		return samplingUnit;
	}

	public void setSamplingUnit(SamplingUnit samplingUnit) {
		this.samplingUnit = samplingUnit;
	}

	public String getStudyLocationName() {
		return studyLocationName;
	}

	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}

	public Set getSamplingUnitData() {
		return samplingUnitData;
	}

	public void setSamplingUnitData(Set samplingUnitData) {
		this.samplingUnitData = samplingUnitData;
	}

}