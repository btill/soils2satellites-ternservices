package au.org.ecoinformatics.s2s.json;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocationName {

	private Integer studyLocationId;
	private String studyLocationName;
	
	public Integer getStudyLocationId() {
		return studyLocationId;
	}
	public void setStudyLocationId(Integer studyLocationId) {
		this.studyLocationId = studyLocationId;
	}
	public String getStudyLocationName() {
		return studyLocationName;
	}
	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}
	
	
}
