package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the soil_subsite_observations database table.
 * 
 */
@Entity
@Table(name="soil_subsite_observations")
public class SoilSubsiteObservation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(length=2147483647)
	private String comments;

	private Integer easting;

	@Column(name="metagenomic_barcode", length=2147483647)
	private String metagenomicBarcode;

	@Column(name="metagenomics_are_available")
	private Boolean metagenomicsAreAvailable;

	private Integer northing;

	@Column(length=2147483647)
	private String observer;

	@Column(name="photo_link", length=2147483647)
	private String photoLink;

	@Column(name="photo_no", length=2147483647)
	private String photoNo;

	@Column(name="subsite_id", length=2147483647)
	private String subsiteId;

	@Column(name="ten_to_twenty_barcode", length=2147483647)
	private String tenToTwentyBarcode;

	@Column(name="twenty_to_thirty_barcode", length=2147483647)
	private String twentyToThirtyBarcode;

	@Column(name="zero_to_ten_barcode", length=2147483647)
	private String zeroToTenBarcode;

	private Integer zone;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public SoilSubsiteObservation() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getEasting() {
		return this.easting;
	}

	public void setEasting(Integer easting) {
		this.easting = easting;
	}

	public String getMetagenomicBarcode() {
		return this.metagenomicBarcode;
	}

	public void setMetagenomicBarcode(String metagenomicBarcode) {
		this.metagenomicBarcode = metagenomicBarcode;
	}

	public Boolean getMetagenomicsAreAvailable() {
		return this.metagenomicsAreAvailable;
	}

	public void setMetagenomicsAreAvailable(Boolean metagenomicsAreAvailable) {
		this.metagenomicsAreAvailable = metagenomicsAreAvailable;
	}

	public Integer getNorthing() {
		return this.northing;
	}

	public void setNorthing(Integer northing) {
		this.northing = northing;
	}

	public String getObserver() {
		return this.observer;
	}

	public void setObserver(String observer) {
		this.observer = observer;
	}

	public String getPhotoLink() {
		return this.photoLink;
	}

	public void setPhotoLink(String photoLink) {
		this.photoLink = photoLink;
	}

	public String getPhotoNo() {
		return this.photoNo;
	}

	public void setPhotoNo(String photoNo) {
		this.photoNo = photoNo;
	}

	public String getSubsiteId() {
		return this.subsiteId;
	}

	public void setSubsiteId(String subsiteId) {
		this.subsiteId = subsiteId;
	}

	public String getTenToTwentyBarcode() {
		return this.tenToTwentyBarcode;
	}

	public void setTenToTwentyBarcode(String tenToTwentyBarcode) {
		this.tenToTwentyBarcode = tenToTwentyBarcode;
	}

	public String getTwentyToThirtyBarcode() {
		return this.twentyToThirtyBarcode;
	}

	public void setTwentyToThirtyBarcode(String twentyToThirtyBarcode) {
		this.twentyToThirtyBarcode = twentyToThirtyBarcode;
	}

	public String getZeroToTenBarcode() {
		return this.zeroToTenBarcode;
	}

	public void setZeroToTenBarcode(String zeroToTenBarcode) {
		this.zeroToTenBarcode = zeroToTenBarcode;
	}

	public Integer getZone() {
		return this.zone;
	}

	public void setZone(Integer zone) {
		this.zone = zone;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}