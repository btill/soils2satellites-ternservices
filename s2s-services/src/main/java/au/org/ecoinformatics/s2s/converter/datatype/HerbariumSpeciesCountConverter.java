package au.org.ecoinformatics.s2s.converter.datatype;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.HerbariumSpeciesCount;

@Component
public class HerbariumSpeciesCountConverter implements Converter<HerbariumSpeciesCount, Object[]> {

	@Override
	public HerbariumSpeciesCount convert(Object[] value) {
		return convert(new HerbariumSpeciesCount(), value);
	}

	@Override
	public HerbariumSpeciesCount convert(HerbariumSpeciesCount initialValue, Object[] value) {
		initialValue.setHerbariumDetermination((String) value[0]);
		initialValue.setSpeciesCount((Long) value[1]);
		return initialValue;
	}

}
