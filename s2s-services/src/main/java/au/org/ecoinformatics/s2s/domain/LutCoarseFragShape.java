package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_coarse_frag_shape database table.
 * 
 */
@Entity
@Table(name="lut_coarse_frag_shape")
public class LutCoarseFragShape implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String type;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutCoarseFragShape")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutCoarseFragShape() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}