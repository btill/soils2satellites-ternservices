package au.org.ecoinformatics.s2s.converter.studylocation;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.SiteLocationPoint;
import au.org.ecoinformatics.s2s.json.StudyLocationPoint;

@Component
public class StudyLocationPointConverter implements Converter<StudyLocationPoint, SiteLocationPoint> {

	@Override
	public StudyLocationPoint convert(SiteLocationPoint point) {
		return convert(new StudyLocationPoint(), point);
	}

	@Override
	public StudyLocationPoint convert(StudyLocationPoint marker, SiteLocationPoint point) {
		marker.setStudyLocationName(point.getSiteLocation().getSiteLocationName());
		marker.setEasting(point.getEasting());
		marker.setZone(point.getZone());
		marker.setNorthing(point.getNorthing());
		marker.setLatitude(point.getLatitude());
		marker.setLongitude(point.getLongitude());
		String wkt = "POINT (" + marker.getLongitude() + ", " + marker.getLatitude() + ")";
		marker.setWkt(wkt);
		return marker;
	}

}
