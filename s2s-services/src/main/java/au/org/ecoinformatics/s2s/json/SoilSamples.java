package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class SoilSamples implements Serializable {

	private static final long serialVersionUID = 3580274859811860843L;

	private Integer id;
	private String sampleId;
	private String comments;
	private Integer easting;
	private String metagenomicBarcode;
	private Boolean metagenomicsAreAvailable;
	private Integer northing;
	private String observer;
	private String photoLink;
	private String photoNo;
	private String tenToTwentyBarcode;
	private String twentyToThirtyBarcode;
	private String zeroToTenBarcode;
	private Integer zone;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSampleId() {
		return sampleId;
	}
	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getEasting() {
		return easting;
	}
	public void setEasting(Integer easting) {
		this.easting = easting;
	}
	public String getMetagenomicBarcode() {
		return metagenomicBarcode;
	}
	public void setMetagenomicBarcode(String metagenomicBarcode) {
		this.metagenomicBarcode = metagenomicBarcode;
	}
	public Boolean getMetagenomicsAreAvailable() {
		return metagenomicsAreAvailable;
	}
	public void setMetagenomicsAreAvailable(Boolean metagenomicsAreAvailable) {
		this.metagenomicsAreAvailable = metagenomicsAreAvailable;
	}
	public Integer getNorthing() {
		return northing;
	}
	public void setNorthing(Integer northing) {
		this.northing = northing;
	}
	public String getObserver() {
		return observer;
	}
	public void setObserver(String observer) {
		this.observer = observer;
	}
	public String getPhotoLink() {
		return photoLink;
	}
	public void setPhotoLink(String photoLink) {
		this.photoLink = photoLink;
	}
	public String getPhotoNo() {
		return photoNo;
	}
	public void setPhotoNo(String photoNo) {
		this.photoNo = photoNo;
	}
	public String getTenToTwentyBarcode() {
		return tenToTwentyBarcode;
	}
	public void setTenToTwentyBarcode(String tenToTwentyBarcode) {
		this.tenToTwentyBarcode = tenToTwentyBarcode;
	}
	public String getTwentyToThirtyBarcode() {
		return twentyToThirtyBarcode;
	}
	public void setTwentyToThirtyBarcode(String twentyToThirtyBarcode) {
		this.twentyToThirtyBarcode = twentyToThirtyBarcode;
	}
	public String getZeroToTenBarcode() {
		return zeroToTenBarcode;
	}
	public void setZeroToTenBarcode(String zeroToTenBarcode) {
		this.zeroToTenBarcode = zeroToTenBarcode;
	}
	public Integer getZone() {
		return zone;
	}
	public void setZone(Integer zone) {
		this.zone = zone;
	}


	
}
