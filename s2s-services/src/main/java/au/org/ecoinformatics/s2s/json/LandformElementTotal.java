package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class LandformElementTotal implements Serializable {
	
	private static final long serialVersionUID = -1067867203399635704L;
	
	private String landFormElement;
	private Long studyLocationCount;
	private Long totalCount;
	
	public String getLandFormElement() {
		return landFormElement;
	}
	public void setLandFormElement(String landFormElement) {
		this.landFormElement = landFormElement;
	}
	public Long getStudyLocationCount() {
		return studyLocationCount;
	}
	public void setStudyLocationCount(Long studyLocationCount) {
		this.studyLocationCount = studyLocationCount;
	}
	public Long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((landFormElement == null) ? 0 : landFormElement.hashCode());
		result = prime
				* result
				+ ((studyLocationCount == null) ? 0 : studyLocationCount
						.hashCode());
		result = prime * result
				+ ((totalCount == null) ? 0 : totalCount.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LandformElementTotal other = (LandformElementTotal) obj;
		if (landFormElement == null) {
			if (other.landFormElement != null)
				return false;
		} else if (!landFormElement.equals(other.landFormElement))
			return false;
		if (studyLocationCount == null) {
			if (other.studyLocationCount != null)
				return false;
		} else if (!studyLocationCount.equals(other.studyLocationCount))
			return false;
		if (totalCount == null) {
			if (other.totalCount != null)
				return false;
		} else if (!totalCount.equals(other.totalCount))
			return false;
		return true;
	}
	
}
