package au.org.ecoinformatics.s2s.util;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

@Component
public class JsonUtilsBean {

	private ObjectMapper mapper;
	
	@PostConstruct
	public void init() throws Exception {
		this.mapper = new ObjectMapper();
		this.mapper.setDateFormat(DateTimeFormats.getDateFormat());
        SimpleModule testModule = new SimpleModule("jsonModule");
        testModule.addSerializer(new SamplingUnitSerializer());
        testModule.addSerializer(new SearchTraitSerializer());
        this.mapper.registerModule(testModule);
	}
	
	public String writeValueAsString(Object data) throws IOException {
		return this.mapper.writeValueAsString(data);
	}
	
	public <T> T readValue(String jsonString, Class<T> valueType) throws IOException {
		return this.mapper.readValue(jsonString, valueType);
	}
}
