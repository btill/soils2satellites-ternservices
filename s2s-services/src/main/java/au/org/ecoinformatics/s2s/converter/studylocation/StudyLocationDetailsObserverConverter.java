package au.org.ecoinformatics.s2s.converter.studylocation;

import java.util.List;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.Observer;
import au.org.ecoinformatics.s2s.json.StudyLocationDetails;

@Component
public class StudyLocationDetailsObserverConverter implements Converter<StudyLocationDetails, List<Observer>> {

	@Override
	public StudyLocationDetails convert(List<Observer> value) {
		return convert(new StudyLocationDetails(), value);
	}

	@Override
	public StudyLocationDetails convert(StudyLocationDetails initialValue, List<Observer> value) {
		initialValue.setObservers(value);
		return initialValue;
	}

}
