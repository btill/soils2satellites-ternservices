package au.org.ecoinformatics.s2s.search.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.org.ecoinformatics.s2s.json.NameValuePair;
import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.search.SearchTraitDataHandler;
import au.org.ecoinformatics.s2s.search.SearchTraitDataService;

@Service("searchTraitDataService")
public class DefaultSearchTraitDataService implements SearchTraitDataService {

	private Map<SearchTrait, SearchTraitDataHandler<?>> handlerMap = new HashMap<SearchTrait, SearchTraitDataHandler<?>>();
	@Autowired
	private SearchTraitDataHandler<?> defaultSearchTraitDataHandler;
	@Autowired
	private SearchTraitDataHandler<NameValuePair> soilTextureGradeDataHandler;
	@Autowired
	private SearchTraitDataHandler<String> speciesNameDataHandler;
	
	@PostConstruct
	public void init() {
		this.handlerMap.put(this.soilTextureGradeDataHandler.getSearchTrait(), this.soilTextureGradeDataHandler);
		this.handlerMap.put(this.speciesNameDataHandler.getSearchTrait(), this.speciesNameDataHandler);
	}
	
	@Override
	public Set<?> getSearchTraitData(SearchTrait searchTrait) {
		SearchTraitDataHandler<?> handler = this.handlerMap.get(searchTrait);
		if(handler == null) {
			handler = this.defaultSearchTraitDataHandler;
		}
		return handler.getSearchTraitData();
	}

}
