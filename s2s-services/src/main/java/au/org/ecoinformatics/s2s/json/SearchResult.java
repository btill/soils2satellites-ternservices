package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.org.ecoinformatics.s2s.domain.Observer;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class SearchResult implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer studyLocationId;
	private Integer studyLocationVisitId;
	private String studyLocationName;
	private Date visitEndDate;
	private Date visitStartDate;
	private List<Observer> observers = new ArrayList<Observer>();

	public SearchResult() {
	}
	
	public Integer getStudyLocationId() {
		return studyLocationId;
	}

	public void setStudyLocationId(Integer studyLocationId) {
		this.studyLocationId = studyLocationId;
	}

	public Integer getStudyLocationVisitId() {
		return this.studyLocationVisitId;
	}

	public void setStudyLocationVisitId(Integer studyLocationVisitId) {
		this.studyLocationVisitId = studyLocationVisitId;
	}

	public String getStudyLocationName() {
		return studyLocationName;
	}

	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}

	public Date getVisitEndDate() {
		return this.visitEndDate;
	}

	public void setVisitEndDate(Date visitEndDate) {
		this.visitEndDate = visitEndDate;
	}

	public Date getVisitStartDate() {
		return this.visitStartDate;
	}

	public void setVisitStartDate(Date visitStartDate) {
		this.visitStartDate = visitStartDate;
	}

	public List<Observer> getObservers() {
		return observers;
	}

	public void setObservers(List<Observer> observers) {
		this.observers = observers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((observers == null) ? 0 : observers.hashCode());
		result = prime * result
				+ ((studyLocationId == null) ? 0 : studyLocationId.hashCode());
		result = prime
				* result
				+ ((studyLocationName == null) ? 0 : studyLocationName
						.hashCode());
		result = prime
				* result
				+ ((studyLocationVisitId == null) ? 0 : studyLocationVisitId
						.hashCode());
		result = prime * result
				+ ((visitEndDate == null) ? 0 : visitEndDate.hashCode());
		result = prime * result
				+ ((visitStartDate == null) ? 0 : visitStartDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchResult other = (SearchResult) obj;
		if (observers == null) {
			if (other.observers != null)
				return false;
		} else if (!observers.equals(other.observers))
			return false;
		if (studyLocationId == null) {
			if (other.studyLocationId != null)
				return false;
		} else if (!studyLocationId.equals(other.studyLocationId))
			return false;
		if (studyLocationName == null) {
			if (other.studyLocationName != null)
				return false;
		} else if (!studyLocationName.equals(other.studyLocationName))
			return false;
		if (studyLocationVisitId == null) {
			if (other.studyLocationVisitId != null)
				return false;
		} else if (!studyLocationVisitId.equals(other.studyLocationVisitId))
			return false;
		if (visitEndDate == null) {
			if (other.visitEndDate != null)
				return false;
		} else if (!visitEndDate.equals(other.visitEndDate))
			return false;
		if (visitStartDate == null) {
			if (other.visitStartDate != null)
				return false;
		} else if (!visitStartDate.equals(other.visitStartDate))
			return false;
		return true;
	}

}