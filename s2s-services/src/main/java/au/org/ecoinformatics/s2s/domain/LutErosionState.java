package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_erosion_state database table.
 * 
 */
@Entity
@Table(name="lut_erosion_state")
public class LutErosionState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String state;

	//bi-directional many-to-one association to SiteLocationVisit
	@OneToMany(mappedBy="lutErosionState")
	private Set<SiteLocationVisit> siteLocationVisits;

	public LutErosionState() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Set<SiteLocationVisit> getSiteLocationVisits() {
		return this.siteLocationVisits;
	}

	public void setSiteLocationVisits(Set<SiteLocationVisit> siteLocationVisits) {
		this.siteLocationVisits = siteLocationVisits;
	}

}