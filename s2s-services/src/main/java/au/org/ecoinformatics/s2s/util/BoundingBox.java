package au.org.ecoinformatics.s2s.util;

import java.io.Serializable;

import au.org.ecoinformatics.s2s.json.StudyLocationPoint;

public class BoundingBox implements Serializable {

	private static final long serialVersionUID = -3162837914360185207L;
	
	private final double xMin;
	private final double yMin;
	private final double xMax;
	private final double yMax;
	
	public BoundingBox(double xMin, double yMin, double xMax, double yMax) {
		this.xMax = xMax;
		this.xMin = xMin;
		this.yMax = yMax;
		this.yMin = yMin;
	}
	
	public BoundingBox(String value) throws NumberFormatException {
		String [] bboxPieces = value.split(",");
		if(bboxPieces.length != 4){
			throw new NumberFormatException("Expecting 4 values separated by commas");
		}
		this.xMin = Double.parseDouble(bboxPieces[0]);
		this.yMin = Double.parseDouble(bboxPieces[1]);
		this.xMax = Double.parseDouble(bboxPieces[2]);
		this.yMax = Double.parseDouble(bboxPieces[3]);
	}
	
	public boolean containsPoint(StudyLocationPoint point) {
		if (xMin <= point.getLongitude() && point.getLongitude() <= xMax && 
				yMin <= point.getLatitude() && point.getLatitude() <= yMax) {
			return true;
		}
		return false;
	}

	public boolean isNull() {
		return false;
	}
	
	public double getXMin() {
		return xMin;
	}

	public double getYMin() {
		return yMin;
	}

	public double getXMax() {
		return xMax;
	}

	public double getYMax() {
		return yMax;
	}
	
}
