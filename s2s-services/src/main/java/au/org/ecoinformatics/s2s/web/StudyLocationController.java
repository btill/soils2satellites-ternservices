package au.org.ecoinformatics.s2s.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.org.ecoinformatics.s2s.json.StudyLocation;
import au.org.ecoinformatics.s2s.json.StudyLocationDetails;
import au.org.ecoinformatics.s2s.json.StudyLocationName;
import au.org.ecoinformatics.s2s.json.StudyLocationSpecies;
import au.org.ecoinformatics.s2s.json.StudyLocationVoucher;
import au.org.ecoinformatics.s2s.service.StudyLocationDetailsService;
import au.org.ecoinformatics.s2s.service.StudyLocationService;
import au.org.ecoinformatics.s2s.util.JsonUtilsBean;

@Controller(value="studyLocationController")
public class StudyLocationController {

	@Autowired
	private JsonUtilsBean jsonUtilsBean;
	@Autowired
	private StudyLocationService studyLocationService;
	@Autowired
	private StudyLocationDetailsService studyLocationDetailsService;

	@RequestMapping(value = "/getStudyLocationNameList", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationList() throws IOException {
		List<StudyLocationName> studyLocationList = this.studyLocationService.getStudyLocationNameList();
		return this.jsonUtilsBean.writeValueAsString(studyLocationList);
	}
	
	@RequestMapping(value = "/getStudyLocation/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocation(@PathVariable String identifier, HttpServletResponse response) throws IOException {
		Integer siteLocationId = this.studyLocationService.getStudyLocationId(identifier);
		StudyLocation sl = this.studyLocationService.getStudyLocation(siteLocationId);
		return this.jsonUtilsBean.writeValueAsString(sl);
	}
	
	@RequestMapping(value = "/getStudyLocations", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocations(HttpServletResponse response) throws IOException {
		List<StudyLocation> studyLocations = this.studyLocationService.getStudyLocations();
		return this.jsonUtilsBean.writeValueAsString(studyLocations);
	}
	
	@RequestMapping(value = "/getStudyLocationDetails/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationDetails(@PathVariable String identifier, HttpServletResponse response) throws IOException {
		Integer siteLocationId = this.studyLocationService.getStudyLocationId(identifier);
		StudyLocationDetails sld = this.studyLocationDetailsService.getStudyLocationDetails(siteLocationId);
		return this.jsonUtilsBean.writeValueAsString(sld);
	}
	
	@RequestMapping(value = "/getStudyLocationVouchers/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationVouchers(@PathVariable String identifier, HttpServletResponse response) throws IOException {
		Integer siteLocationId = this.studyLocationService.getStudyLocationId(identifier);
		List<StudyLocationVoucher> vouchers = this.studyLocationService.getStudyLocationVouchers(siteLocationId);
		return this.jsonUtilsBean.writeValueAsString(vouchers);
	}
	
	@RequestMapping(value = "/getStudyLocationSpecies/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getStudyLocationSpecies(@PathVariable String identifier, HttpServletResponse response) throws IOException {
		Integer siteLocationId = this.studyLocationService.getStudyLocationId(identifier);
		StudyLocationSpecies species = this.studyLocationService.getStudyLocationSpecies(siteLocationId);
		return this.jsonUtilsBean.writeValueAsString(species);
	}

}
