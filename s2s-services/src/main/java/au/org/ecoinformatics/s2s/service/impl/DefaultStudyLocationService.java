package au.org.ecoinformatics.s2s.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationConverter;
import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationStudyLocationPointConverter;
import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationVoucherConverter;
import au.org.ecoinformatics.s2s.domain.Observer;
import au.org.ecoinformatics.s2s.domain.SiteLocation;
import au.org.ecoinformatics.s2s.domain.VegVoucher;
import au.org.ecoinformatics.s2s.json.StudyLocation;
import au.org.ecoinformatics.s2s.json.StudyLocationName;
import au.org.ecoinformatics.s2s.json.StudyLocationPoint;
import au.org.ecoinformatics.s2s.json.StudyLocationSpecies;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVoucher;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;
import au.org.ecoinformatics.s2s.repository.ObserverRepository;
import au.org.ecoinformatics.s2s.repository.SiteLocationRepository;
import au.org.ecoinformatics.s2s.repository.VegVoucherRepository;
import au.org.ecoinformatics.s2s.service.StudyLocationPointService;
import au.org.ecoinformatics.s2s.service.StudyLocationService;
import au.org.ecoinformatics.s2s.service.StudyLocationVisitService;

@Service("studyLocationService")
public class DefaultStudyLocationService implements StudyLocationService {

	@Autowired
	private StudyLocationPointService studyLocationPointService;
	@Autowired
	private StudyLocationVisitService studyLocationVisitService;
	
	@Autowired
	private SiteLocationRepository siteLocationRepository;
	@Autowired
	private ObserverRepository observerRepository;
	@Autowired
	private VegVoucherRepository vegVoucherRepository;
	
	@Autowired
	private StudyLocationConverter studyLocationConverter;
	@Autowired
	private StudyLocationStudyLocationPointConverter studyLocationStudyLocationPointConverter;
	@Autowired
	private StudyLocationVoucherConverter studyLocationVoucherConverter;
	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	
	@Override
	@Transactional
	public List<StudyLocationVoucher> getStudyLocationVouchers(Integer studyLocationId) {
		List<VegVoucher> vegVouchers = this.vegVoucherRepository.findForSiteLocation(studyLocationId);
		List<StudyLocationVoucher> slvVouchers = new ArrayList<StudyLocationVoucher>();
		for(VegVoucher vv : vegVouchers) {
			StudyLocationVoucher voucher = this.studyLocationVoucherConverter.convert(vv);
			slvVouchers.add(voucher);
		}
		return slvVouchers;
	}

	@Override
	@Transactional
	public StudyLocationSpecies getStudyLocationSpecies(Integer studyLocationId) {
		SiteLocation sl = this.siteLocationRepository.findOne(studyLocationId);
		List<String> speciesList = this.herbariumDeterminationRepository.findSiteDistinctSpecies(studyLocationId);
		StudyLocationSpecies species = new StudyLocationSpecies();
		species.setStudyLocationId(studyLocationId);
		species.setStudyLocationName(sl.getSiteLocationName());
		species.setSpeciesList(speciesList);
		return species;
	}

	@Override
	@Transactional
	public Integer getStudyLocationId(String identifier) {
		if(identifier.matches("[0-9]+")) {
			return Integer.parseInt(identifier);
		}
		List<Integer>idList = this.siteLocationRepository.findSiteLocationId(identifier);
		Assert.isTrue(idList.size() == 1, "Expecting a single ID value for SiteLocationName: " + identifier);
		return idList.get(0);
	}

	@Override
	public List<Integer> getStudyLocationIdList(String[] identifiers) {
		List<Integer> studyLocationIdList = new ArrayList<Integer>(identifiers.length);
		for(String identifier : identifiers) {
			Integer studyLocationId = getStudyLocationId(identifier);
			studyLocationIdList.add(studyLocationId);
		}
		return studyLocationIdList;
	}

	@Override
	@Transactional
	public List<StudyLocationName> getStudyLocationNameList() {
		List<Object[]> dataList = this.siteLocationRepository.findSiteLocationNameList();
		List<StudyLocationName> slnList = new ArrayList<StudyLocationName>();
		for(Object[] data : dataList) {
			StudyLocationName sln = new StudyLocationName();
			sln.setStudyLocationId((Integer) data[0]);
			sln.setStudyLocationName((String) data[1]);
			slnList.add(sln);
		}
		return slnList;
	}
	
	@Override
	@Transactional
	public List<StudyLocation> getStudyLocations() {
		List<StudyLocation> studyLocationList = new ArrayList<StudyLocation>();
		for(Integer siteLocationId : this.siteLocationRepository.findSiteLocationIdList()) {
			studyLocationList.add(getStudyLocation(siteLocationId));
		}
		return studyLocationList;
	}
	
	@Override
	@Transactional
	public StudyLocation getStudyLocation(Integer siteLocationId) {
		SiteLocation sl = this.siteLocationRepository.findOne(siteLocationId);
		Assert.notNull(sl, "No Site Location exists for siteLocationId: " + siteLocationId);
		List<Observer> observers = this.observerRepository.findDistinctObservers(siteLocationId);
		StudyLocationVisit slvFirst = this.studyLocationVisitService.getFirstStudyLocationVisit(siteLocationId);
		StudyLocationVisit slvLast = this.studyLocationVisitService.getLastStudyLocationVisit(siteLocationId);
		StudyLocationPoint markerPoint = this.studyLocationPointService.getStudyLocationMarkerPoint(siteLocationId);
		
		StudyLocation studyLocation = this.studyLocationConverter.convert(sl);
		studyLocation = this.studyLocationStudyLocationPointConverter.convert(studyLocation, markerPoint);
		studyLocation.setFirstVisit(slvFirst.getVisitStartDate());
		studyLocation.setLastVisit(slvLast.getVisitStartDate());
		studyLocation.setObservers(observers);
		return studyLocation;
	}

}
