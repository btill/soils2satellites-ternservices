package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the site_location database table.
 * 
 */
@Entity
@Table(name="site_location")
public class SiteLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="site_location_id", unique=true, nullable=false)
	private Integer siteLocationId;

	@Column(name="app_lat", length=2147483647)
	private String appLat;

	@Column(name="app_long", length=2147483647)
	private String appLong;

	@Column(name="bool_test")
	private Boolean boolTest;

	@Column(length=2147483647)
	private String comments;

	@Column(length=2147483647)
	private String description;

	@Column(name="established_date")
	private Timestamp establishedDate;

	@Column(name="other_outcrop_lithology", length=2147483647)
	private String otherOutcropLithology;

	@Column(name="outcrop_lithology", length=2147483647)
	private String outcropLithology;

	@Column(length=2147483647)
	private String paddock;

	@Column(name="plot_dimensions", length=2147483647)
	private String plotDimensions;

	@Column(name="plot_is_100m_by_100m")
	private Boolean plotIs100mBy100m;

	@Column(name="plot_is_aligned_to_grid")
	private Boolean plotIsAlignedToGrid;

	@Column(name="plot_is_permanently_marked")
	private Boolean plotIsPermanentlyMarked;

	@Column(length=2147483647)
	private String property;

	@Column(name="site_aspect")
	private Integer siteAspect;

	@Column(name="site_location_name", length=2147483647)
	private String siteLocationName;

	@Column(name="site_slope")
	private Integer siteSlope;

	//bi-directional many-to-one association to LutIbra
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="bioregion_name", nullable=false)
	private LutIbra lutIbra;

	//bi-directional many-to-one association to LutLandformElement
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="landform_element", nullable=false)
	private LutLandformElement lutLandformElement;

	//bi-directional many-to-one association to LutLandformPattern
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="landform_pattern", nullable=false)
	private LutLandformPattern lutLandformPattern;

	//bi-directional many-to-one association to LutMgaZone
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="mga_zone", nullable=false)
	private LutMgaZone lutMgaZone;

	//bi-directional many-to-one association to SiteLocationPoint
	@OneToMany(mappedBy="siteLocation")
	private Set<SiteLocationPoint> siteLocationPoints = new HashSet<SiteLocationPoint>();

	//bi-directional many-to-one association to SiteLocationVisit
	@OneToMany(mappedBy="siteLocation")
	private Set<SiteLocationVisit> siteLocationVisits;

	public SiteLocation() {
	}

	public Integer getSiteLocationId() {
		return this.siteLocationId;
	}

	public void setSiteLocationId(Integer siteLocationId) {
		this.siteLocationId = siteLocationId;
	}

	public String getAppLat() {
		return this.appLat;
	}

	public void setAppLat(String appLat) {
		this.appLat = appLat;
	}

	public String getAppLong() {
		return this.appLong;
	}

	public void setAppLong(String appLong) {
		this.appLong = appLong;
	}

	public Boolean getBoolTest() {
		return this.boolTest;
	}

	public void setBoolTest(Boolean boolTest) {
		this.boolTest = boolTest;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEstablishedDate() {
		return this.establishedDate;
	}

	public void setEstablishedDate(Timestamp establishedDate) {
		this.establishedDate = establishedDate;
	}

	public String getOtherOutcropLithology() {
		return this.otherOutcropLithology;
	}

	public void setOtherOutcropLithology(String otherOutcropLithology) {
		this.otherOutcropLithology = otherOutcropLithology;
	}

	public String getOutcropLithology() {
		return this.outcropLithology;
	}

	public void setOutcropLithology(String outcropLithology) {
		this.outcropLithology = outcropLithology;
	}

	public String getPaddock() {
		return this.paddock;
	}

	public void setPaddock(String paddock) {
		this.paddock = paddock;
	}

	public String getPlotDimensions() {
		return this.plotDimensions;
	}

	public void setPlotDimensions(String plotDimensions) {
		this.plotDimensions = plotDimensions;
	}

	public Boolean getPlotIs100mBy100m() {
		return this.plotIs100mBy100m;
	}

	public void setPlotIs100mBy100m(Boolean plotIs100mBy100m) {
		this.plotIs100mBy100m = plotIs100mBy100m;
	}

	public Boolean getPlotIsAlignedToGrid() {
		return this.plotIsAlignedToGrid;
	}

	public void setPlotIsAlignedToGrid(Boolean plotIsAlignedToGrid) {
		this.plotIsAlignedToGrid = plotIsAlignedToGrid;
	}

	public Boolean getPlotIsPermanentlyMarked() {
		return this.plotIsPermanentlyMarked;
	}

	public void setPlotIsPermanentlyMarked(Boolean plotIsPermanentlyMarked) {
		this.plotIsPermanentlyMarked = plotIsPermanentlyMarked;
	}

	public String getProperty() {
		return this.property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public Integer getSiteAspect() {
		return this.siteAspect;
	}

	public void setSiteAspect(Integer siteAspect) {
		this.siteAspect = siteAspect;
	}

	public String getSiteLocationName() {
		return this.siteLocationName;
	}

	public void setSiteLocationName(String siteLocationName) {
		this.siteLocationName = siteLocationName;
	}

	public Integer getSiteSlope() {
		return this.siteSlope;
	}

	public void setSiteSlope(Integer siteSlope) {
		this.siteSlope = siteSlope;
	}

	public LutIbra getLutIbra() {
		return this.lutIbra;
	}

	public void setLutIbra(LutIbra lutIbra) {
		this.lutIbra = lutIbra;
	}

	public LutLandformElement getLutLandformElement() {
		return this.lutLandformElement;
	}

	public void setLutLandformElement(LutLandformElement lutLandformElement) {
		this.lutLandformElement = lutLandformElement;
	}

	public LutLandformPattern getLutLandformPattern() {
		return this.lutLandformPattern;
	}

	public void setLutLandformPattern(LutLandformPattern lutLandformPattern) {
		this.lutLandformPattern = lutLandformPattern;
	}

	public LutMgaZone getLutMgaZone() {
		return this.lutMgaZone;
	}

	public void setLutMgaZone(LutMgaZone lutMgaZone) {
		this.lutMgaZone = lutMgaZone;
	}

	public Set<SiteLocationPoint> getSiteLocationPoints() {
		return this.siteLocationPoints;
	}

	public void setSiteLocationPoints(Set<SiteLocationPoint> siteLocationPoints) {
		this.siteLocationPoints = siteLocationPoints;
	}

	public Set<SiteLocationVisit> getSiteLocationVisits() {
		return this.siteLocationVisits;
	}

	public void setSiteLocationVisits(Set<SiteLocationVisit> siteLocationVisits) {
		this.siteLocationVisits = siteLocationVisits;
	}

}