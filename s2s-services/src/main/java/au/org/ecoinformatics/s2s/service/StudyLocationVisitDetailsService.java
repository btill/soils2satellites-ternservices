package au.org.ecoinformatics.s2s.service;

import au.org.ecoinformatics.s2s.json.SamplingUnit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitDetails;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnitDetails;

public interface StudyLocationVisitDetailsService {

	StudyLocationVisitDetails getStudyLocationVisitDetails(Integer studyLocationVisitId);
	
	StudyLocationVisitSamplingUnitDetails getStudyLocationSamplingUnitDetails(Integer studyLocationVisitId, SamplingUnit samplingUnit);
}