package au.org.ecoinformatics.s2s.converter.samplingunit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import au.org.ecoinformatics.s2s.converter.KeyedConverter;
import au.org.ecoinformatics.s2s.domain.HerbariumDetermination;
import au.org.ecoinformatics.s2s.json.PointIntercept;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;
import au.org.ecoinformatics.s2s.util.ConvertUtils;

@Component
public class PointInterceptConverter implements KeyedConverter<PointIntercept, au.org.ecoinformatics.s2s.domain.PointIntercept> {

	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	
	@Override
	public PointIntercept convert(au.org.ecoinformatics.s2s.domain.PointIntercept value) {
		return convert(new PointIntercept(), value);
	}

	@Override
	public PointIntercept convert(PointIntercept initialValue, au.org.ecoinformatics.s2s.domain.PointIntercept value) {
		initialValue.setId(value.getId());
		initialValue.setDead(value.getDead());
		initialValue.setGrowthForm((String) ConvertUtils.getProperty(value, "lutGrowthForm.growthForm"));
		initialValue.setHeight(value.getHeight());
		initialValue.setInCanopySky(value.getInCanopySky());
		initialValue.setMethod(value.getMethod());
		initialValue.setPointInterceptId(value.getId());
		initialValue.setPointNumber(value.getPointNumber());
		initialValue.setSubstrate((String) ConvertUtils.getProperty(value, "lutSubstrate.substrate"));
		initialValue.setTransect(value.getTransect());
		initialValue.setVegBarcode(value.getVegBarcode());
		initialValue.setHerbariumDetermination(getHerbariumDetermination(value.getVegBarcode()));
		return initialValue;
	}
	
	private String getHerbariumDetermination(String vegBarcode) {
		if(!StringUtils.hasText(vegBarcode)) {
			return null;
		}
		HerbariumDetermination hb = this.herbariumDeterminationRepository.findOne(vegBarcode);
		if(hb == null) {
			return null;
		}
		return hb.getHerbariumDetermination();
	}

	@Override
	public Class<PointIntercept> getOutputClass() {
		return PointIntercept.class;
	}

	@Override
	public Class<au.org.ecoinformatics.s2s.domain.PointIntercept> getInputClass() {
		return au.org.ecoinformatics.s2s.domain.PointIntercept.class;
	}

}
