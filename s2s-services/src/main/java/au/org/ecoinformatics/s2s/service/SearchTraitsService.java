package au.org.ecoinformatics.s2s.service;

import java.util.List;
import java.util.Set;

import au.org.ecoinformatics.s2s.json.SearchTrait;

public interface SearchTraitsService {
	
	Set<?> getSearchTraitData(SearchTrait searchTrait);
	
	List<SearchTrait> getSearchTraits();
}
