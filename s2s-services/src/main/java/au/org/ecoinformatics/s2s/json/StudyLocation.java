package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.org.ecoinformatics.s2s.domain.Observer;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocation implements Serializable {

	private static final long serialVersionUID = 63465764186040317L;
	
	private Integer studyLocationId;
	private String studyLocationName;
	private Date firstVisit;
	private Date lastVisit;
	private Double longitude;
	private Double latitude;
	private Double easting;
	private Double northing;
	private Integer mgaZone;
	private List<Observer> observers = new ArrayList<Observer>();
	
	public StudyLocation() {}

	public Integer getStudyLocationId() {
		return studyLocationId;
	}

	public void setStudyLocationId(Integer studyLocationId) {
		this.studyLocationId = studyLocationId;
	}

	public String getStudyLocationName() {
		return studyLocationName;
	}

	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}

	public Date getFirstVisit() {
		return firstVisit;
	}

	public void setFirstVisit(Date firstVisit) {
		this.firstVisit = firstVisit;
	}

	public Date getLastVisit() {
		return lastVisit;
	}

	public void setLastVisit(Date lastVisit) {
		this.lastVisit = lastVisit;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getEasting() {
		return easting;
	}

	public void setEasting(Double easting) {
		this.easting = easting;
	}

	public Double getNorthing() {
		return northing;
	}

	public void setNorthing(Double northing) {
		this.northing = northing;
	}

	public Integer getMgaZone() {
		return mgaZone;
	}

	public void setMgaZone(Integer mgaZone) {
		this.mgaZone = mgaZone;
	}

	public List<Observer> getObservers() {
		return observers;
	}

	public void setObservers(List<Observer> observers) {
		this.observers = observers;
	}
	
	
}
