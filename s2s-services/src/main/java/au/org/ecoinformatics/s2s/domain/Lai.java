package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the lai database table.
 * 
 */
@Entity
@Table(name="lai")
public class Lai implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(name="completion_date_time")
	private Timestamp completionDateTime;

	@Column(name="location_of_lai_data", length=2147483647)
	private String locationOfLaiData;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public Lai() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getCompletionDateTime() {
		return this.completionDateTime;
	}

	public void setCompletionDateTime(Timestamp completionDateTime) {
		this.completionDateTime = completionDateTime;
	}

	public String getLocationOfLaiData() {
		return this.locationOfLaiData;
	}

	public void setLocationOfLaiData(String locationOfLaiData) {
		this.locationOfLaiData = locationOfLaiData;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}