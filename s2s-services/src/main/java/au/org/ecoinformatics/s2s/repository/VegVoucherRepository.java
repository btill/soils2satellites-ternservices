package au.org.ecoinformatics.s2s.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import au.org.ecoinformatics.s2s.domain.VegVoucher;

@Repository("vegVoucherRepository")
public interface VegVoucherRepository extends JpaRepository<VegVoucher, Integer> {

	@Query("SELECT vv " +
	       "FROM VegVoucher vv " +
		   "WHERE vv.siteLocationVisit.siteLocation.siteLocationId = ?1 " +
	       "ORDER BY vv.herbariumDetermination.herbariumDetermination ASC")
	List<VegVoucher> findForSiteLocation(Integer siteLocationId);
//	
	@Query("SELECT vv " +
	       "FROM VegVoucher vv " +
		   "WHERE vv.siteLocationVisit.siteLocationVisitId = ?1 " +
		   "ORDER BY vv.herbariumDetermination.herbariumDetermination ASC")
	List<VegVoucher> findForSiteLocationVisit(Integer siteLocationVisitId);
}
