package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_disturbance database table.
 * 
 */
@Entity
@Table(name="lut_disturbance")
public class LutDisturbance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String disturbance;

	//bi-directional many-to-one association to SiteLocationVisit
	@OneToMany(mappedBy="lutDisturbance")
	private Set<SiteLocationVisit> siteLocationVisits;

	public LutDisturbance() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisturbance() {
		return this.disturbance;
	}

	public void setDisturbance(String disturbance) {
		this.disturbance = disturbance;
	}

	public Set<SiteLocationVisit> getSiteLocationVisits() {
		return this.siteLocationVisits;
	}

	public void setSiteLocationVisits(Set<SiteLocationVisit> siteLocationVisits) {
		this.siteLocationVisits = siteLocationVisits;
	}

}