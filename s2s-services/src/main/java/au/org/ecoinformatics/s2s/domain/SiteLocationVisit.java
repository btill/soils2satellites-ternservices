package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the site_location_visit database table.
 * 
 */
@Entity
@Table(name="site_location_visit")
public class SiteLocationVisit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="site_location_visit_id", unique=true, nullable=false)
	private Integer siteLocationVisitId;

	@Column(name="a_s_c", length=2147483647)
	private String aSC;

	@Column(name="basal_wedge_has_been_collected")
	private Boolean basalWedgeHasBeenCollected;

	@Column(name="climatic_condition", length=2147483647)
	private String climaticCondition;

	@Column(name="described_by", length=2147483647)
	private String describedBy;

	@Column(name="leaf_area_index_exists")
	private Boolean leafAreaIndexExists;

	@Column(name="location_description", length=2147483647)
	private String locationDescription;

	@Column(name="observer_soil", length=2147483647)
	private String observerSoil;

	@Column(name="observer_veg", length=2147483647)
	private String observerVeg;

	@Column(name="ok_to_publish")
	private Boolean okToPublish;

	@Column(name="photopoints_exists")
	private Boolean photopointsExists;

	@Column(name="pit_marker_datum", length=2147483647)
	private String pitMarkerDatum;

	@Column(name="pit_marker_easting")
	private Double pitMarkerEasting;

	@Column(name="pit_marker_location_method", length=2147483647)
	private String pitMarkerLocationMethod;

	@Column(name="pit_marker_mga_zones")
	private Integer pitMarkerMgaZones;

	@Column(name="pit_marker_northing")
	private Double pitMarkerNorthing;

	@Column(name="surface_coarse_frags_abundance", length=2147483647)
	private String surfaceCoarseFragsAbundance;

	@Column(name="surface_coarse_frags_lithology", length=2147483647)
	private String surfaceCoarseFragsLithology;

	@Column(name="surface_coarse_frags_size", length=2147483647)
	private String surfaceCoarseFragsSize;

	@Column(name="surface_coarse_frags_type", length=2147483647)
	private String surfaceCoarseFragsType;

	@Column(name="vegetation_condition", length=2147483647)
	private String vegetationCondition;

	@Column(name="visit_end_date")
	private Timestamp visitEndDate;

	@Column(name="visit_notes", length=2147483647)
	private String visitNotes;

	@Column(name="visit_start_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date visitStartDate;

	//bi-directional many-to-one association to BasalArea
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<BasalArea> basalAreas;

	//bi-directional many-to-one association to GeneticVoucher
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<GeneticVoucher> geneticVouchers;

	//bi-directional many-to-one association to Lai
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<Lai> lais;

	//bi-directional many-to-one association to Observer
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<Observer> observers;

	//bi-directional many-to-one association to Photopoint
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<Photopoint> photopoints;

	//bi-directional many-to-one association to PointIntercept
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<PointIntercept> pointIntercepts = new HashSet<PointIntercept>();

	//bi-directional many-to-one association to LutDisturbance
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="disturbance", nullable=false)
	private LutDisturbance lutDisturbance;

	//bi-directional many-to-one association to LutDrainage
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="drainage_type", nullable=false)
	private LutDrainage lutDrainage;

	//bi-directional many-to-one association to LutErosionAbund
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="erosion_abundance", nullable=false)
	private LutErosionAbund lutErosionAbund;

	//bi-directional many-to-one association to LutErosionState
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="erosion_state", nullable=false)
	private LutErosionState lutErosionState;

	//bi-directional many-to-one association to LutErosionType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="erosion_type", nullable=false)
	private LutErosionType lutErosionType;

	//bi-directional many-to-one association to LutMicrorelief
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="microrelief", nullable=false)
	private LutMicrorelief lutMicrorelief;

	//bi-directional many-to-one association to LutSoilObsType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="soil_observation_type", nullable=false)
	private LutSoilObsType lutSoilObsType;

	//bi-directional many-to-one association to SiteLocation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_id", nullable=false)
	private SiteLocation siteLocation;

	//bi-directional many-to-one association to SoilBulkDensity
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<SoilBulkDensity> soilBulkDensities;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<SoilCharacterisation> soilCharacterisations = new HashSet<SoilCharacterisation>();

	//bi-directional many-to-one association to SoilSubsiteObservation
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<SoilSubsiteObservation> soilSubsiteObservations; // = new HashSet<SoilSubsiteObservation>();

	//bi-directional many-to-one association to StructuralSummary
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<StructuralSummary> structuralSummaries;

	//bi-directional many-to-one association to VegVoucher
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<VegVoucher> vegVouchers = new HashSet<VegVoucher>();

	//bi-directional many-to-one association to VisitSurfaceSoilCondition
	@OneToMany(mappedBy="siteLocationVisit")
	private Set<VisitSurfaceSoilCondition> visitSurfaceSoilConditions;

	public SiteLocationVisit() {
	}

	public Integer getSiteLocationVisitId() {
		return this.siteLocationVisitId;
	}

	public void setSiteLocationVisitId(Integer siteLocationVisitId) {
		this.siteLocationVisitId = siteLocationVisitId;
	}

	public String getASC() {
		return this.aSC;
	}

	public void setASC(String aSC) {
		this.aSC = aSC;
	}

	public Boolean getBasalWedgeHasBeenCollected() {
		return this.basalWedgeHasBeenCollected;
	}

	public void setBasalWedgeHasBeenCollected(Boolean basalWedgeHasBeenCollected) {
		this.basalWedgeHasBeenCollected = basalWedgeHasBeenCollected;
	}

	public String getClimaticCondition() {
		return this.climaticCondition;
	}

	public void setClimaticCondition(String climaticCondition) {
		this.climaticCondition = climaticCondition;
	}

	public String getDescribedBy() {
		return this.describedBy;
	}

	public void setDescribedBy(String describedBy) {
		this.describedBy = describedBy;
	}

	public Boolean getLeafAreaIndexExists() {
		return this.leafAreaIndexExists;
	}

	public void setLeafAreaIndexExists(Boolean leafAreaIndexExists) {
		this.leafAreaIndexExists = leafAreaIndexExists;
	}

	public String getLocationDescription() {
		return this.locationDescription;
	}

	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}

	public String getObserverSoil() {
		return this.observerSoil;
	}

	public void setObserverSoil(String observerSoil) {
		this.observerSoil = observerSoil;
	}

	public String getObserverVeg() {
		return this.observerVeg;
	}

	public void setObserverVeg(String observerVeg) {
		this.observerVeg = observerVeg;
	}

	public Boolean getOkToPublish() {
		return this.okToPublish;
	}

	public void setOkToPublish(Boolean okToPublish) {
		this.okToPublish = okToPublish;
	}

	public Boolean getPhotopointsExists() {
		return this.photopointsExists;
	}

	public void setPhotopointsExists(Boolean photopointsExists) {
		this.photopointsExists = photopointsExists;
	}

	public String getPitMarkerDatum() {
		return this.pitMarkerDatum;
	}

	public void setPitMarkerDatum(String pitMarkerDatum) {
		this.pitMarkerDatum = pitMarkerDatum;
	}

	public Double getPitMarkerEasting() {
		return this.pitMarkerEasting;
	}

	public void setPitMarkerEasting(Double pitMarkerEasting) {
		this.pitMarkerEasting = pitMarkerEasting;
	}

	public String getPitMarkerLocationMethod() {
		return this.pitMarkerLocationMethod;
	}

	public void setPitMarkerLocationMethod(String pitMarkerLocationMethod) {
		this.pitMarkerLocationMethod = pitMarkerLocationMethod;
	}

	public Integer getPitMarkerMgaZones() {
		return this.pitMarkerMgaZones;
	}

	public void setPitMarkerMgaZones(Integer pitMarkerMgaZones) {
		this.pitMarkerMgaZones = pitMarkerMgaZones;
	}

	public Double getPitMarkerNorthing() {
		return this.pitMarkerNorthing;
	}

	public void setPitMarkerNorthing(Double pitMarkerNorthing) {
		this.pitMarkerNorthing = pitMarkerNorthing;
	}

	public String getSurfaceCoarseFragsAbundance() {
		return this.surfaceCoarseFragsAbundance;
	}

	public void setSurfaceCoarseFragsAbundance(String surfaceCoarseFragsAbundance) {
		this.surfaceCoarseFragsAbundance = surfaceCoarseFragsAbundance;
	}

	public String getSurfaceCoarseFragsLithology() {
		return this.surfaceCoarseFragsLithology;
	}

	public void setSurfaceCoarseFragsLithology(String surfaceCoarseFragsLithology) {
		this.surfaceCoarseFragsLithology = surfaceCoarseFragsLithology;
	}

	public String getSurfaceCoarseFragsSize() {
		return this.surfaceCoarseFragsSize;
	}

	public void setSurfaceCoarseFragsSize(String surfaceCoarseFragsSize) {
		this.surfaceCoarseFragsSize = surfaceCoarseFragsSize;
	}

	public String getSurfaceCoarseFragsType() {
		return this.surfaceCoarseFragsType;
	}

	public void setSurfaceCoarseFragsType(String surfaceCoarseFragsType) {
		this.surfaceCoarseFragsType = surfaceCoarseFragsType;
	}

	public String getVegetationCondition() {
		return this.vegetationCondition;
	}

	public void setVegetationCondition(String vegetationCondition) {
		this.vegetationCondition = vegetationCondition;
	}

	public Timestamp getVisitEndDate() {
		return this.visitEndDate;
	}

	public void setVisitEndDate(Timestamp visitEndDate) {
		this.visitEndDate = visitEndDate;
	}

	public String getVisitNotes() {
		return this.visitNotes;
	}

	public void setVisitNotes(String visitNotes) {
		this.visitNotes = visitNotes;
	}

	public Date getVisitStartDate() {
		return this.visitStartDate;
	}

	public void setVisitStartDate(Date visitStartDate) {
		this.visitStartDate = visitStartDate;
	}

	public Set<BasalArea> getBasalAreas() {
		return this.basalAreas;
	}

	public void setBasalAreas(Set<BasalArea> basalAreas) {
		this.basalAreas = basalAreas;
	}

	public Set<GeneticVoucher> getGeneticVouchers() {
		return this.geneticVouchers;
	}

	public void setGeneticVouchers(Set<GeneticVoucher> geneticVouchers) {
		this.geneticVouchers = geneticVouchers;
	}

	public Set<Lai> getLais() {
		return this.lais;
	}

	public void setLais(Set<Lai> lais) {
		this.lais = lais;
	}

	public Set<Observer> getObservers() {
		return this.observers;
	}

	public void setObservers(Set<Observer> observers) {
		this.observers = observers;
	}

	public Set<Photopoint> getPhotopoints() {
		return this.photopoints;
	}

	public void setPhotopoints(Set<Photopoint> photopoints) {
		this.photopoints = photopoints;
	}

	public Set<PointIntercept> getPointIntercepts() {
		return this.pointIntercepts;
	}

	public void setPointIntercepts(Set<PointIntercept> pointIntercepts) {
		this.pointIntercepts = pointIntercepts;
	}

	public LutDisturbance getLutDisturbance() {
		return this.lutDisturbance;
	}

	public void setLutDisturbance(LutDisturbance lutDisturbance) {
		this.lutDisturbance = lutDisturbance;
	}

	public LutDrainage getLutDrainage() {
		return this.lutDrainage;
	}

	public void setLutDrainage(LutDrainage lutDrainage) {
		this.lutDrainage = lutDrainage;
	}

	public LutErosionAbund getLutErosionAbund() {
		return this.lutErosionAbund;
	}

	public void setLutErosionAbund(LutErosionAbund lutErosionAbund) {
		this.lutErosionAbund = lutErosionAbund;
	}

	public LutErosionState getLutErosionState() {
		return this.lutErosionState;
	}

	public void setLutErosionState(LutErosionState lutErosionState) {
		this.lutErosionState = lutErosionState;
	}

	public LutErosionType getLutErosionType() {
		return this.lutErosionType;
	}

	public void setLutErosionType(LutErosionType lutErosionType) {
		this.lutErosionType = lutErosionType;
	}

	public LutMicrorelief getLutMicrorelief() {
		return this.lutMicrorelief;
	}

	public void setLutMicrorelief(LutMicrorelief lutMicrorelief) {
		this.lutMicrorelief = lutMicrorelief;
	}

	public LutSoilObsType getLutSoilObsType() {
		return this.lutSoilObsType;
	}

	public void setLutSoilObsType(LutSoilObsType lutSoilObsType) {
		this.lutSoilObsType = lutSoilObsType;
	}

	public SiteLocation getSiteLocation() {
		return this.siteLocation;
	}

	public void setSiteLocation(SiteLocation siteLocation) {
		this.siteLocation = siteLocation;
	}

	public Set<SoilBulkDensity> getSoilBulkDensities() {
		return this.soilBulkDensities;
	}

	public void setSoilBulkDensities(Set<SoilBulkDensity> soilBulkDensities) {
		this.soilBulkDensities = soilBulkDensities;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

	public Set<SoilSubsiteObservation> getSoilSubsiteObservations() {
		return this.soilSubsiteObservations;
	}

	public void setSoilSubsiteObservations(Set<SoilSubsiteObservation> soilSubsiteObservations) {
		this.soilSubsiteObservations = soilSubsiteObservations;
	}

	public Set<StructuralSummary> getStructuralSummaries() {
		return this.structuralSummaries;
	}

	public void setStructuralSummaries(Set<StructuralSummary> structuralSummaries) {
		this.structuralSummaries = structuralSummaries;
	}

	public Set<VegVoucher> getVegVouchers() {
		return this.vegVouchers;
	}

	public void setVegVouchers(Set<VegVoucher> vegVouchers) {
		this.vegVouchers = vegVouchers;
	}

	public Set<VisitSurfaceSoilCondition> getVisitSurfaceSoilConditions() {
		return this.visitSurfaceSoilConditions;
	}

	public void setVisitSurfaceSoilConditions(Set<VisitSurfaceSoilCondition> visitSurfaceSoilConditions) {
		this.visitSurfaceSoilConditions = visitSurfaceSoilConditions;
	}

}