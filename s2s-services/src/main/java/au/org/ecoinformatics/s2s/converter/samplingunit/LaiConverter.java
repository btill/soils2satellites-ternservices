package au.org.ecoinformatics.s2s.converter.samplingunit;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.KeyedConverter;
import au.org.ecoinformatics.s2s.json.Lai;

@Component
public class LaiConverter implements KeyedConverter<Lai, au.org.ecoinformatics.s2s.domain.Lai> {
	
	@Override
	public Lai convert(au.org.ecoinformatics.s2s.domain.Lai value) {
		return convert(new Lai(), value);
	}

	@Override
	public Lai convert(Lai initialValue, au.org.ecoinformatics.s2s.domain.Lai value) {
		initialValue.setCompletionDateTime(value.getCompletionDateTime());
		initialValue.setId(value.getId());
		initialValue.setLocationOfLaiData(value.getLocationOfLaiData());
		return initialValue;
	}

	@Override
	public Class<Lai> getOutputClass() {
		return Lai.class;
	}

	@Override
	public Class<au.org.ecoinformatics.s2s.domain.Lai> getInputClass() {
		return au.org.ecoinformatics.s2s.domain.Lai.class;
	}

}
