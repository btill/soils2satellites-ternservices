package au.org.ecoinformatics.s2s.converter.datatype;

import java.util.Date;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.HerbariumPointIntercept;

@Component
public class HerbariumPointInterceptConverter implements Converter<HerbariumPointIntercept, Object[]> {

	@Override
	public HerbariumPointIntercept convert(Object[] value) {
		return convert(new HerbariumPointIntercept(), value);
	}

	@Override
	public HerbariumPointIntercept convert(HerbariumPointIntercept initialValue, Object[] value) {
		initialValue.setSiteLocationId((Integer) value[0]);
		initialValue.setSiteLocationName((String) value[1]);
		initialValue.setSiteLocationVisitId((Integer) value[2]);
		initialValue.setVisitStartDate((Date) value[3]);
		initialValue.setPointInterceptId((Integer) value[4]);
		initialValue.setHerbariumDetermination((String) value[5]);
		return initialValue;
	}

}
