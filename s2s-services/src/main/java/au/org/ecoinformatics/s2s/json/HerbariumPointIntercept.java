package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class HerbariumPointIntercept implements Serializable {

	private static final long serialVersionUID = -1823801837849728877L;
	
	private Integer siteLocationId;
	private String siteLocationName;
	private Integer siteLocationVisitId;
	private Date visitStartDate;
	private Integer pointInterceptId;
	private String herbariumDetermination;
	
	public Integer getSiteLocationId() {
		return siteLocationId;
	}
	public void setSiteLocationId(Integer siteLocationId) {
		this.siteLocationId = siteLocationId;
	}
	public String getSiteLocationName() {
		return siteLocationName;
	}
	public void setSiteLocationName(String siteLocationName) {
		this.siteLocationName = siteLocationName;
	}
	public Integer getSiteLocationVisitId() {
		return siteLocationVisitId;
	}
	public void setSiteLocationVisitId(Integer siteLocationVisitId) {
		this.siteLocationVisitId = siteLocationVisitId;
	}
	public Date getVisitStartDate() {
		return visitStartDate;
	}
	public void setVisitStartDate(Date visitStartDate) {
		this.visitStartDate = visitStartDate;
	}
	public Integer getPointInterceptId() {
		return pointInterceptId;
	}
	public void setPointInterceptId(Integer pointInterceptId) {
		this.pointInterceptId = pointInterceptId;
	}
	public String getHerbariumDetermination() {
		return herbariumDetermination;
	}
	public void setHerbariumDetermination(String herbariumDetermination) {
		this.herbariumDetermination = herbariumDetermination;
	}
	
	

}
