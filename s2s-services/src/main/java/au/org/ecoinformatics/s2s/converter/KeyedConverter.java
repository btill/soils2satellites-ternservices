package au.org.ecoinformatics.s2s.converter;

public interface KeyedConverter<T, K> extends Converter<T, K> {

	Class<T> getOutputClass();
	Class<K> getInputClass();
}
