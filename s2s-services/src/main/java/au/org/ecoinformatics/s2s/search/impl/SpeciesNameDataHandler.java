package au.org.ecoinformatics.s2s.search.impl;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;
import au.org.ecoinformatics.s2s.search.SearchTraitDataHandler;

@Component
public class SpeciesNameDataHandler implements SearchTraitDataHandler<String> {

	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	
	@Override
	public Set<String> getSearchTraitData() {
		List<String> speciesList = this.herbariumDeterminationRepository.findDistinctSpecies();
		Set<String> speciesSet = new LinkedHashSet<String>();
		speciesSet.addAll(speciesList);
		return speciesSet;
	}

	@Override
	public SearchTrait getSearchTrait() {
		return SearchTrait.SPECIES_NAME;
	}

}
