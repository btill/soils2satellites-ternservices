package au.org.ecoinformatics.s2s.converter;


public interface Converter<T, K> {

	T convert(K value);
	T convert(T initialValue, K value);

}
