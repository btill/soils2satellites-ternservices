package au.org.ecoinformatics.s2s.converter.studylocation;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.SiteLocation;
import au.org.ecoinformatics.s2s.json.StudyLocation;

@Component
public class StudyLocationConverter implements Converter<StudyLocation, SiteLocation> {

	@Override
	public StudyLocation convert(SiteLocation value) {
		return convert(new StudyLocation(), value);
	}

	@Override
	public StudyLocation convert(StudyLocation initialValue, SiteLocation value) {
		initialValue.setStudyLocationName(value.getSiteLocationName());
		initialValue.setStudyLocationId(value.getSiteLocationId());
		return initialValue;
	}

}
