package au.org.ecoinformatics.s2s.service;

import java.util.List;
import java.util.Map;

import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;

public interface SearchService {

	List<StudyLocationVisit> searchSiteVisits(String searchExpression);
	
	List<SearchTrait> getSearchTraits();
	
	Map<String, String> getFieldMappings();
}
