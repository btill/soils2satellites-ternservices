package au.org.ecoinformatics.s2s.search;

import java.util.Set;

import au.org.ecoinformatics.s2s.json.SearchTrait;

public interface SearchTraitDataService {

	Set<?> getSearchTraitData(SearchTrait searchTrait);
}
