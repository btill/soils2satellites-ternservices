package au.org.ecoinformatics.s2s.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import au.org.ecoinformatics.s2s.domain.LutLandformElement;

@Repository("lutLandformElementRepository")
public interface LutLandformElementRepository extends JpaRepository<LutLandformElement, String> {

	@Query("SELECT sl.siteLocationId, sl.siteLocationName, lle.landformElement " +
	       "FROM SiteLocation sl, LutLandformElement lle " +
	       "WHERE sl.lutLandformElement.code = lle.code " +
	       "AND sl.siteLocationId IN (?1)")
	List<Object[]> findLutLandFormElements(List<Integer> siteLocationIds);

    @Query("SELECT lle.landformElement, COUNT(sl.siteLocationName) as siteCount, " + 
           "  (SELECT COUNT(sl1.siteLocationId) FROM SiteLocation sl1 " +
    	   "   WHERE sl1.siteLocationId IN (?1)) as totalCount " +
           "FROM SiteLocation sl, LutLandformElement lle " +
    	   "WHERE sl.lutLandformElement.code = lle.code " +
    	   "AND sl.siteLocationId IN (?1) " +
    	   "GROUP BY lle.landformElement")
    List<Object[]> findLutLandFormElementTotals(List<Integer> siteLocationIds);

}