package au.org.ecoinformatics.s2s.converter.studylocationvisit;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.Observer;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitDetails;

@Component
public class StudyLocationVisitDetailsConverter implements Converter<StudyLocationVisitDetails, SiteLocationVisit> {
	
	@Override
	public StudyLocationVisitDetails convert(SiteLocationVisit value) {
		return convert(new StudyLocationVisitDetails(), value);
	}

	@Override
	public StudyLocationVisitDetails convert(StudyLocationVisitDetails slvd, SiteLocationVisit value) {
		slvd.setStudyLocationVisitId(value.getSiteLocationVisitId());
		slvd.setVisitEndDate(value.getVisitEndDate());
		slvd.setVisitNotes(value.getVisitNotes());
		slvd.setVisitStartDate(value.getVisitStartDate());
		slvd.setStudyLocationName(value.getSiteLocation().getSiteLocationName());
		slvd.setLocationDescription(value.getLocationDescription());
		slvd.setClimaticCondition(value.getClimaticCondition());
		slvd.setVegetationCondition(value.getVegetationCondition());
		slvd.setSurfaceCoarseFragsAbundance(value.getSurfaceCoarseFragsAbundance());
		slvd.setSurfaceCoarseFragsLithology(value.getSurfaceCoarseFragsLithology());
		slvd.setSurfaceCoarseFragsSize(value.getSurfaceCoarseFragsSize());
		slvd.setSurfaceCoarseFragsType(value.getSurfaceCoarseFragsType());
		slvd.setPitMarkerDatum(value.getPitMarkerDatum());
		slvd.setPitMarkerEasting(value.getPitMarkerEasting());
		slvd.setPitMarkerLocationMethod(value.getPitMarkerLocationMethod());
		slvd.setPitMarkerMgaZones(value.getPitMarkerMgaZones());
		slvd.setPitMarkerNorthing(value.getPitMarkerNorthing());
		slvd.setErosionType(value.getLutErosionType().getErosionType());
		slvd.setErosionState(value.getLutErosionState().getState());
		slvd.setErosionAbundance(value.getLutErosionAbund().getAbundance());
		slvd.setDisturbance(value.getLutDisturbance().getDisturbance());
		slvd.setSoilObservationType(value.getLutSoilObsType().getType());
		slvd.setMicrorelief(value.getLutMicrorelief().getType());
		slvd.setDrainageType(value.getLutDrainage().getDrainage());
		return convertObservers(slvd, value);
	}
	
	private StudyLocationVisitDetails convertObservers(StudyLocationVisitDetails slvd, SiteLocationVisit value) {
		String[] observerNames = {value.getObserverSoil(), value.getObserverVeg()};
		List<Observer> jsonObservers = new ArrayList<Observer>();
		for(String observerName : observerNames) {
			if(null == observerName) {
				continue;
			}
			Observer o = new Observer();
			o.setObserverName(observerName);
			jsonObservers.add(o);
		}
		slvd.setObservers(jsonObservers);
		return slvd;
	}

}
