package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the veg_vouchers database table.
 * 
 */
@Entity
@Table(name="veg_vouchers")
public class VegVoucher implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="veg_barcode", unique=true, nullable=false, length=2147483647)
	private String vegBarcode;

	@Column(name="field_name", length=2147483647)
	private String fieldName;

	//bi-directional many-to-one association to BasalArea
	@OneToMany(mappedBy="vegVoucher")
	private Set<BasalArea> basalAreas;

	//bi-directional one-to-one association to HerbariumDetermination
	@OneToOne(mappedBy="vegVoucher", fetch=FetchType.LAZY)
	private HerbariumDetermination herbariumDetermination;

	//bi-directional many-to-one association to HerbariumSheet
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="herbarium_sheet_id")
	private HerbariumSheet herbariumSheet;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public VegVoucher() {
	}

	public String getVegBarcode() {
		return this.vegBarcode;
	}

	public void setVegBarcode(String vegBarcode) {
		this.vegBarcode = vegBarcode;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Set<BasalArea> getBasalAreas() {
		return this.basalAreas;
	}

	public void setBasalAreas(Set<BasalArea> basalAreas) {
		this.basalAreas = basalAreas;
	}

	public HerbariumDetermination getHerbariumDetermination() {
		return this.herbariumDetermination;
	}

	public void setHerbariumDetermination(HerbariumDetermination herbariumDetermination) {
		this.herbariumDetermination = herbariumDetermination;
	}

	public HerbariumSheet getHerbariumSheet() {
		return this.herbariumSheet;
	}

	public void setHerbariumSheet(HerbariumSheet herbariumSheet) {
		this.herbariumSheet = herbariumSheet;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}