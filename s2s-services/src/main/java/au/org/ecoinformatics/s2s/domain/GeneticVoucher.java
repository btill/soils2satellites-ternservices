package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the genetic_vouchers database table.
 * 
 */
@Entity
@Table(name="genetic_vouchers")
public class GeneticVoucher implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="veg_barcode", unique=true, nullable=false, length=2147483647)
	private String vegBarcode;

	@Column(name="primary_gen_barcode", length=2147483647)
	private String primaryGenBarcode;

	@Column(name="sample_for_dna", length=2147483647)
	private String sampleForDna;

	@Column(name="sample_for_isotope", length=2147483647)
	private String sampleForIsotope;

	@Column(name="secondary_gen_barcode_1", length=2147483647)
	private String secondaryGenBarcode1;

	@Column(name="secondary_gen_barcode_2", length=2147483647)
	private String secondaryGenBarcode2;

	@Column(name="secondary_gen_barcode_3", length=2147483647)
	private String secondaryGenBarcode3;

	@Column(name="secondary_gen_barcode_4", length=2147483647)
	private String secondaryGenBarcode4;

	@Column(name="tea_bag_is_synthetic", length=2147483647)
	private String teaBagIsSynthetic;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public GeneticVoucher() {
	}

	public String getVegBarcode() {
		return this.vegBarcode;
	}

	public void setVegBarcode(String vegBarcode) {
		this.vegBarcode = vegBarcode;
	}

	public String getPrimaryGenBarcode() {
		return this.primaryGenBarcode;
	}

	public void setPrimaryGenBarcode(String primaryGenBarcode) {
		this.primaryGenBarcode = primaryGenBarcode;
	}

	public String getSampleForDna() {
		return this.sampleForDna;
	}

	public void setSampleForDna(String sampleForDna) {
		this.sampleForDna = sampleForDna;
	}

	public String getSampleForIsotope() {
		return this.sampleForIsotope;
	}

	public void setSampleForIsotope(String sampleForIsotope) {
		this.sampleForIsotope = sampleForIsotope;
	}

	public String getSecondaryGenBarcode1() {
		return this.secondaryGenBarcode1;
	}

	public void setSecondaryGenBarcode1(String secondaryGenBarcode1) {
		this.secondaryGenBarcode1 = secondaryGenBarcode1;
	}

	public String getSecondaryGenBarcode2() {
		return this.secondaryGenBarcode2;
	}

	public void setSecondaryGenBarcode2(String secondaryGenBarcode2) {
		this.secondaryGenBarcode2 = secondaryGenBarcode2;
	}

	public String getSecondaryGenBarcode3() {
		return this.secondaryGenBarcode3;
	}

	public void setSecondaryGenBarcode3(String secondaryGenBarcode3) {
		this.secondaryGenBarcode3 = secondaryGenBarcode3;
	}

	public String getSecondaryGenBarcode4() {
		return this.secondaryGenBarcode4;
	}

	public void setSecondaryGenBarcode4(String secondaryGenBarcode4) {
		this.secondaryGenBarcode4 = secondaryGenBarcode4;
	}

	public String getTeaBagIsSynthetic() {
		return this.teaBagIsSynthetic;
	}

	public void setTeaBagIsSynthetic(String teaBagIsSynthetic) {
		this.teaBagIsSynthetic = teaBagIsSynthetic;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}