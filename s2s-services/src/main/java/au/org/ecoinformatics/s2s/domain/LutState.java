package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_state database table.
 * 
 */
@Entity
@Table(name="lut_state")
public class LutState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String state;

	@Column(nullable=false, length=2147483647)
	private String description;

	//bi-directional many-to-one association to HerbariumSheet
	@OneToMany(mappedBy="lutState")
	private Set<HerbariumSheet> herbariumSheets;

	public LutState() {
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<HerbariumSheet> getHerbariumSheets() {
		return this.herbariumSheets;
	}

	public void setHerbariumSheets(Set<HerbariumSheet> herbariumSheets) {
		this.herbariumSheets = herbariumSheets;
	}

}