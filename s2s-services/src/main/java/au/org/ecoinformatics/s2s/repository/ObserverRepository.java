package au.org.ecoinformatics.s2s.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import au.org.ecoinformatics.s2s.domain.Observer;

public interface ObserverRepository extends JpaRepository<Observer, Integer> {

	@Query("SELECT DISTINCT o " +
	       "FROM Observer o, SiteLocationVisit slv " +
	       "WHERE o.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
	       "AND slv.siteLocation.siteLocationId = ?1")
	List<Observer> findDistinctObservers(Integer siteLocationId);
	
	@Query("SELECT DISTINCT o " +
	       "FROM Observer o, SiteLocationVisit slv " +
	       "WHERE o.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
	       "AND slv.siteLocationVisitId = ?1")
	List<Observer> findSiteVisitDistinctObservers(Integer siteLocationVisitId);
}
