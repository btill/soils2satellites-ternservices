package au.org.ecoinformatics.s2s.converter.samplingunit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.KeyedConverter;
import au.org.ecoinformatics.s2s.domain.HerbariumDetermination;
import au.org.ecoinformatics.s2s.json.VegVoucher;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;

@Component
public class VegVouchersConverter implements KeyedConverter<VegVoucher, au.org.ecoinformatics.s2s.domain.VegVoucher> {

	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	
	@Override
	public VegVoucher convert(au.org.ecoinformatics.s2s.domain.VegVoucher value) {
		return convert(new VegVoucher(), value);
	}

	@Override
	public VegVoucher convert(VegVoucher initialValue, au.org.ecoinformatics.s2s.domain.VegVoucher value) {
		HerbariumDetermination hb = getHerbariumDetermination(value.getVegBarcode());
		initialValue.setDeterminationDate(hb.getDeterminationDate());
		initialValue.setDeterminer(hb.getDeterminer());
		initialValue.setFieldName(value.getFieldName());
		initialValue.setHerbariumDetermination(hb.getHerbariumDetermination());
		initialValue.setStateAccessionNumber(hb.getStateAccessionNumber());
		initialValue.setVegBarcode(value.getVegBarcode());
		return initialValue;
	}
	
	private HerbariumDetermination getHerbariumDetermination(String vegBarcode) {
		HerbariumDetermination hb = this.herbariumDeterminationRepository.findOne(vegBarcode);
		if(hb == null) {
			return new HerbariumDetermination();
		}
		return hb;
	}

	@Override
	public Class<VegVoucher> getOutputClass() {
		return VegVoucher.class;
	}

	@Override
	public Class<au.org.ecoinformatics.s2s.domain.VegVoucher> getInputClass() {
		return au.org.ecoinformatics.s2s.domain.VegVoucher.class;
	}

}
