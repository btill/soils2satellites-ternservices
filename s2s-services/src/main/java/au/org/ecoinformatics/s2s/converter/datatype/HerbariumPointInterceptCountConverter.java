package au.org.ecoinformatics.s2s.converter.datatype;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.HerbariumPointInterceptCount;

@Component
public class HerbariumPointInterceptCountConverter implements Converter<HerbariumPointInterceptCount, Object[]> {

	@Override
	public HerbariumPointInterceptCount convert(Object[] value) {
		return convert(new HerbariumPointInterceptCount(), value);
	}

	@Override
	public HerbariumPointInterceptCount convert(HerbariumPointInterceptCount initialValue, Object[] value) {
		initialValue.setSiteLocationId((Integer) value[0]);
		initialValue.setSiteLocationName((String) value[1]);
		initialValue.setSiteLocationVisitId((Integer) value[2]);
		initialValue.setPointInterceptCount((Long) value[3]);
		initialValue.setHerbariumDetermination((String) value[4]);
		return initialValue;
	}

}
