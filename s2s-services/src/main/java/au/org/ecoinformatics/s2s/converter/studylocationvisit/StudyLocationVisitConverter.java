package au.org.ecoinformatics.s2s.converter.studylocationvisit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.Observer;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;

@Component
public class StudyLocationVisitConverter implements Converter<StudyLocationVisit, SiteLocationVisit> {
	
	@Override
	public StudyLocationVisit convert(SiteLocationVisit value) {
		return convert(new StudyLocationVisit(), value);
	}

	@Override
	public StudyLocationVisit convert(StudyLocationVisit slv, SiteLocationVisit value) {
		slv.setStudyLocationId(value.getSiteLocation().getSiteLocationId());
		slv.setStudyLocationVisitId(value.getSiteLocationVisitId());
		slv.setVisitEndDate(new Date(value.getVisitEndDate().getTime()));
		slv.setVisitStartDate(new Date(value.getVisitStartDate().getTime()));
		slv.setStudyLocationName(value.getSiteLocation().getSiteLocationName());
		return convertObservers(slv, value);
	}
	
	private StudyLocationVisit convertObservers(StudyLocationVisit slv, SiteLocationVisit value) {
		String[] observerNames = {value.getObserverSoil(), value.getObserverVeg()};
		List<Observer> jsonObservers = new ArrayList<Observer>();
		for(String observerName : observerNames) {
			if(null == observerName) {
				continue;
			}
			Observer o = new Observer();
			o.setObserverName(observerName);
			jsonObservers.add(o);
		}
		slv.setObservers(jsonObservers);
		return slv;
	}

}
