package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocationSpecies implements Serializable {

	private static final long serialVersionUID = 4866214501058500072L;
	
	private Integer studyLocationId;
	private String studyLocationName;
	private List<String> speciesList = new ArrayList<String>();
	
	public Integer getStudyLocationId() {
		return studyLocationId;
	}
	public void setStudyLocationId(Integer studyLocationId) {
		this.studyLocationId = studyLocationId;
	}
	public String getStudyLocationName() {
		return studyLocationName;
	}
	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}
	public List<String> getSpeciesList() {
		return speciesList;
	}
	public void setSpeciesList(List<String> speciesList) {
		this.speciesList = speciesList;
	}
	
}
