package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocationVisitSpecies implements Serializable {

	private static final long serialVersionUID = 4866214501058500072L;
	
	private Integer studyLocationVisitId;
	private List<String> speciesList = new ArrayList<String>();
	
	public Integer getStudyLocationVisitId() {
		return studyLocationVisitId;
	}
	public void setStudyLocationVisitId(Integer studyLocationVisitId) {
		this.studyLocationVisitId = studyLocationVisitId;
	}
	public List<String> getSpeciesList() {
		return speciesList;
	}
	public void setSpeciesList(List<String> speciesList) {
		this.speciesList = speciesList;
	}
	
}
