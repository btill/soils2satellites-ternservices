package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_erosion_type database table.
 * 
 */
@Entity
@Table(name="lut_erosion_type")
public class LutErosionType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(name="erosion_type", length=2147483647)
	private String erosionType;

	//bi-directional many-to-one association to SiteLocationVisit
	@OneToMany(mappedBy="lutErosionType")
	private Set<SiteLocationVisit> siteLocationVisits;

	public LutErosionType() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getErosionType() {
		return this.erosionType;
	}

	public void setErosionType(String erosionType) {
		this.erosionType = erosionType;
	}

	public Set<SiteLocationVisit> getSiteLocationVisits() {
		return this.siteLocationVisits;
	}

	public void setSiteLocationVisits(Set<SiteLocationVisit> siteLocationVisits) {
		this.siteLocationVisits = siteLocationVisits;
	}

}