package au.org.ecoinformatics.s2s.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import au.org.ecoinformatics.s2s.domain.HerbariumDetermination;


@Repository("herbariumDeterminationRepository")
public interface HerbariumDeterminationRepository extends JpaRepository<HerbariumDetermination, String> {

	@Query("SELECT COUNT(DISTINCT hd.herbariumDetermination) " +
	           "FROM VegVoucher vv, " +
	           "     SiteLocationVisit slv, " +
	           "     SiteLocation sl, " +
	           "     HerbariumDetermination hd " +
	           "WHERE vv.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
	           "AND vv.vegBarcode = hd.vegBarcode " +
	           "AND slv.siteLocation.siteLocationId = sl.siteLocationId " +
	           "AND sl.siteLocationId = ?1")
	Long findDistinctSpeciesCount(Integer siteLocationId);
	
	@Query("SELECT DISTINCT hd.herbariumDetermination " +
           "FROM VegVoucher vv, " +
           "     SiteLocationVisit slv, " +
           "     SiteLocation sl, " +
           "     HerbariumDetermination hd " +
           "WHERE vv.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
           "AND vv.vegBarcode = hd.vegBarcode " +
           "AND slv.siteLocation.siteLocationId = sl.siteLocationId " +
           "AND sl.siteLocationId = ?1 " +
           "AND hd.herbariumDetermination != NULL " +
           "AND hd.herbariumDetermination != '' " +
           "ORDER BY hd.herbariumDetermination ASC ")
	List<String> findSiteDistinctSpecies(Integer siteLocationId);
	
	@Query("SELECT DISTINCT hd.herbariumDetermination " +
           "FROM VegVoucher vv, " +
           "     SiteLocationVisit slv, " +
           "     HerbariumDetermination hd " +
           "WHERE vv.siteLocationVisit.siteLocationVisitId = ?1 " +
           "AND vv.vegBarcode = hd.vegBarcode " +
           "AND hd.herbariumDetermination != NULL " +
           "AND hd.herbariumDetermination != '' " +
           "ORDER BY hd.herbariumDetermination ASC ")
	List<String> findSiteVisitDistinctSpecies(Integer siteLocationVisitId);
	
	@Query("SELECT DISTINCT hd.herbariumDetermination " +
           "FROM VegVoucher vv, " +
           "     SiteLocationVisit slv, " +
           "     SiteLocation sl, " +
           "     HerbariumDetermination hd " +
           "WHERE vv.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
           "AND vv.vegBarcode = hd.vegBarcode " +
           "AND slv.siteLocation.siteLocationId = sl.siteLocationId " +
           "AND hd.herbariumDetermination != NULL " +
           "AND hd.herbariumDetermination != '' " +
           "ORDER BY hd.herbariumDetermination ASC ")
	List<String> findDistinctSpecies();
	
	@Query("SELECT sl.siteLocationId, sl.siteLocationName, COUNT(DISTINCT hd.herbariumDetermination) as distinctSpeciesCount " +
           "FROM VegVoucher vv, HerbariumDetermination hd, SiteLocationVisit slv, SiteLocation sl " +
           "WHERE vv.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
           "AND vv.vegBarcode = hd.vegBarcode " +
           "AND slv.siteLocation.siteLocationId = sl.siteLocationId " +
           "AND sl.siteLocationId IN (?1) " +
           "GROUP BY sl.siteLocationId, sl.siteLocationName " +
           "ORDER BY sl.siteLocationName")
	List<Object[]> findDistinctSpeciesCountList(List<Integer> siteLocationIds);
	
	@Query(value="SELECT sl.siteLocationId, sl.siteLocationName, slv.siteLocationVisitId, slv.visitStartDate, pi.id, hd.herbariumDetermination " +
				 "FROM SiteLocationVisit slv, SiteLocation sl, PointIntercept pi, VegVoucher vv, HerbariumDetermination hd " +
				 "WHERE slv.siteLocation.siteLocationId = sl.siteLocationId " +
				 "AND pi.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
				 "AND pi.vegBarcode = vv.vegBarcode " +
				 "AND hd.vegBarcode = vv.vegBarcode " +
				 "AND sl.siteLocationId = ?1 " +
				 "AND slv.visitStartDate = ?2")
	List<Object[]> findHerbariumSiteLocationPointIntercepts(Integer siteLocationId, Date visitStartDate);
	
	@Query(value="SELECT sl.siteLocationId, sl.siteLocationName, slv.siteLocationVisitId, COUNT(pi.id), hd.herbariumDetermination " +
			     "FROM SiteLocationVisit slv, HerbariumDetermination hd, PointIntercept pi, SiteLocation sl " +
			     "WHERE slv.siteLocation.siteLocationId = sl.siteLocationId " +
			     "AND pi.vegBarcode = hd.vegBarcode " +
			     "AND pi.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
			     "GROUP BY sl.siteLocationId, sl.siteLocationName, slv.siteLocationVisitId, hd.herbariumDetermination")
	List<Object[]> findHerbariumPointInterceptCount();
	
	@Query("SELECT hd.herbariumDetermination, COUNT(pi.id) AS speciesCount " +
		   "FROM SiteLocationVisit slv, StructuralSummary ss, PointIntercept pi, VegVoucher vv, HerbariumDetermination hd " +
		   "WHERE pi.vegBarcode = vv.vegBarcode " +
		   "AND hd.vegBarcode = vv.vegBarcode " +
		   "AND pi.siteLocationVisit.siteLocationVisitId = ?1 " +
		   "AND slv.siteLocationVisitId = ss.siteLocationVisit.siteLocationVisitId " +
		   "AND (ss.upper1Dominant = hd.herbariumDetermination OR ss.upper1Dominant IS NULL) " +
		   "AND slv.siteLocationVisitId = ?1 " +
		   "GROUP BY hd.herbariumDetermination ")
	List<Object[]> findHerbariumSpeciesCount(Integer siteLocationVisitId);
}
