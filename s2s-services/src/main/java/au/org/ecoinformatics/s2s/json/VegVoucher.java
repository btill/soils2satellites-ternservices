package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder(alphabetic=true)
public class VegVoucher implements Serializable {
	private static final long serialVersionUID = 1L;

	private String vegBarcode;
	private String fieldName;
	private Timestamp determinationDate;
	private String determiner;
	private String herbariumDetermination;
	private String stateAccessionNumber;

	public VegVoucher() {
	}

	public String getVegBarcode() {
		return vegBarcode;
	}

	public void setVegBarcode(String vegBarcode) {
		this.vegBarcode = vegBarcode;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Timestamp getDeterminationDate() {
		return determinationDate;
	}

	public void setDeterminationDate(Timestamp determinationDate) {
		this.determinationDate = determinationDate;
	}

	public String getDeterminer() {
		return determiner;
	}

	public void setDeterminer(String determiner) {
		this.determiner = determiner;
	}

	public String getHerbariumDetermination() {
		return herbariumDetermination;
	}

	public void setHerbariumDetermination(String herbariumDetermination) {
		this.herbariumDetermination = herbariumDetermination;
	}

	public String getStateAccessionNumber() {
		return stateAccessionNumber;
	}

	public void setStateAccessionNumber(String stateAccessionNumber) {
		this.stateAccessionNumber = stateAccessionNumber;
	}

	
}