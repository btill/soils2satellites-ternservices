package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_microrelief database table.
 * 
 */
@Entity
@Table(name="lut_microrelief")
public class LutMicrorelief implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(length=2147483647)
	private String description;

	@Column(length=2147483647)
	private String type;

	//bi-directional many-to-one association to SiteLocationVisit
	@OneToMany(mappedBy="lutMicrorelief")
	private Set<SiteLocationVisit> siteLocationVisits;

	public LutMicrorelief() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<SiteLocationVisit> getSiteLocationVisits() {
		return this.siteLocationVisits;
	}

	public void setSiteLocationVisits(Set<SiteLocationVisit> siteLocationVisits) {
		this.siteLocationVisits = siteLocationVisits;
	}

}