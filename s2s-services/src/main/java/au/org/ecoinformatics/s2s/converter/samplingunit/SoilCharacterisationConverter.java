package au.org.ecoinformatics.s2s.converter.samplingunit;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.KeyedConverter;
import au.org.ecoinformatics.s2s.json.SoilCharacterisation;
import au.org.ecoinformatics.s2s.util.ConvertUtils;

@Component
public class SoilCharacterisationConverter implements KeyedConverter<SoilCharacterisation, au.org.ecoinformatics.s2s.domain.SoilCharacterisation> {

	@Override
	public SoilCharacterisation convert(au.org.ecoinformatics.s2s.domain.SoilCharacterisation value) {
		return convert(new SoilCharacterisation(), value);
	}

	@Override
	public SoilCharacterisation convert(SoilCharacterisation initialValue, au.org.ecoinformatics.s2s.domain.SoilCharacterisation value) {
		initialValue.setId(value.getId());
		initialValue.setCoarseFragsAbundance((String) ConvertUtils.getProperty(value,  "lutCoarseFragAbund.type"));
		initialValue.setCoarseFragsLithology(value.getCoarseFragsLithology());
		initialValue.setCoarseFragsShape((String) ConvertUtils.getProperty(value, "lutCoarseFragShape.type"));
		initialValue.setCoarseFragsSize((String) ConvertUtils.getProperty(value, "lutCoarseFragSize.size"));
		initialValue.setCollectedBy(value.getCollectedBy());
		initialValue.setColourWhenDry(value.getColourWhenDry());
		initialValue.setColourWhenMoist(value.getColourWhenMoist());
		initialValue.setComments(value.getComments());
		initialValue.setEc(value.getEc());
		initialValue.setEffervescence((String) ConvertUtils.getProperty(value, "lutEffervescence.effervescence"));
		initialValue.setHorizon(value.getHorizon());
		initialValue.setLayerNumber(value.getLayerNumber());
		initialValue.setLowerDepth(value.getLowerDepth());
		initialValue.setMottlesAbundance((String) ConvertUtils.getProperty(value, "lutMottleAbund.abundance"));
		initialValue.setMottlesColour((String) ConvertUtils.getProperty(value, "lutMottleColour.colour"));
		initialValue.setMottleSize((String) ConvertUtils.getProperty(value, "lutMottleSize.size"));
		initialValue.setNextSize1(value.getNextSize1());
		initialValue.setNextSize2(value.getNextSize2());
		initialValue.setNextSizeType1(value.getNextSizeType1());
		initialValue.setNextSizeType2(value.getNextSizeType2());
		initialValue.setPedalityFabric((String) ConvertUtils.getProperty(value, "lutPedalityFabric.pedalityFabric"));
		initialValue.setPedalityGrade((String) ConvertUtils.getProperty(value, "lutPedalityGrade.grade"));
		initialValue.setPedalityType((String) ConvertUtils.getProperty(value, "lutPedalityType.pedalityType"));
		initialValue.setPh(value.getPh());
		initialValue.setSegregationForm((String) ConvertUtils.getProperty(value, "lutSegForm.form"));
		initialValue.setSegregationNature((String) ConvertUtils.getProperty(value, "lutSegNature.nature"));
		initialValue.setSegregationsAbundance((String) ConvertUtils.getProperty(value, "lutSegAbundance.abundance"));
		initialValue.setSegregationSize((String) ConvertUtils.getProperty(value, "lutSegSize.size"));
		initialValue.setSmallestSize1(value.getSmallestSize1());
		initialValue.setSmallestSize1(value.getSmallestSize2());
		initialValue.setSmallestSizeType1(value.getSmallestSizeType1());
		initialValue.setSmallestSizeType2(value.getSmallestSizeType2());
		initialValue.setTextureGrade((String) ConvertUtils.getProperty(value, "lutSoilTexGrade.name"));
		initialValue.setTextureModifier((String) ConvertUtils.getProperty(value, "lutSoilTexMod.modifier"));
		initialValue.setTextureQualifier((String) ConvertUtils.getProperty(value, "lutSoilTexQual.qualification"));
		initialValue.setUpperDepth(value.getUpperDepth());
		return initialValue;
	}
	
	@Override
	public Class<SoilCharacterisation> getOutputClass() {
		return SoilCharacterisation.class;
	}

	@Override
	public Class<au.org.ecoinformatics.s2s.domain.SoilCharacterisation> getInputClass() {
		return au.org.ecoinformatics.s2s.domain.SoilCharacterisation.class;
	}

}
