package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocationVisitVoucher implements Serializable {

	private static final long serialVersionUID = 4866214501058500072L;
	
	private Integer studyLocationVisitId;
	private String vegBarcode;
	private String fieldName;
	private String herbariumDetermination;
	
	public Integer getStudyLocationVisitId() {
		return studyLocationVisitId;
	}
	public void setStudyLocationVisitId(Integer studyLocationVisitId) {
		this.studyLocationVisitId = studyLocationVisitId;
	}
	public String getVegBarcode() {
		return vegBarcode;
	}
	public void setVegBarcode(String vegBarcode) {
		this.vegBarcode = vegBarcode;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getHerbariumDetermination() {
		return herbariumDetermination;
	}
	public void setHerbariumDetermination(String herbariumDetermination) {
		this.herbariumDetermination = herbariumDetermination;
	}
	
}
