package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the point_intercept database table.
 * 
 */
@Entity
@Table(name="point_intercept")
public class PointIntercept implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	private Boolean dead;

	private Double height;

	@Column(name="in_canopy_sky")
	private Boolean inCanopySky;

	@Column(length=2147483647)
	private String method;

	@Column(name="point_number")
	private Double pointNumber;

	@Column(length=2147483647)
	private String transect;

	@Column(name="veg_barcode", length=2147483647)
	private String vegBarcode;

	//bi-directional many-to-one association to LutGrowthForm
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="growth_form")
	private LutGrowthForm lutGrowthForm;

	//bi-directional many-to-one association to LutSubstrate
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="substrate", nullable=false)
	private LutSubstrate lutSubstrate;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public PointIntercept() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getDead() {
		return this.dead;
	}

	public void setDead(Boolean dead) {
		this.dead = dead;
	}

	public Double getHeight() {
		return this.height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Boolean getInCanopySky() {
		return this.inCanopySky;
	}

	public void setInCanopySky(Boolean inCanopySky) {
		this.inCanopySky = inCanopySky;
	}

	public String getMethod() {
		return this.method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Double getPointNumber() {
		return this.pointNumber;
	}

	public void setPointNumber(Double pointNumber) {
		this.pointNumber = pointNumber;
	}

	public String getTransect() {
		return this.transect;
	}

	public void setTransect(String transect) {
		this.transect = transect;
	}

	public String getVegBarcode() {
		return this.vegBarcode;
	}

	public void setVegBarcode(String vegBarcode) {
		this.vegBarcode = vegBarcode;
	}

	public LutGrowthForm getLutGrowthForm() {
		return this.lutGrowthForm;
	}

	public void setLutGrowthForm(LutGrowthForm lutGrowthForm) {
		this.lutGrowthForm = lutGrowthForm;
	}

	public LutSubstrate getLutSubstrate() {
		return this.lutSubstrate;
	}

	public void setLutSubstrate(LutSubstrate lutSubstrate) {
		this.lutSubstrate = lutSubstrate;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}