package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class PointIntercept implements Serializable {

	private static final long serialVersionUID = 3987149212411811739L;

	private Integer id;
	private Integer pointInterceptId;
	private Boolean dead;
	private Double height;
	private Boolean inCanopySky;
	private String method;
	private Double pointNumber;
	private String transect;
	private String vegBarcode;
	private String growthForm;
	private String substrate;
	private String herbariumDetermination;
	
	public PointIntercept() { }
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPointInterceptId() {
		return pointInterceptId;
	}

	public void setPointInterceptId(Integer pointInterceptId) {
		this.pointInterceptId = pointInterceptId;
	}

	public Boolean getDead() {
		return dead;
	}

	public void setDead(Boolean dead) {
		this.dead = dead;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Boolean getInCanopySky() {
		return inCanopySky;
	}

	public void setInCanopySky(Boolean inCanopySky) {
		this.inCanopySky = inCanopySky;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Double getPointNumber() {
		return pointNumber;
	}

	public void setPointNumber(Double pointNumber) {
		this.pointNumber = pointNumber;
	}

	public String getTransect() {
		return transect;
	}

	public void setTransect(String transect) {
		this.transect = transect;
	}

	public String getVegBarcode() {
		return vegBarcode;
	}

	public void setVegBarcode(String vegBarcode) {
		this.vegBarcode = vegBarcode;
	}

	public String getGrowthForm() {
		return growthForm;
	}

	public void setGrowthForm(String growthForm) {
		this.growthForm = growthForm;
	}

	public String getSubstrate() {
		return substrate;
	}

	public void setSubstrate(String substrate) {
		this.substrate = substrate;
	}

	public String getHerbariumDetermination() {
		return herbariumDetermination;
	}

	public void setHerbariumDetermination(String herbariumDetermination) {
		this.herbariumDetermination = herbariumDetermination;
	}

}
