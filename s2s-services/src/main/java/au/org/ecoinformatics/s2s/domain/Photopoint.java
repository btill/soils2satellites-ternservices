package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the photopoints database table.
 * 
 */
@Entity
@Table(name="photopoints")
public class Photopoint implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(name="completion_date_time")
	private Timestamp completionDateTime;

	@Column(name="photo_storage_location", length=2147483647)
	private String photoStorageLocation;

	@Column(name="photopoint_centre_peg_lat", length=2147483647)
	private String photopointCentrePegLat;

	@Column(name="photopoint_centre_peg_long", length=2147483647)
	private String photopointCentrePegLong;

	@Column(name="photopoint_lat_1", length=2147483647)
	private String photopointLat1;

	@Column(name="photopoint_lat_2", length=2147483647)
	private String photopointLat2;

	@Column(name="photopoint_lat_3", length=2147483647)
	private String photopointLat3;

	@Column(name="photopoint_long_1", length=2147483647)
	private String photopointLong1;

	@Column(name="photopoint_long_2", length=2147483647)
	private String photopointLong2;

	@Column(name="photopoint_long_3", length=2147483647)
	private String photopointLong3;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public Photopoint() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getCompletionDateTime() {
		return this.completionDateTime;
	}

	public void setCompletionDateTime(Timestamp completionDateTime) {
		this.completionDateTime = completionDateTime;
	}

	public String getPhotoStorageLocation() {
		return this.photoStorageLocation;
	}

	public void setPhotoStorageLocation(String photoStorageLocation) {
		this.photoStorageLocation = photoStorageLocation;
	}

	public String getPhotopointCentrePegLat() {
		return this.photopointCentrePegLat;
	}

	public void setPhotopointCentrePegLat(String photopointCentrePegLat) {
		this.photopointCentrePegLat = photopointCentrePegLat;
	}

	public String getPhotopointCentrePegLong() {
		return this.photopointCentrePegLong;
	}

	public void setPhotopointCentrePegLong(String photopointCentrePegLong) {
		this.photopointCentrePegLong = photopointCentrePegLong;
	}

	public String getPhotopointLat1() {
		return this.photopointLat1;
	}

	public void setPhotopointLat1(String photopointLat1) {
		this.photopointLat1 = photopointLat1;
	}

	public String getPhotopointLat2() {
		return this.photopointLat2;
	}

	public void setPhotopointLat2(String photopointLat2) {
		this.photopointLat2 = photopointLat2;
	}

	public String getPhotopointLat3() {
		return this.photopointLat3;
	}

	public void setPhotopointLat3(String photopointLat3) {
		this.photopointLat3 = photopointLat3;
	}

	public String getPhotopointLong1() {
		return this.photopointLong1;
	}

	public void setPhotopointLong1(String photopointLong1) {
		this.photopointLong1 = photopointLong1;
	}

	public String getPhotopointLong2() {
		return this.photopointLong2;
	}

	public void setPhotopointLong2(String photopointLong2) {
		this.photopointLong2 = photopointLong2;
	}

	public String getPhotopointLong3() {
		return this.photopointLong3;
	}

	public void setPhotopointLong3(String photopointLong3) {
		this.photopointLong3 = photopointLong3;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}