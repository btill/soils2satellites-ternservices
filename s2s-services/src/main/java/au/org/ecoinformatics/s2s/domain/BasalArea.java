package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the basal_area database table.
 * 
 */
@Entity
@Table(name="basal_area")
public class BasalArea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(name="basal_area")
	private Double basalArea;

	@Column(name="basal_area_factor")
	private Double basalAreaFactor;

	@Column(nullable=false)
	private Integer hits;

	//bi-directional many-to-one association to LutBasalPoint
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="point_id", nullable=false)
	private LutBasalPoint lutBasalPoint;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	//bi-directional many-to-one association to VegVoucher
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="veg_barcode", nullable=false)
	private VegVoucher vegVoucher;

	public BasalArea() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getBasalArea() {
		return this.basalArea;
	}

	public void setBasalArea(Double basalArea) {
		this.basalArea = basalArea;
	}

	public Double getBasalAreaFactor() {
		return this.basalAreaFactor;
	}

	public void setBasalAreaFactor(Double basalAreaFactor) {
		this.basalAreaFactor = basalAreaFactor;
	}

	public Integer getHits() {
		return this.hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}

	public LutBasalPoint getLutBasalPoint() {
		return this.lutBasalPoint;
	}

	public void setLutBasalPoint(LutBasalPoint lutBasalPoint) {
		this.lutBasalPoint = lutBasalPoint;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

	public VegVoucher getVegVoucher() {
		return this.vegVoucher;
	}

	public void setVegVoucher(VegVoucher vegVoucher) {
		this.vegVoucher = vegVoucher;
	}

}