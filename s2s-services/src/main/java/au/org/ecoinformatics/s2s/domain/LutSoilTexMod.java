package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_soil_tex_mod database table.
 * 
 */
@Entity
@Table(name="lut_soil_tex_mod")
public class LutSoilTexMod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String modifier;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutSoilTexMod")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutSoilTexMod() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModifier() {
		return this.modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}