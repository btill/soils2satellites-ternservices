package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_soil_tex_grade database table.
 * 
 */
@Entity
@Table(name="lut_soil_tex_grade")
public class LutSoilTexGrade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String code;

	@Column(name="bolus_description", nullable=false, length=2147483647)
	private String bolusDescription;

	@Column(name="clay_content", nullable=false, length=2147483647)
	private String clayContent;

	@Column(nullable=false, length=2147483647)
	private String name;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutSoilTexGrade")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutSoilTexGrade() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBolusDescription() {
		return this.bolusDescription;
	}

	public void setBolusDescription(String bolusDescription) {
		this.bolusDescription = bolusDescription;
	}

	public String getClayContent() {
		return this.clayContent;
	}

	public void setClayContent(String clayContent) {
		this.clayContent = clayContent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}