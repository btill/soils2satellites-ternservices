package au.org.ecoinformatics.s2s.service;

import java.util.Date;
import java.util.List;

import au.org.ecoinformatics.s2s.json.DistinctSpeciesCount;
import au.org.ecoinformatics.s2s.json.HerbariumPointIntercept;
import au.org.ecoinformatics.s2s.json.HerbariumPointInterceptCount;
import au.org.ecoinformatics.s2s.json.HerbariumSpeciesCount;
import au.org.ecoinformatics.s2s.json.LandformElement;
import au.org.ecoinformatics.s2s.json.LandformElementTotal;

public interface VisualisationService {

	List<DistinctSpeciesCount> getDistinctSpeciesCountList(List<Integer> studyLocationIdList);
	
	List<HerbariumPointIntercept> getHerbariumPointIntercepts(Integer studyLocationId, Date visitStartDate);
	
	List<HerbariumPointInterceptCount> getHerbariumPointInterceptCount();
	
	List<HerbariumSpeciesCount> getHerbariumSpeciesCount(Integer studyLocationVisitId);
	
	List<LandformElement> getLandFormElements(List<Integer> studyLocationIdList);
	
	List<LandformElementTotal> getLandFormElementTotals(List<Integer> studyLocationIdList);
}
