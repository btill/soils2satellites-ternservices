package au.org.ecoinformatics.s2s.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import au.org.ecoinformatics.s2s.converter.studylocationvisit.StudyLocationVisitConverter;
import au.org.ecoinformatics.s2s.converter.studylocationvisit.StudyLocationVisitSamplingUnitConverter;
import au.org.ecoinformatics.s2s.converter.studylocationvisit.StudyLocationVisitVoucherConverter;
import au.org.ecoinformatics.s2s.domain.Observer;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.domain.VegVoucher;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnits;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSpecies;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitVoucher;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;
import au.org.ecoinformatics.s2s.repository.ObserverRepository;
import au.org.ecoinformatics.s2s.repository.SiteLocationVisitRepository;
import au.org.ecoinformatics.s2s.repository.VegVoucherRepository;
import au.org.ecoinformatics.s2s.service.StudyLocationVisitService;

@Service("studyLocationVisitService")
public class DefaultStudyLocationVisitService implements StudyLocationVisitService {

	@Autowired
	private StudyLocationVisitConverter studyLocationVisitConverter;
	@Autowired
	private StudyLocationVisitSamplingUnitConverter studyLocationVisitSamplingUnitConverter;
	@Autowired
	private StudyLocationVisitVoucherConverter studyLocationVisitVoucherConverter;
	
	@Autowired
	private SiteLocationVisitRepository siteLocationVisitRepository;
	@Autowired
	private ObserverRepository observerRepository;
	@Autowired
	private VegVoucherRepository vegVoucherRepository;
	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	
	@Override
	@Transactional
	public List<StudyLocationVisitVoucher> getStudyLocationVisitVouchers(Integer studyLocationVisitId) {
		List<VegVoucher> vegVouchers = this.vegVoucherRepository.findForSiteLocationVisit(studyLocationVisitId);
		List<StudyLocationVisitVoucher> slvVouchers = new ArrayList<StudyLocationVisitVoucher>();
		for(VegVoucher vv : vegVouchers) {
			StudyLocationVisitVoucher voucher = this.studyLocationVisitVoucherConverter.convert(vv);
			slvVouchers.add(voucher);
		}
		return slvVouchers;
	}

	@Override
	@Transactional
	public StudyLocationVisitSpecies getStudyLocationVisitSpecies(Integer studyLocationVisitId) {
		List<String> speciesList = this.herbariumDeterminationRepository.findSiteVisitDistinctSpecies(studyLocationVisitId);
		StudyLocationVisitSpecies species = new StudyLocationVisitSpecies();
		species.setStudyLocationVisitId(studyLocationVisitId);
		species.setSpeciesList(speciesList);
		return species;
	}

	@Override
	@Transactional
	public List<StudyLocationVisit> getStudyLocationVisits(Integer studyLocationId) {
		List<SiteLocationVisit> slvList = this.siteLocationVisitRepository.findSiteLocationVisits(studyLocationId);
		List<StudyLocationVisit> studyLocationVisitList = new ArrayList<StudyLocationVisit>(slvList.size());
		for(SiteLocationVisit slv : slvList) {
			List<Observer> observers = this.observerRepository.findSiteVisitDistinctObservers(slv.getSiteLocationVisitId());
			studyLocationVisitList.add(convertStudyLocationVisit(slv, observers));
		}
		return studyLocationVisitList;
		
	}
	
	@Override
	@Transactional
	public StudyLocationVisit getStudyLocationVisit(Integer studyLocationVisitId) {
		SiteLocationVisit siteLocationVisit = this.siteLocationVisitRepository.findOne(studyLocationVisitId);
		Assert.notNull(siteLocationVisit, "No matching SiteLocationVisit for siteLocationVisitId: " + studyLocationVisitId);
		List<Observer> observers = this.observerRepository.findSiteVisitDistinctObservers(studyLocationVisitId);
		return convertStudyLocationVisit(siteLocationVisit, observers);
	}

	@Transactional
	private StudyLocationVisit convertStudyLocationVisit(SiteLocationVisit slv, List<Observer> observers) {
		StudyLocationVisit studyLocationVisit = this.studyLocationVisitConverter.convert(slv);
		if(!observers.isEmpty()) {
			studyLocationVisit.setObservers(observers);
		}
		return studyLocationVisit;
	}
	
	@Override
	@Transactional
	public StudyLocationVisit getStudyLocationVisitSamplingUnitDetails(Integer studyLocationVisitId) {
		SiteLocationVisit siteLocationVisit = this.siteLocationVisitRepository.findOne(studyLocationVisitId);
		if(null == siteLocationVisit) {
			return new StudyLocationVisit();
		}
		return this.studyLocationVisitConverter.convert(siteLocationVisit);
	}

	@Override
	@Transactional
	public StudyLocationVisit getFirstStudyLocationVisit(Integer siteLocationId) {
		List<SiteLocationVisit> firstVisitList = this.siteLocationVisitRepository.findFirstSiteLocationVisit(siteLocationId);
		if(firstVisitList.isEmpty()) {
			return new StudyLocationVisit();
		}
		return this.studyLocationVisitConverter.convert(firstVisitList.get(0));
	}

	@Override
	@Transactional
	public StudyLocationVisit getLastStudyLocationVisit(Integer siteLocationId) {
		List<SiteLocationVisit> lastVisitList = this.siteLocationVisitRepository.findLastSiteLocationVisit(siteLocationId);
		if(lastVisitList.isEmpty()) {
			return new StudyLocationVisit();
		}
		return this.studyLocationVisitConverter.convert(lastVisitList.get(0));
	}

	@Override
	@Transactional
	public StudyLocationVisitSamplingUnits getStudyLocationVisitSamplingUnits(Integer studyLocationVisitId) {
		SiteLocationVisit siteLocationVisit = this.siteLocationVisitRepository.findOne(studyLocationVisitId);
		if(null == siteLocationVisit) {
			return new StudyLocationVisitSamplingUnits();
		}
		return this.studyLocationVisitSamplingUnitConverter.convert(siteLocationVisit);
	}
	
	
	
}
