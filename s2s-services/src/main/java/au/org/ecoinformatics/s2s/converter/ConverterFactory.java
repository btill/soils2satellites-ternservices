package au.org.ecoinformatics.s2s.converter;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.samplingunit.BasalAreaConverter;
import au.org.ecoinformatics.s2s.converter.samplingunit.LaiConverter;
import au.org.ecoinformatics.s2s.converter.samplingunit.PointInterceptConverter;
import au.org.ecoinformatics.s2s.converter.samplingunit.SoilBulkDensityConverter;
import au.org.ecoinformatics.s2s.converter.samplingunit.SoilCharacterisationConverter;
import au.org.ecoinformatics.s2s.converter.samplingunit.SoilSamplesSoilSubiteObservationConverter;
import au.org.ecoinformatics.s2s.converter.samplingunit.StructuralSummaryConverter;
import au.org.ecoinformatics.s2s.converter.samplingunit.VegVouchersConverter;

@Component
@SuppressWarnings("rawtypes")
public class ConverterFactory implements Converter<Object, Object> {

	@Autowired
	private SoilSamplesSoilSubiteObservationConverter soilSamplesSoilSubiteObservationConverter;
	@Autowired
	private PointInterceptConverter pointInterceptConverter;
	@Autowired
	private VegVouchersConverter vegVouchersConverter;
	@Autowired
	private StructuralSummaryConverter structuralSummaryConverter;
	@Autowired
	private BasalAreaConverter basalAreaConverter;
	@Autowired
	private LaiConverter laiConverter;
	@Autowired
	private SoilCharacterisationConverter soilCharacterisationConverter;
	@Autowired
	private SoilBulkDensityConverter soilBulkDensityConverter;
	
	private Map<Class, Map<Class, Converter<Object, Object>>> keyMap = new HashMap<Class, Map<Class, Converter<Object, Object>>>();
	
	@PostConstruct
	public void init() throws Exception {
		addKeyedConverter(this.soilSamplesSoilSubiteObservationConverter);
		addKeyedConverter(this.pointInterceptConverter);
		addKeyedConverter(this.vegVouchersConverter);
		addKeyedConverter(this.structuralSummaryConverter);
		addKeyedConverter(this.basalAreaConverter);
		addKeyedConverter(this.laiConverter);
		addKeyedConverter(this.soilCharacterisationConverter);
		addKeyedConverter(this.soilBulkDensityConverter);
	}
	
	@SuppressWarnings("unchecked")
	private void addKeyedConverter(KeyedConverter converter) {
		Map<Class, Converter<Object, Object>> innerMap = this.keyMap.get(converter.getInputClass());
		if(null == innerMap) {
			innerMap = new HashMap<Class, Converter<Object, Object>>();
		}
		innerMap.put(converter.getOutputClass(), converter);
		this.keyMap.put(converter.getInputClass(), innerMap);
	}
	
	@Override
	public Object convert(Object value) {
		Converter<Object, Object> converter = getSingleConverter(value.getClass());
		return converter.convert(value);
	}

	@Override
	public Object convert(Object outputValue, Object value) {
		Converter<Object, Object> converter = getConverter(value.getClass(), outputValue.getClass());
		return converter.convert(outputValue, value);
	}
	
	private Converter<Object, Object> getSingleConverter(Class inputKey) {
		Map<Class, Converter<Object, Object>> outputMap = getConvertersMap(inputKey);
		if(outputMap.size() != 1) {
			throw new IllegalArgumentException("No single converter for input type: " + inputKey.getName() +" Found: " + outputMap.size());
		}
		return outputMap.values().iterator().next();
	}
	
	private Converter<Object, Object> getConverter(Class inputKey, Class outputKey) {
		Map<Class, Converter<Object, Object>> outputMap = getConvertersMap(inputKey);
		if(!outputMap.containsKey(outputKey)) {
			throw new IllegalArgumentException("No Converter for input type: " + inputKey.getName() + " and output type: " + outputKey.getName());
		}
		return outputMap.get(outputKey);
	}
	
	private Map<Class, Converter<Object, Object>> getConvertersMap(Class inputKey) {
		Map<Class, Converter<Object, Object>> outputMap = this.keyMap.get(inputKey);
		if(outputMap == null || outputMap.isEmpty()) {
			throw new IllegalArgumentException("No converter for input type: " + inputKey.getName());
		}
		return outputMap;
	}

}
