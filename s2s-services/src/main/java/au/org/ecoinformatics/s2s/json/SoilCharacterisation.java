package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder(alphabetic=true)
public class SoilCharacterisation implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String coarseFragsLithology;
	private String collectedBy;
	private String colourWhenDry;
	private String colourWhenMoist;
	private String comments;
	private Double ec;
	private String horizon;
	private String layerNumber;
	private Double lowerDepth;
	private Integer nextSize1;
	private Integer nextSize2;
	private String nextSizeType1;
	private String nextSizeType2;
	private Double ph;
	private Integer smallestSize1;
	private Integer smallestSize2;
	private String smallestSizeType1;
	private String smallestSizeType2;
	private Double upperDepth;
	private String textureGrade;
	private String textureQualifier;
	private String textureModifier;
	private String mottlesColour;
	private String mottlesAbundance;
	private String mottleSize;
	
	private String coarseFragsAbundance;
	private String coarseFragsShape;
	private String coarseFragsSize;
	private String segregationsAbundance;
	private String segregationSize;
	private String segregationNature;
	private String segregationForm;
	private String pedalityFabric;
	private String pedalityType;
	private String pedalityGrade;
	private String effervescence;

	public SoilCharacterisation() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCoarseFragsLithology() {
		return this.coarseFragsLithology;
	}

	public void setCoarseFragsLithology(String coarseFragsLithology) {
		this.coarseFragsLithology = coarseFragsLithology;
	}

	public String getCollectedBy() {
		return this.collectedBy;
	}

	public void setCollectedBy(String collectedBy) {
		this.collectedBy = collectedBy;
	}

	public String getColourWhenDry() {
		return this.colourWhenDry;
	}

	public void setColourWhenDry(String colourWhenDry) {
		this.colourWhenDry = colourWhenDry;
	}

	public String getColourWhenMoist() {
		return this.colourWhenMoist;
	}

	public void setColourWhenMoist(String colourWhenMoist) {
		this.colourWhenMoist = colourWhenMoist;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Double getEc() {
		return this.ec;
	}

	public void setEc(Double ec) {
		this.ec = ec;
	}

	public String getHorizon() {
		return this.horizon;
	}

	public void setHorizon(String horizon) {
		this.horizon = horizon;
	}

	public String getLayerNumber() {
		return this.layerNumber;
	}

	public void setLayerNumber(String layerNumber) {
		this.layerNumber = layerNumber;
	}

	public Double getLowerDepth() {
		return this.lowerDepth;
	}

	public void setLowerDepth(Double lowerDepth) {
		this.lowerDepth = lowerDepth;
	}

	public Integer getNextSize1() {
		return this.nextSize1;
	}

	public void setNextSize1(Integer nextSize1) {
		this.nextSize1 = nextSize1;
	}

	public Integer getNextSize2() {
		return this.nextSize2;
	}

	public void setNextSize2(Integer nextSize2) {
		this.nextSize2 = nextSize2;
	}

	public String getNextSizeType1() {
		return this.nextSizeType1;
	}

	public void setNextSizeType1(String nextSizeType1) {
		this.nextSizeType1 = nextSizeType1;
	}

	public String getNextSizeType2() {
		return this.nextSizeType2;
	}

	public void setNextSizeType2(String nextSizeType2) {
		this.nextSizeType2 = nextSizeType2;
	}

	public Integer getSmallestSize1() {
		return this.smallestSize1;
	}

	public void setSmallestSize1(Integer smallestSize1) {
		this.smallestSize1 = smallestSize1;
	}

	public Integer getSmallestSize2() {
		return this.smallestSize2;
	}

	public void setSmallestSize2(Integer smallestSize2) {
		this.smallestSize2 = smallestSize2;
	}

	public String getSmallestSizeType1() {
		return this.smallestSizeType1;
	}

	public void setSmallestSizeType1(String smallestSizeType1) {
		this.smallestSizeType1 = smallestSizeType1;
	}

	public String getSmallestSizeType2() {
		return this.smallestSizeType2;
	}

	public void setSmallestSizeType2(String smallestSizeType2) {
		this.smallestSizeType2 = smallestSizeType2;
	}

	public Double getUpperDepth() {
		return this.upperDepth;
	}

	public void setUpperDepth(Double upperDepth) {
		this.upperDepth = upperDepth;
	}

	public Double getPh() {
		return ph;
	}

	public void setPh(Double ph) {
		this.ph = ph;
	}

	public String getTextureGrade() {
		return textureGrade;
	}

	public void setTextureGrade(String textureGrade) {
		this.textureGrade = textureGrade;
	}

	public String getTextureQualifier() {
		return textureQualifier;
	}

	public void setTextureQualifier(String textureQualifier) {
		this.textureQualifier = textureQualifier;
	}

	public String getTextureModifier() {
		return textureModifier;
	}

	public void setTextureModifier(String textureModifier) {
		this.textureModifier = textureModifier;
	}

	public String getMottlesColour() {
		return mottlesColour;
	}

	public void setMottlesColour(String mottlesColour) {
		this.mottlesColour = mottlesColour;
	}

	public String getMottlesAbundance() {
		return mottlesAbundance;
	}

	public void setMottlesAbundance(String mottlesAbundance) {
		this.mottlesAbundance = mottlesAbundance;
	}

	public String getMottleSize() {
		return mottleSize;
	}

	public void setMottleSize(String mottleSize) {
		this.mottleSize = mottleSize;
	}

	public String getCoarseFragsAbundance() {
		return coarseFragsAbundance;
	}

	public void setCoarseFragsAbundance(String coarseFragsAbundance) {
		this.coarseFragsAbundance = coarseFragsAbundance;
	}

	public String getCoarseFragsShape() {
		return coarseFragsShape;
	}

	public void setCoarseFragsShape(String coarseFragsShape) {
		this.coarseFragsShape = coarseFragsShape;
	}

	public String getCoarseFragsSize() {
		return coarseFragsSize;
	}

	public void setCoarseFragsSize(String coarseFragsSize) {
		this.coarseFragsSize = coarseFragsSize;
	}

	public String getSegregationsAbundance() {
		return segregationsAbundance;
	}

	public void setSegregationsAbundance(String segregationsAbundance) {
		this.segregationsAbundance = segregationsAbundance;
	}

	public String getSegregationSize() {
		return segregationSize;
	}

	public void setSegregationSize(String segregationSize) {
		this.segregationSize = segregationSize;
	}

	public String getSegregationNature() {
		return segregationNature;
	}

	public void setSegregationNature(String segregationNature) {
		this.segregationNature = segregationNature;
	}

	public String getSegregationForm() {
		return segregationForm;
	}

	public void setSegregationForm(String segregationForm) {
		this.segregationForm = segregationForm;
	}

	public String getPedalityFabric() {
		return pedalityFabric;
	}

	public void setPedalityFabric(String pedalityFabric) {
		this.pedalityFabric = pedalityFabric;
	}

	public String getPedalityType() {
		return pedalityType;
	}

	public void setPedalityType(String pedalityType) {
		this.pedalityType = pedalityType;
	}

	public String getPedalityGrade() {
		return pedalityGrade;
	}

	public void setPedalityGrade(String pedalityGrade) {
		this.pedalityGrade = pedalityGrade;
	}

	public String getEffervescence() {
		return effervescence;
	}

	public void setEffervescence(String effervescence) {
		this.effervescence = effervescence;
	}

}