package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the soil_bulk_density database table.
 * 
 */
@Entity
@Table(name="soil_bulk_density")
public class SoilBulkDensity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(name="fine_earth_bulk_density")
	private Double fineEarthBulkDensity;

	@Column(name="fine_earth_volume")
	private Double fineEarthVolume;

	@Column(name="fine_earth_weight")
	private Double fineEarthWeight;

	@Column(name="fine_earth_weight_in_bag")
	private Double fineEarthWeightInBag;

	@Column(name="gravel_bulk_density")
	private Double gravelBulkDensity;

	@Column(name="gravel_volume")
	private Double gravelVolume;

	@Column(name="gravel_weight")
	private Double gravelWeight;

	@Column(name="oven_dried_weight_in_bag")
	private Double ovenDriedWeightInBag;

	@Column(name="paper_bag_weight")
	private Double paperBagWeight;

	@Column(name="ring_volume")
	private Double ringVolume;

	@Column(name="ring_weight")
	private Double ringWeight;

	@Column(name="sample_id", length=2147483647)
	private String sampleId;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public SoilBulkDensity() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getFineEarthBulkDensity() {
		return this.fineEarthBulkDensity;
	}

	public void setFineEarthBulkDensity(Double fineEarthBulkDensity) {
		this.fineEarthBulkDensity = fineEarthBulkDensity;
	}

	public Double getFineEarthVolume() {
		return this.fineEarthVolume;
	}

	public void setFineEarthVolume(Double fineEarthVolume) {
		this.fineEarthVolume = fineEarthVolume;
	}

	public Double getFineEarthWeight() {
		return this.fineEarthWeight;
	}

	public void setFineEarthWeight(Double fineEarthWeight) {
		this.fineEarthWeight = fineEarthWeight;
	}

	public Double getFineEarthWeightInBag() {
		return this.fineEarthWeightInBag;
	}

	public void setFineEarthWeightInBag(Double fineEarthWeightInBag) {
		this.fineEarthWeightInBag = fineEarthWeightInBag;
	}

	public Double getGravelBulkDensity() {
		return this.gravelBulkDensity;
	}

	public void setGravelBulkDensity(Double gravelBulkDensity) {
		this.gravelBulkDensity = gravelBulkDensity;
	}

	public Double getGravelVolume() {
		return this.gravelVolume;
	}

	public void setGravelVolume(Double gravelVolume) {
		this.gravelVolume = gravelVolume;
	}

	public Double getGravelWeight() {
		return this.gravelWeight;
	}

	public void setGravelWeight(Double gravelWeight) {
		this.gravelWeight = gravelWeight;
	}

	public Double getOvenDriedWeightInBag() {
		return this.ovenDriedWeightInBag;
	}

	public void setOvenDriedWeightInBag(Double ovenDriedWeightInBag) {
		this.ovenDriedWeightInBag = ovenDriedWeightInBag;
	}

	public Double getPaperBagWeight() {
		return this.paperBagWeight;
	}

	public void setPaperBagWeight(Double paperBagWeight) {
		this.paperBagWeight = paperBagWeight;
	}

	public Double getRingVolume() {
		return this.ringVolume;
	}

	public void setRingVolume(Double ringVolume) {
		this.ringVolume = ringVolume;
	}

	public Double getRingWeight() {
		return this.ringWeight;
	}

	public void setRingWeight(Double ringWeight) {
		this.ringWeight = ringWeight;
	}

	public String getSampleId() {
		return this.sampleId;
	}

	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}