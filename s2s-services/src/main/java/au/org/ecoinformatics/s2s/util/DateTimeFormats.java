package au.org.ecoinformatics.s2s.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class DateTimeFormats {
	
	public static DateFormat getDateFormat() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat;
	}

	public static DateFormat getTimestampFormat() {
		DateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		return timestampFormat;
	}
	
	public static DateFormat getFilenameDateFormat() {
		DateFormat filenameDateFormat = new SimpleDateFormat("yyyy-MM-dd'_'HH-mm-ss-SSS");
		return filenameDateFormat;
	}
	
	public static TimeZone getTimeZone() {
		return TimeZone.getTimeZone("Australia/Adelaide");
	}
	
}
