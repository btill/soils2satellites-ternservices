package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_datum database table.
 * 
 */
@Entity
@Table(name="lut_datum")
public class LutDatum implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String datum;

	@Column(length=2147483647)
	private String description;

	@Column(name="description_geoscience_australia", length=2147483647)
	private String descriptionGeoscienceAustralia;

	@Column(name="equatorial_radius_in_m", length=2147483647)
	private String equatorialRadiusInM;

	@Column(name="inverse_flattening", length=2147483647)
	private String inverseFlattening;

	@Column(name="reference_spheroid", length=2147483647)
	private String referenceSpheroid;

	@Column(name="year_of_acceptance", length=2147483647)
	private String yearOfAcceptance;

	//bi-directional many-to-one association to LutMgaZone
	@OneToMany(mappedBy="lutDatum")
	private Set<LutMgaZone> lutMgaZones;

	public LutDatum() {
	}

	public String getDatum() {
		return this.datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionGeoscienceAustralia() {
		return this.descriptionGeoscienceAustralia;
	}

	public void setDescriptionGeoscienceAustralia(String descriptionGeoscienceAustralia) {
		this.descriptionGeoscienceAustralia = descriptionGeoscienceAustralia;
	}

	public String getEquatorialRadiusInM() {
		return this.equatorialRadiusInM;
	}

	public void setEquatorialRadiusInM(String equatorialRadiusInM) {
		this.equatorialRadiusInM = equatorialRadiusInM;
	}

	public String getInverseFlattening() {
		return this.inverseFlattening;
	}

	public void setInverseFlattening(String inverseFlattening) {
		this.inverseFlattening = inverseFlattening;
	}

	public String getReferenceSpheroid() {
		return this.referenceSpheroid;
	}

	public void setReferenceSpheroid(String referenceSpheroid) {
		this.referenceSpheroid = referenceSpheroid;
	}

	public String getYearOfAcceptance() {
		return this.yearOfAcceptance;
	}

	public void setYearOfAcceptance(String yearOfAcceptance) {
		this.yearOfAcceptance = yearOfAcceptance;
	}

	public Set<LutMgaZone> getLutMgaZones() {
		return this.lutMgaZones;
	}

	public void setLutMgaZones(Set<LutMgaZone> lutMgaZones) {
		this.lutMgaZones = lutMgaZones;
	}

}