package au.org.ecoinformatics.s2s.service;

import java.util.List;

import au.org.ecoinformatics.s2s.json.StudyLocation;
import au.org.ecoinformatics.s2s.json.StudyLocationName;
import au.org.ecoinformatics.s2s.json.StudyLocationSpecies;
import au.org.ecoinformatics.s2s.json.StudyLocationVoucher;

public interface StudyLocationService {

	Integer getStudyLocationId(String identifier);
	
	List<Integer> getStudyLocationIdList(String[] identifiers);
	
	List<StudyLocationName> getStudyLocationNameList();

	StudyLocation getStudyLocation(Integer studyLocationId);
	
	List<StudyLocation> getStudyLocations();

	List<StudyLocationVoucher> getStudyLocationVouchers(Integer studyLocationId);
	
	StudyLocationSpecies getStudyLocationSpecies(Integer studyLocationId);
}