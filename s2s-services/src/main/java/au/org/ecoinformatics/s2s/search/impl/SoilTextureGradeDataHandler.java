package au.org.ecoinformatics.s2s.search.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.json.NameValuePair;
import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.repository.SoilCharacterisationRepository;
import au.org.ecoinformatics.s2s.search.SearchTraitDataHandler;

@Component
public class SoilTextureGradeDataHandler implements SearchTraitDataHandler<NameValuePair> {

	@Autowired
	private SoilCharacterisationRepository soilCharacterisationRepository;
	
	@Override
	public Set<NameValuePair> getSearchTraitData() {
		Set<NameValuePair> dataSet = new HashSet<NameValuePair>();
		for(Object[] data : this.soilCharacterisationRepository.findSoilTextureGrades()) {
			NameValuePair value = new NameValuePair(data[0], data[1]);
			dataSet.add(value);
		}
		return dataSet;
	}

	@Override
	public SearchTrait getSearchTrait() {
		return SearchTrait.SOIL_TEXTURE;
	}

}
