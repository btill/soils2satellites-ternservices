package au.org.ecoinformatics.s2s.web;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.service.SearchService;
import au.org.ecoinformatics.s2s.service.SearchTraitsService;
import au.org.ecoinformatics.s2s.util.JsonUtilsBean;

@Controller(value="searchController")
public class SearchController {

	@Autowired
	private JsonUtilsBean jsonUtilsBean;
	@Autowired
	private SearchService searchService;
	@Autowired
	private SearchTraitsService searchTraitsService;
	
	@RequestMapping(value="/search", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String searchStudyLocationVisits(@RequestParam(value="_s", required=true) final String searchExpression, HttpServletResponse response) throws IOException {
		String decodedSearchExpression = URLDecoder.decode(searchExpression, "ISO-8859-1");
		List<StudyLocationVisit> slvList = this.searchService.searchSiteVisits(decodedSearchExpression);
		return this.jsonUtilsBean.writeValueAsString(slvList);
	}
	
	@RequestMapping(value= "/getSearchTraits", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getSearchTraits(HttpServletResponse response) throws IOException {
		return this.jsonUtilsBean.writeValueAsString(this.searchTraitsService.getSearchTraits());
	}
	
	@RequestMapping(value = "/getSearchTraitData/{identifier}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String getSearchTraits(@PathVariable Integer identifier, HttpServletResponse response) throws IOException {
		SearchTrait searchTrait = SearchTrait.valueOf(identifier);
		return this.jsonUtilsBean.writeValueAsString(this.searchTraitsService.getSearchTraitData(searchTrait));
	}
	
	
}
