package au.org.ecoinformatics.s2s.converter.studylocation;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.VegVoucher;
import au.org.ecoinformatics.s2s.json.StudyLocationVoucher;
import au.org.ecoinformatics.s2s.util.ConvertUtils;

@Component
public class StudyLocationVoucherConverter implements Converter<StudyLocationVoucher, VegVoucher> {

	@Override
	public StudyLocationVoucher convert(VegVoucher value) {
		return convert(new StudyLocationVoucher(), value);
	}

	@Override
	public StudyLocationVoucher convert(StudyLocationVoucher initialValue, VegVoucher value) {
		initialValue.setFieldName(value.getFieldName());
		initialValue.setHerbariumDetermination((String) ConvertUtils.getProperty(value, "herbariumDetermination.herbariumDetermination"));
		initialValue.setStudyLocationId(Integer.parseInt((String) ConvertUtils.getProperty(value, "siteLocationVisit.siteLocation.siteLocationId")));
		initialValue.setStudyLocationName((String) ConvertUtils.getProperty(value, "siteLocationVisit.siteLocation.siteLocationName"));
		initialValue.setVegBarcode(value.getVegBarcode());
		return initialValue;
	}

}
