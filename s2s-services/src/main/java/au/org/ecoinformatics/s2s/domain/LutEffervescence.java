package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_effervescence database table.
 * 
 */
@Entity
@Table(name="lut_effervescence")
public class LutEffervescence implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String effervescence;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutEffervescence")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutEffervescence() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEffervescence() {
		return this.effervescence;
	}

	public void setEffervescence(String effervescence) {
		this.effervescence = effervescence;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}