package au.org.ecoinformatics.s2s.converter.studylocation;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.SiteLocation;
import au.org.ecoinformatics.s2s.json.StudyLocationDetails;

@Component
public class StudyLocationDetailsConverter implements Converter<StudyLocationDetails, SiteLocation> {

	@Override
	public StudyLocationDetails convert(SiteLocation value) {
		return convert(new StudyLocationDetails(), value);
	}

	@Override
	public StudyLocationDetails convert(StudyLocationDetails details, SiteLocation site) {
		details.setStudyLocationId(site.getSiteLocationId());
		details.setStudyLocationName(site.getSiteLocationName());
		details.setBioregionName(site.getLutIbra().getBioregionDescription());
		details.setLandformElement(site.getLutLandformElement().getLandformElement());
		details.setLandformPattern(site.getLutLandformPattern().getLandformPattern());
		return details;
	}

}
