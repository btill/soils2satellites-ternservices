package au.org.ecoinformatics.s2s.converter.samplingunit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import au.org.ecoinformatics.s2s.converter.KeyedConverter;
import au.org.ecoinformatics.s2s.domain.HerbariumDetermination;
import au.org.ecoinformatics.s2s.json.BasalArea;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;
import au.org.ecoinformatics.s2s.util.ConvertUtils;

@Component
public class BasalAreaConverter implements KeyedConverter<BasalArea, au.org.ecoinformatics.s2s.domain.BasalArea> {

	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	
	@Override
	public BasalArea convert(au.org.ecoinformatics.s2s.domain.BasalArea value) {
		return convert(new BasalArea(), value);
	}

	@Override
	public BasalArea convert(BasalArea initialValue, au.org.ecoinformatics.s2s.domain.BasalArea value) {
		String vegBarcode = (String) ConvertUtils.getProperty(value, "vegVoucher.vegBarcode");
		HerbariumDetermination hb = getHerbariumDetermination(vegBarcode);
		initialValue.setBasalArea(value.getBasalArea());
		initialValue.setBasalAreaFactor(value.getBasalAreaFactor());
		initialValue.setHerbariumDeterrmination(hb.getHerbariumDetermination());
		initialValue.setHits(value.getHits());
		initialValue.setId(value.getId());
		initialValue.setPointId((String) ConvertUtils.getProperty(value, "lutBasalPoint.pointId"));
		initialValue.setVegBarcode(vegBarcode);
		return initialValue;
	}
	
	private HerbariumDetermination getHerbariumDetermination(String vegBarcode) {
		if(!StringUtils.hasText(vegBarcode)) {
			return new HerbariumDetermination();
		}
		HerbariumDetermination hb = this.herbariumDeterminationRepository.findOne(vegBarcode);
		if(hb == null) {
			return new HerbariumDetermination();
		}
		return hb;
	}

	@Override
	public Class<BasalArea> getOutputClass() {
		return BasalArea.class;
	}

	@Override
	public Class<au.org.ecoinformatics.s2s.domain.BasalArea> getInputClass() {
		return au.org.ecoinformatics.s2s.domain.BasalArea.class;
	}

}
