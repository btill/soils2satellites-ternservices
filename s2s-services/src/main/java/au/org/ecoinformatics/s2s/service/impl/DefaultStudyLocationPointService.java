package au.org.ecoinformatics.s2s.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationPointConverter;
import au.org.ecoinformatics.s2s.domain.SiteLocationPoint;
import au.org.ecoinformatics.s2s.json.StudyLocationPoint;
import au.org.ecoinformatics.s2s.repository.SiteLocationPointRepository;
import au.org.ecoinformatics.s2s.service.StudyLocationPointService;
import au.org.ecoinformatics.s2s.util.BoundingBox;
import au.org.ecoinformatics.s2s.util.CoordinateUtils;

@Service("studyLocationPointService")
public class DefaultStudyLocationPointService implements StudyLocationPointService {

	@Autowired
	private SiteLocationPointRepository siteLocationPointRepository;
	@Autowired
	private StudyLocationPointConverter studyLocationPointConverter;
	
	@Override
	@Transactional
	public StudyLocationPoint getStudyLocationMarkerPoint(Integer siteLocationId) {
		List<SiteLocationPoint> points = this.siteLocationPointRepository.findSiteLocationMarkerPoint(siteLocationId);
		if(points.isEmpty()) {
			return new StudyLocationPoint();
		}
		return this.studyLocationPointConverter.convert(points.get(0));
	}
	
	@Override
	@Transactional
	public List<StudyLocationPoint> getStudyLocationMarkers(String bboxString) {
		List<StudyLocationPoint> filteredPoints = new ArrayList<StudyLocationPoint>();
		BoundingBox boundingBox = CoordinateUtils.newBoundingBox(bboxString);
		for(StudyLocationPoint marker : getAllStudyLocationMarkers()){
			if(boundingBox.containsPoint(marker)) {
				filteredPoints.add(marker);
			}
		}
		return filteredPoints;
	}
	
	@Override
	@Transactional
	public List<StudyLocationPoint> getAllStudyLocationMarkers() {
		List<SiteLocationPoint> points = this.siteLocationPointRepository.findAll();
		List<StudyLocationPoint> slPoints = new ArrayList<StudyLocationPoint>(points.size());
		for(SiteLocationPoint point : points) {
			slPoints.add(this.studyLocationPointConverter.convert(point));
		}
		return slPoints;
	}

}
