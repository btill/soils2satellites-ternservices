package au.org.ecoinformatics.s2s.util;

import java.io.IOException;

import au.org.ecoinformatics.s2s.json.SearchTrait;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

public class SearchTraitSerializer extends com.fasterxml.jackson.databind.ser.std.StdSerializer<SearchTrait> {

	public SearchTraitSerializer() {
		super(SearchTrait.class);
	}

	@Override
	public void serialize(SearchTrait searchTrait, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeNumberField("id", searchTrait.getId());
		jgen.writeStringField("name", searchTrait.getName());
		jgen.writeStringField("criteria", searchTrait.getCriteria());
		jgen.writeStringField("searchDataType", searchTrait.getSearchDataType().name());
		jgen.writeEndObject();
	}

}
