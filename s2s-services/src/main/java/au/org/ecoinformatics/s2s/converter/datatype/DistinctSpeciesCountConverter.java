package au.org.ecoinformatics.s2s.converter.datatype;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.DistinctSpeciesCount;

@Component
public class DistinctSpeciesCountConverter implements Converter<DistinctSpeciesCount, Object[]> {

	@Override
	public DistinctSpeciesCount convert(Object[] value) {
		return convert(new DistinctSpeciesCount(), value);
	}

	@Override
	public DistinctSpeciesCount convert(DistinctSpeciesCount initialValue, Object[] value) {
		initialValue.setSiteLocationId((Integer) value[0]);
		initialValue.setSiteLocationName((String) value[1]);
		initialValue.setDistinctSpeciesCount((Long) value[2]);
		return initialValue;
	}

}
