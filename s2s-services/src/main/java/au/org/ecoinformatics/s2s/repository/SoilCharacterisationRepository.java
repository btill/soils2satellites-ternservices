package au.org.ecoinformatics.s2s.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import au.org.ecoinformatics.s2s.domain.SoilCharacterisation;

@Repository("soilCharacterisationRepository")
public interface SoilCharacterisationRepository extends JpaRepository<SoilCharacterisation, Integer> {

	@Query("SELECT sc " +
		   "FROM SiteLocation sl, SiteLocationVisit slv, SoilCharacterisation sc " +
		   "WHERE slv.siteLocation.siteLocationId = sl.siteLocationId " +
		   "AND sc.siteLocationVisit.siteLocationVisitId = slv.siteLocationVisitId " +
		   "AND sl.siteLocationId = ?1 " +
		   "AND slv.visitStartDate = " + 
           "(SELECT MAX(slv1.visitStartDate) " +
           " FROM SiteLocationVisit slv1 " +
           " WHERE slv1.siteLocation.siteLocationId = ?1) " +
           "ORDER BY sc.layerNumber")
	List<SoilCharacterisation> findForSiteLocation(Integer siteLocationId);
	
	@Query("SELECT code, name " +
		   "FROM LutSoilTexGrade stg ")
	List<Object[]> findSoilTextureGrades();
}
