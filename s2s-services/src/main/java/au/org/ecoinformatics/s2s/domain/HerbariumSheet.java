package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;


/**
 * The persistent class for the herbarium_sheet database table.
 * 
 */
@Entity
@Table(name="herbarium_sheet")
public class HerbariumSheet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="herbarium_sheet_id", unique=true, nullable=false)
	private Integer herbariumSheetId;

	@Column(length=2147483647)
	private String comments;

	@Column(name="follow_up_date")
	private Timestamp followUpDate;

	@Column(name="follow_up_text", length=2147483647)
	private String followUpText;

	@Column(name="herbarium_sheet_name", length=2147483647)
	private String herbariumSheetName;

	@Column(name="lodgement_date")
	private Timestamp lodgementDate;

	@Column(name="return_date")
	private Timestamp returnDate;

	//bi-directional many-to-one association to LutState
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="state", nullable=false)
	private LutState lutState;

	//bi-directional many-to-one association to VegVoucher
	@OneToMany(mappedBy="herbariumSheet")
	private Set<VegVoucher> vegVouchers;

	public HerbariumSheet() {
	}

	public Integer getHerbariumSheetId() {
		return this.herbariumSheetId;
	}

	public void setHerbariumSheetId(Integer herbariumSheetId) {
		this.herbariumSheetId = herbariumSheetId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Timestamp getFollowUpDate() {
		return this.followUpDate;
	}

	public void setFollowUpDate(Timestamp followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getFollowUpText() {
		return this.followUpText;
	}

	public void setFollowUpText(String followUpText) {
		this.followUpText = followUpText;
	}

	public String getHerbariumSheetName() {
		return this.herbariumSheetName;
	}

	public void setHerbariumSheetName(String herbariumSheetName) {
		this.herbariumSheetName = herbariumSheetName;
	}

	public Timestamp getLodgementDate() {
		return this.lodgementDate;
	}

	public void setLodgementDate(Timestamp lodgementDate) {
		this.lodgementDate = lodgementDate;
	}

	public Timestamp getReturnDate() {
		return this.returnDate;
	}

	public void setReturnDate(Timestamp returnDate) {
		this.returnDate = returnDate;
	}

	public LutState getLutState() {
		return this.lutState;
	}

	public void setLutState(LutState lutState) {
		this.lutState = lutState;
	}

	public Set<VegVoucher> getVegVouchers() {
		return this.vegVouchers;
	}

	public void setVegVouchers(Set<VegVoucher> vegVouchers) {
		this.vegVouchers = vegVouchers;
	}

}