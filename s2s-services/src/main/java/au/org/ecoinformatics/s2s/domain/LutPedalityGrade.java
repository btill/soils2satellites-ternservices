package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_pedality_grade database table.
 * 
 */
@Entity
@Table(name="lut_pedality_grade")
public class LutPedalityGrade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String description;

	@Column(nullable=false, length=2147483647)
	private String grade;

	@Column(nullable=false, length=2147483647)
	private String pedality;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutPedalityGrade")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutPedalityGrade() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getPedality() {
		return this.pedality;
	}

	public void setPedality(String pedality) {
		this.pedality = pedality;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}