package au.org.ecoinformatics.s2s.converter.samplingunitdetails;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.converter.ConverterFactory;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnitDetails;

@Component
@SuppressWarnings({"rawtypes","unchecked"})
public class StudyLocationVisitSamplingUnitDetailsConverter implements Converter<StudyLocationVisitSamplingUnitDetails, SiteLocationVisit> {
	
	@Autowired
	private ConverterFactory converterFactory;
	
	@Override
	public StudyLocationVisitSamplingUnitDetails convert(SiteLocationVisit value) {
		return convert(new StudyLocationVisitSamplingUnitDetails(), value);
	}

	@Override
	public StudyLocationVisitSamplingUnitDetails convert(StudyLocationVisitSamplingUnitDetails slvsud, SiteLocationVisit value) {
		slvsud.setStudyLocationName(value.getSiteLocation().getSiteLocationName());
		slvsud.setStudyLocationVisitId(value.getSiteLocationVisitId());
		slvsud.setSamplingUnitData(convertSamplingUnitData(slvsud, value));
		return slvsud;
	}
	
	private Set convertSamplingUnitData(StudyLocationVisitSamplingUnitDetails slvsud, SiteLocationVisit value) {
		Set output = new HashSet();
		for(Object samplingUnitData : slvsud.getSamplingUnit().getSamplingUnitData(value)) {
			Object convertedValue = this.converterFactory.convert(samplingUnitData);
			output.add(convertedValue);
		}
		return output;
	}

}
