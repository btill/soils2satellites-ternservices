package au.org.ecoinformatics.s2s.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import au.org.ecoinformatics.s2s.converter.samplingunit.SamplingUnitConverter;
import au.org.ecoinformatics.s2s.converter.samplingunitdetails.StudyLocationVisitSamplingUnitDetailsConverter;
import au.org.ecoinformatics.s2s.converter.studylocationvisit.StudyLocationVisitDetailsConverter;
import au.org.ecoinformatics.s2s.domain.Observer;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.SamplingUnit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitDetails;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnitDetails;
import au.org.ecoinformatics.s2s.repository.ObserverRepository;
import au.org.ecoinformatics.s2s.repository.SiteLocationVisitRepository;
import au.org.ecoinformatics.s2s.service.StudyLocationVisitDetailsService;

@Service("studyLocationVisitDetailsService")
public class DefaultStudyLocationVisitDetailsService implements StudyLocationVisitDetailsService {
	
	@Autowired
	private SamplingUnitConverter samplingUnitConverter;
	@Autowired
	private StudyLocationVisitDetailsConverter studyLocationVisitDetailsConverter;
	@Autowired
	private StudyLocationVisitSamplingUnitDetailsConverter studyLocationVisitSamplingUnitDetailsConverter;

	@Autowired
	private SiteLocationVisitRepository siteLocationVisitRepository;
	@Autowired
	private ObserverRepository observerRepository;
	
	@Override
	@Transactional
	public StudyLocationVisitDetails getStudyLocationVisitDetails(Integer studyLocationVisitId) {
		SiteLocationVisit siteLocationVisit = this.siteLocationVisitRepository.findOne(studyLocationVisitId);
		Assert.notNull(siteLocationVisit, "No matching SiteLocationVisit for siteLocationVisitId: " + studyLocationVisitId);
		List<Observer> observers = this.observerRepository.findSiteVisitDistinctObservers(studyLocationVisitId);
		List<SamplingUnit> samplingUnits = this.samplingUnitConverter.convert(siteLocationVisit);
		
		StudyLocationVisitDetails slvd = this.studyLocationVisitDetailsConverter.convert(siteLocationVisit);
		slvd.setSamplingUnits(samplingUnits);
		if(!observers.isEmpty()) {
			slvd.setObservers(observers);
		}
		return slvd;
	}

	@Override
	@Transactional
	public StudyLocationVisitSamplingUnitDetails getStudyLocationSamplingUnitDetails(Integer studyLocationVisitId, SamplingUnit samplingUnit) {
		SiteLocationVisit siteLocationVisit = this.siteLocationVisitRepository.findOne(studyLocationVisitId);
		Assert.notNull(siteLocationVisit, "No matching SiteLocationVisit for siteLocationVisitId: " + studyLocationVisitId);
		StudyLocationVisitSamplingUnitDetails studyLocationVisitSamplingUnitDetails = new StudyLocationVisitSamplingUnitDetails();
		studyLocationVisitSamplingUnitDetails.setSamplingUnit(samplingUnit);
		return this.studyLocationVisitSamplingUnitDetailsConverter.convert(studyLocationVisitSamplingUnitDetails, siteLocationVisit);
	}
	
}
