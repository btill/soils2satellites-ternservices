package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_substrate database table.
 * 
 */
@Entity
@Table(name="lut_substrate")
public class LutSubstrate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String substrate;

	@Column(length=2147483647)
	private String description;

	//bi-directional many-to-one association to PointIntercept
	@OneToMany(mappedBy="lutSubstrate")
	private Set<PointIntercept> pointIntercepts;

	public LutSubstrate() {
	}

	public String getSubstrate() {
		return this.substrate;
	}

	public void setSubstrate(String substrate) {
		this.substrate = substrate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<PointIntercept> getPointIntercepts() {
		return this.pointIntercepts;
	}

	public void setPointIntercepts(Set<PointIntercept> pointIntercepts) {
		this.pointIntercepts = pointIntercepts;
	}

}