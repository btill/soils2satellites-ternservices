package au.org.ecoinformatics.s2s.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class LandformElement implements Serializable {
	
	private static final long serialVersionUID = -1067867203399635704L;
	
	private Integer studyLocationId;
	private String studyLocationName;
	private String landFormElement;
	
	public Integer getStudyLocationId() {
		return studyLocationId;
	}
	public void setStudyLocationId(Integer studyLocationId) {
		this.studyLocationId = studyLocationId;
	}
	public String getStudyLocationName() {
		return studyLocationName;
	}
	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}
	public String getLandFormElement() {
		return landFormElement;
	}
	public void setLandFormElement(String landFormElement) {
		this.landFormElement = landFormElement;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((landFormElement == null) ? 0 : landFormElement.hashCode());
		result = prime * result
				+ ((studyLocationId == null) ? 0 : studyLocationId.hashCode());
		result = prime
				* result
				+ ((studyLocationName == null) ? 0 : studyLocationName
						.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LandformElement other = (LandformElement) obj;
		if (landFormElement == null) {
			if (other.landFormElement != null)
				return false;
		} else if (!landFormElement.equals(other.landFormElement))
			return false;
		if (studyLocationId == null) {
			if (other.studyLocationId != null)
				return false;
		} else if (!studyLocationId.equals(other.studyLocationId))
			return false;
		if (studyLocationName == null) {
			if (other.studyLocationName != null)
				return false;
		} else if (!studyLocationName.equals(other.studyLocationName))
			return false;
		return true;
	}
	
}
