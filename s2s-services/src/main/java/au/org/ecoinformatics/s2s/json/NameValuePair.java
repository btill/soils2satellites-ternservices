package au.org.ecoinformatics.s2s.json;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class NameValuePair {

	private Object name;
	private Object value;
	
	public NameValuePair() { }
	
	public NameValuePair(Object name, Object value) {
		this.name = name;
		this.value = value;
	}
	
	public Object getName() {
		return name;
	}
	public void setName(Object name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	
}
