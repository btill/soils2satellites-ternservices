package au.org.ecoinformatics.s2s.json;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic=true)
public class StudyLocationPoint {

	private String studyLocationName;
	private Double longitude;  //Not sure of floating point precision here, nor if its an issue.
	private Double latitude;
	private Double easting;
	private Double northing;
	private Integer zone;
	private String wkt;
	
	public StudyLocationPoint() {}
	
	public String getStudyLocationName() {
		return studyLocationName;
	}
	public void setStudyLocationName(String studyLocationName) {
		this.studyLocationName = studyLocationName;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getEasting() {
		return easting;
	}
	public void setEasting(Double easting) {
		this.easting = easting;
	}
	public Double getNorthing() {
		return northing;
	}
	public void setNorthing(Double northing) {
		this.northing = northing;
	}
	public Integer getZone() {
		return zone;
	}
	public void setZone(Integer zone) {
		this.zone = zone;
	}
	public String getWkt() {
		return wkt;
	}
	public void setWkt(String wkt) {
		this.wkt = wkt;
	}
	
}
