package au.org.ecoinformatics.s2s.converter.samplingunit;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.SamplingUnit;

@Component
public class SamplingUnitConverter implements Converter<List<SamplingUnit>, SiteLocationVisit> {

	@Override
	public List<SamplingUnit> convert(SiteLocationVisit value) {
		return convert(new ArrayList<SamplingUnit>(), value);
	}

	@Override
	public List<SamplingUnit> convert(List<SamplingUnit> initialValue, SiteLocationVisit value) {
		initialValue.clear();
		for(SamplingUnit su : SamplingUnit.values()) {
			if(su.hasSamplingUnitData(value)) {
				initialValue.add(su);
			}
		}
		return initialValue;
	}

}
