package au.org.ecoinformatics.s2s.service;

import au.org.ecoinformatics.s2s.json.StudyLocationDetails;

public interface StudyLocationDetailsService {

	StudyLocationDetails getStudyLocationDetails(Integer studyLocationId);

}