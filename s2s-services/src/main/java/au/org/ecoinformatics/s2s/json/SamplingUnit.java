package au.org.ecoinformatics.s2s.json;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;

public enum SamplingUnit {

	POINT_INTERCEPT (0, "Point Intercept") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return siteLocationVisit.getPointIntercepts();
		}
	},
	BASAL_AREA(1, "Basal Area") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return siteLocationVisit.getBasalAreas();
		}
	},
	LEAF_AREA_INDEX(2, "Leaf Area Index (LAI)") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return siteLocationVisit.getLais();
		}
	},
	PHOTOPOINT(3, "Photopoint") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return siteLocationVisit.getPhotopoints();
		}
	},
	STRUCTURAL_SUMMARY(4, "Structural Summary") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return siteLocationVisit.getStructuralSummaries();
		}
	},
	SOIL_OBSERVATION(5, "Soil Observation") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return siteLocationVisit.getSoilSubsiteObservations();
		}
	},
	SOIL_BULK_DENSITY(6, "Soil Bulk Density") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return siteLocationVisit.getSoilBulkDensities();
		}
	},
	SOIL_CHARACTER(7, "Soil Character") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return siteLocationVisit.getSoilCharacterisations();
		}
	},
	NULL(999, "NULL") {
		@Override
		public Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit) {
			return Collections.emptySet();
		}
	};
	
	private static Map<Integer, SamplingUnit> idMap = new HashMap<Integer, SamplingUnit>();
	
	private final Integer id;
	private final String description;
	
	private SamplingUnit(Integer id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public abstract Set<?> getSamplingUnitData(SiteLocationVisit siteLocationVisit);
	
	public boolean hasSamplingUnitData(SiteLocationVisit siteLocationVisit) {
		Set<?> samplingUnitData = getSamplingUnitData(siteLocationVisit);
		return (null != samplingUnitData && !samplingUnitData.isEmpty());
	}
	
	public static SamplingUnit valueOf(Integer id) {
		SamplingUnit su = idMap.get(id);
		if(su == null ) {
			throw new IllegalArgumentException("No Sampling Unit matching ID: " + id);
		}
		return su;
	}
	
	static {
		for(SamplingUnit su : SamplingUnit.values()) {
			idMap.put(su.getId(), su);
		}
	}
	
}
