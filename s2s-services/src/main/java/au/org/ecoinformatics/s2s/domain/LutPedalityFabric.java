package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_pedality_fabric database table.
 * 
 */
@Entity
@Table(name="lut_pedality_fabric")
public class LutPedalityFabric implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String description;

	@Column(name="pedality_fabric", nullable=false, length=2147483647)
	private String pedalityFabric;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutPedalityFabric")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutPedalityFabric() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPedalityFabric() {
		return this.pedalityFabric;
	}

	public void setPedalityFabric(String pedalityFabric) {
		this.pedalityFabric = pedalityFabric;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}