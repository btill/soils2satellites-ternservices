package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the herbarium_determination database table.
 * 
 */
@Entity
@Table(name="herbarium_determination")
public class HerbariumDetermination implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="veg_barcode", unique=true, nullable=false, length=2147483647)
	private String vegBarcode;

	@Column(name="determination_date")
	private Timestamp determinationDate;

	@Column(length=2147483647)
	private String determiner;

	@Column(name="herbarium_determination", length=2147483647)
	private String herbariumDetermination;

	@Column(name="state_accession_number", length=2147483647)
	private String stateAccessionNumber;

	//bi-directional one-to-one association to VegVoucher
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="veg_barcode", nullable=false, insertable=false, updatable=false)
	private VegVoucher vegVoucher;

	public HerbariumDetermination() {
	}

	public String getVegBarcode() {
		return this.vegBarcode;
	}

	public void setVegBarcode(String vegBarcode) {
		this.vegBarcode = vegBarcode;
	}

	public Timestamp getDeterminationDate() {
		return this.determinationDate;
	}

	public void setDeterminationDate(Timestamp determinationDate) {
		this.determinationDate = determinationDate;
	}

	public String getDeterminer() {
		return this.determiner;
	}

	public void setDeterminer(String determiner) {
		this.determiner = determiner;
	}

	public String getHerbariumDetermination() {
		return this.herbariumDetermination;
	}

	public void setHerbariumDetermination(String herbariumDetermination) {
		this.herbariumDetermination = herbariumDetermination;
	}

	public String getStateAccessionNumber() {
		return this.stateAccessionNumber;
	}

	public void setStateAccessionNumber(String stateAccessionNumber) {
		this.stateAccessionNumber = stateAccessionNumber;
	}

	public VegVoucher getVegVoucher() {
		return this.vegVoucher;
	}

	public void setVegVoucher(VegVoucher vegVoucher) {
		this.vegVoucher = vegVoucher;
	}

}