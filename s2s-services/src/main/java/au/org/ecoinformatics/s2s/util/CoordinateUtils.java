package au.org.ecoinformatics.s2s.util;

import java.text.DecimalFormat;

public final class CoordinateUtils {

	private static DecimalFormat coordinateFormat = new DecimalFormat("#.######");
	
	public static BoundingBox newBoundingBox(String bboxString) {
		try {
			return new BoundingBox(bboxString);
		}
		catch(NumberFormatException nfe) {
			return new NullBoundingBox();
		}
	}
	
	/**
	 * Needs to be able to handle coordinates like -22.16'59.48" and
	 * 133.14'52.00"
	 */
	public static Double convertMinuteDecimalSecondToDecimal(String initialCoordinate) {
		int startIndex = getFirstIndex(initialCoordinate);
		int endIndex = getLastIndex(initialCoordinate) + 1;
		String coord = initialCoordinate.substring(startIndex, endIndex);
		if(isDecimalFormat(coord)) {
			return formatCoordinate(coord);
		}
		
		// Need to Split up the coord into hours, minutes, and decimal seconds
		// first, find the hours decimal point.
		// Attempting to clean spaces and bad formatting
		coord = coord.replace("' ", "'").replace(" ", ".");
		
		int indexHour = coord.indexOf('.');
		int indexMinute = coord.indexOf('\'');
		String hour = coord.substring(0, indexHour);
		String minute = coord.substring(indexHour + 1, indexMinute);
		String decSecond = coord.substring(indexMinute + 1, coord.length() - 1);

		// minute * ( 1.0 / 60.0 ) + decSecond * ( 1.0 / 3600.0 )
		Double coordFraction = (Integer.parseInt(minute) * (1.0 / 60.0))
				+ (Double.parseDouble(decSecond) * (1.0 / 3600.0));
		int degree = Integer.parseInt(hour);
		if (degree < 0) {
			coordFraction = -coordFraction;
		}
		double result = degree + coordFraction;
		return formatCoordinate(result);
	}
	
	private static Double formatCoordinate(String value) {
		Double result = Double.valueOf(value);
		return formatCoordinate(result);
	}
	
	private static Double formatCoordinate(double value) {
		return Double.valueOf(coordinateFormat.format(value));
	}
	
	private static int getFirstIndex(String coordinate) {
		for(int i = 0; i < coordinate.length(); i++) {
			char c = coordinate.charAt(i);
			if(isDigit(c) || c == '-') {
				return i;
			}
		}
		throw new IllegalArgumentException("No Digits in coordinate: " + coordinate);
	}
	
	private static int getLastIndex(String coordinate) {
		int index = -1;
		for(int i = coordinate.length() - 1; i >= 0; i--) {
			char c = coordinate.charAt(i);
			if(c == '"') {
				index = i;
				continue;
			}
			if(isDigit(c)) {
				return (index != -1 ? index : i);
			}
		}
		throw new IllegalArgumentException("No Digits in coordinate: " + coordinate);
	}

	private static boolean isDigit(char c) {
		return (c >= '0' && c <= '9');
	}
	
	public static boolean isDecimalFormat(String coordinate) {
		return (coordinate.indexOf('\'') == -1 && coordinate.indexOf('"') == -1);
	}
	
}
