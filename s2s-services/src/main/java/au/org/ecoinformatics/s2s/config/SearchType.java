package au.org.ecoinformatics.s2s.config;

public enum SearchType {
    DATE_RANGE,
    CHAR,
    TEXT_VALUE_MULTI,
    NOT_TEXT_VALUE_MULTI,
    NUMERIC_RANGE_OR_VALUE,
    BOOLEAN_EXISTS,
    BOOLEAN_EXISTS_CHECK
}
