package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the structural_summary database table.
 * 
 */
@Entity
@Table(name="structural_summary")
public class StructuralSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(length=2147483647)
	private String description;

	@Column(name="ground_1_dominant", length=2147483647)
	private String ground1Dominant;

	@Column(name="ground_2_dominant", length=2147483647)
	private String ground2Dominant;

	@Column(name="ground_3_dominant", length=2147483647)
	private String ground3Dominant;

	@Column(name="mass_flowering_event")
	private Integer massFloweringEvent;

	@Column(name="mid_1_dominant", length=2147483647)
	private String mid1Dominant;

	@Column(name="mid_2_dominant", length=2147483647)
	private String mid2Dominant;

	@Column(name="mid_3_dominant", length=2147483647)
	private String mid3Dominant;

	@Column(name="phenology_comment", length=2147483647)
	private String phenologyComment;

	@Column(name="upper_1_dominant", length=2147483647)
	private String upper1Dominant;

	@Column(name="upper_2_dominant", length=2147483647)
	private String upper2Dominant;

	@Column(name="upper_3_dominant", length=2147483647)
	private String upper3Dominant;

	//bi-directional many-to-one association to SiteLocationVisit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="site_location_visit_id", nullable=false)
	private SiteLocationVisit siteLocationVisit;

	public StructuralSummary() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGround1Dominant() {
		return this.ground1Dominant;
	}

	public void setGround1Dominant(String ground1Dominant) {
		this.ground1Dominant = ground1Dominant;
	}

	public String getGround2Dominant() {
		return this.ground2Dominant;
	}

	public void setGround2Dominant(String ground2Dominant) {
		this.ground2Dominant = ground2Dominant;
	}

	public String getGround3Dominant() {
		return this.ground3Dominant;
	}

	public void setGround3Dominant(String ground3Dominant) {
		this.ground3Dominant = ground3Dominant;
	}

	public Integer getMassFloweringEvent() {
		return this.massFloweringEvent;
	}

	public void setMassFloweringEvent(Integer massFloweringEvent) {
		this.massFloweringEvent = massFloweringEvent;
	}

	public String getMid1Dominant() {
		return this.mid1Dominant;
	}

	public void setMid1Dominant(String mid1Dominant) {
		this.mid1Dominant = mid1Dominant;
	}

	public String getMid2Dominant() {
		return this.mid2Dominant;
	}

	public void setMid2Dominant(String mid2Dominant) {
		this.mid2Dominant = mid2Dominant;
	}

	public String getMid3Dominant() {
		return this.mid3Dominant;
	}

	public void setMid3Dominant(String mid3Dominant) {
		this.mid3Dominant = mid3Dominant;
	}

	public String getPhenologyComment() {
		return this.phenologyComment;
	}

	public void setPhenologyComment(String phenologyComment) {
		this.phenologyComment = phenologyComment;
	}

	public String getUpper1Dominant() {
		return this.upper1Dominant;
	}

	public void setUpper1Dominant(String upper1Dominant) {
		this.upper1Dominant = upper1Dominant;
	}

	public String getUpper2Dominant() {
		return this.upper2Dominant;
	}

	public void setUpper2Dominant(String upper2Dominant) {
		this.upper2Dominant = upper2Dominant;
	}

	public String getUpper3Dominant() {
		return this.upper3Dominant;
	}

	public void setUpper3Dominant(String upper3Dominant) {
		this.upper3Dominant = upper3Dominant;
	}

	public SiteLocationVisit getSiteLocationVisit() {
		return this.siteLocationVisit;
	}

	public void setSiteLocationVisit(SiteLocationVisit siteLocationVisit) {
		this.siteLocationVisit = siteLocationVisit;
	}

}