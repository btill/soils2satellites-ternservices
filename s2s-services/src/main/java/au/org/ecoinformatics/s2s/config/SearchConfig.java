package au.org.ecoinformatics.s2s.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class SearchConfig {
	
	private List<TraitConfig> traitConfigList = new ArrayList<TraitConfig>();

	public List<TraitConfig> getTraitConfigList() {
		return traitConfigList;
	}

	public void setTraitConfigList(List<TraitConfig> traitConfigList) {
		this.traitConfigList = traitConfigList;
	}

	
	
}
