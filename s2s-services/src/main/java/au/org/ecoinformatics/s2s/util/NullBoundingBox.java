package au.org.ecoinformatics.s2s.util;

import au.org.ecoinformatics.s2s.json.StudyLocationPoint;

public class NullBoundingBox extends BoundingBox {

	private static final long serialVersionUID = -3197398929729402632L;

	public NullBoundingBox() {
		super(0, 0, 0, 0);
	}

	@Override
	public boolean containsPoint(StudyLocationPoint point) {
		return true;
	}

	@Override
	public boolean isNull() {
		return true;
	}
	
	
}
