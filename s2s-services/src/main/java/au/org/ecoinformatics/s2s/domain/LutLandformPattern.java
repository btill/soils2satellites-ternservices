package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_landform_pattern database table.
 * 
 */
@Entity
@Table(name="lut_landform_pattern")
public class LutLandformPattern implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(name="common_elements", length=2147483647)
	private String commonElements;

	@Column(name="compare_with", length=2147483647)
	private String compareWith;

	@Column(length=2147483647)
	private String description;

	@Column(name="landform_pattern", length=2147483647)
	private String landformPattern;

	@Column(name="occasional_elements", length=2147483647)
	private String occasionalElements;

	@Column(name="typical_elements", length=2147483647)
	private String typicalElements;

	//bi-directional many-to-one association to SiteLocation
	@OneToMany(mappedBy="lutLandformPattern")
	private Set<SiteLocation> siteLocations;

	public LutLandformPattern() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCommonElements() {
		return this.commonElements;
	}

	public void setCommonElements(String commonElements) {
		this.commonElements = commonElements;
	}

	public String getCompareWith() {
		return this.compareWith;
	}

	public void setCompareWith(String compareWith) {
		this.compareWith = compareWith;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLandformPattern() {
		return this.landformPattern;
	}

	public void setLandformPattern(String landformPattern) {
		this.landformPattern = landformPattern;
	}

	public String getOccasionalElements() {
		return this.occasionalElements;
	}

	public void setOccasionalElements(String occasionalElements) {
		this.occasionalElements = occasionalElements;
	}

	public String getTypicalElements() {
		return this.typicalElements;
	}

	public void setTypicalElements(String typicalElements) {
		this.typicalElements = typicalElements;
	}

	public Set<SiteLocation> getSiteLocations() {
		return this.siteLocations;
	}

	public void setSiteLocations(Set<SiteLocation> siteLocations) {
		this.siteLocations = siteLocations;
	}

}