package au.org.ecoinformatics.s2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the lut_soil_tex_qual database table.
 * 
 */
@Entity
@Table(name="lut_soil_tex_qual")
public class LutSoilTexQual implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=2147483647)
	private String id;

	@Column(nullable=false, length=2147483647)
	private String qualification;

	//bi-directional many-to-one association to SoilCharacterisation
	@OneToMany(mappedBy="lutSoilTexQual")
	private Set<SoilCharacterisation> soilCharacterisations;

	public LutSoilTexQual() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQualification() {
		return this.qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public Set<SoilCharacterisation> getSoilCharacterisations() {
		return this.soilCharacterisations;
	}

	public void setSoilCharacterisations(Set<SoilCharacterisation> soilCharacterisations) {
		this.soilCharacterisations = soilCharacterisations;
	}

}