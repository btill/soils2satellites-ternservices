package au.org.ecoinformatics.s2s.converter.datatype;

import org.springframework.stereotype.Component;

import au.org.ecoinformatics.s2s.converter.Converter;
import au.org.ecoinformatics.s2s.json.LandformElementTotal;

@Component
public class LandformElementTotalConverter implements Converter<LandformElementTotal, Object[]> {

	@Override
	public LandformElementTotal convert(Object[] value) {
		return convert(new LandformElementTotal(), value);
	}

	@Override
	public LandformElementTotal convert(LandformElementTotal initialValue, Object[] value) {
		initialValue.setLandFormElement((String) value[0]);
		initialValue.setStudyLocationCount((Long) value[1]);
		initialValue.setTotalCount((Long) value[2]);
		return initialValue;
	}

}
