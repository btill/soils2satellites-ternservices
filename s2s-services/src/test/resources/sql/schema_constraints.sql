ALTER TABLE site_location ADD CONSTRAINT lut_landform_element_site_location_fk
FOREIGN KEY (landform_element)
REFERENCES lut_landform_element (code) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE site_location ADD CONSTRAINT lut_landform_pattern_site_location_fk
FOREIGN KEY (landform_pattern)
REFERENCES lut_landform_pattern (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE visit_surface_soil_conditions ADD CONSTRAINT lut_surface_soil_cond_visit_surface_soil_conditions_fk
FOREIGN KEY (surface_soil_condition_id)
REFERENCES lut_surface_soil_cond (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE point_intercept ADD CONSTRAINT lut_substrate_point_intercept_fk FOREIGN KEY (substrate) REFERENCES lut_substrate (substrate) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE herbarium_sheet ADD CONSTRAINT lut_state_herbarium_sheet_fk FOREIGN KEY (state) REFERENCES lut_state (state) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_soiltexqual_soil_characterisation_fk
FOREIGN KEY (texture_qualifier)
REFERENCES lut_soil_tex_qual (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_soiltexmod_soil_characterisation_fk
FOREIGN KEY (texture_modifier)
REFERENCES lut_soil_tex_mod (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_soiltexgrade_soil_characterisation_fk
FOREIGN KEY (texture_grade)
REFERENCES lut_soil_tex_grade (code) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE site_location_visit ADD CONSTRAINT lut_soilobstype_site_location_visit_fk
FOREIGN KEY (soil_observation_type)
REFERENCES lut_soil_obs_type (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_segsize_soil_characterisation_fk
FOREIGN KEY (segregations_size)
REFERENCES lut_seg_size (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_segnature_soil_characterisation_fk
FOREIGN KEY (segregations_nature)
REFERENCES lut_seg_nature (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_segform_soil_characterisation_fk
FOREIGN KEY (segregations_form)
REFERENCES lut_seg_form (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_segabundance_soil_characterisation_fk
FOREIGN KEY (segregations_abundance)
REFERENCES lut_seg_abundance (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_pedality_type_soil_characterisation_fk
FOREIGN KEY (pedality_type)
REFERENCES lut_pedality_type (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_pedalitygrade_soil_characterisation_fk
FOREIGN KEY (pedality_grade)
REFERENCES lut_pedality_grade (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_pedalityfabric_soil_characterisation_fk
FOREIGN KEY (pedality_fabric)
REFERENCES lut_pedality_fabric (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_mottlesize_soil_characterisation_fk
FOREIGN KEY (mottles_size)
REFERENCES lut_mottle_size (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_mottlecolour_soil_characterisation_fk
FOREIGN KEY (mottles_colour)
REFERENCES lut_mottle_colour (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_mottleabund_soil_characterisation_fk
FOREIGN KEY (mottles_abundance)
REFERENCES lut_mottle_abund (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE site_location_visit ADD CONSTRAINT lut_microrelief_site_location_visit_fk
FOREIGN KEY (microrelief)
REFERENCES lut_microrelief (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE site_location ADD CONSTRAINT lut_ibra_site_location_fk FOREIGN KEY (bioregion_name) REFERENCES lut_ibra (bioregion_name) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE point_intercept ADD CONSTRAINT lut_growthform_point_intercept_fk FOREIGN KEY (growth_form) REFERENCES lut_growth_form (growth_form) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE site_location_visit ADD CONSTRAINT lut_erosiontype_site_location_visit_fk
FOREIGN KEY (erosion_type)
REFERENCES lut_erosion_type (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE site_location_visit ADD CONSTRAINT lut_erosionstate_site_location_visit_fk
FOREIGN KEY (erosion_state)
REFERENCES lut_erosion_state (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE site_location_visit ADD CONSTRAINT lut_erosionabund_site_location_visit_fk
FOREIGN KEY (erosion_abundance)
REFERENCES lut_erosion_abund (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_effervscence_soil_characterisation_fk
FOREIGN KEY (effervescence)
REFERENCES lut_effervescence (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE site_location_visit ADD CONSTRAINT lut_drainage_site_location_visit_fk
FOREIGN KEY (drainage_type)
REFERENCES lut_drainage (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE site_location_visit ADD CONSTRAINT lut_disturbance_site_location_visit_fk
FOREIGN KEY (disturbance)
REFERENCES lut_disturbance (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE lut_mga_zone ADD CONSTRAINT lut_datum_lut_mgazone_fk FOREIGN KEY (datum) REFERENCES lut_datum (datum) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE site_location ADD CONSTRAINT lut_mgazone_site_location_fk FOREIGN KEY (mga_zone) REFERENCES lut_mga_zone (mga_zone) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE site_location_visit ADD CONSTRAINT site_location_site_location_visit_fk
FOREIGN KEY (site_location_id)
REFERENCES site_location (site_location_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE site_location_point ADD CONSTRAINT site_location_site_location_point_fk
FOREIGN KEY (site_location_id)
REFERENCES site_location (site_location_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE point_intercept ADD CONSTRAINT site_location_visit_point_intercept_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE photopoints ADD CONSTRAINT site_location_visit_photopoints_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE observer ADD CONSTRAINT site_location_visit_observer_fk FOREIGN KEY (site_location_visit_id) REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_bulk_density ADD CONSTRAINT site_location_visit_soil_bulk_density_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_subsite_observations ADD CONSTRAINT site_location_visit_soil_subsite_observations_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT site_location_visit_soil_characterisation_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE structural_summary ADD CONSTRAINT site_location_visit_structural_summary_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE veg_vouchers ADD CONSTRAINT site_location_visit_veg_vouchers_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE basal_area ADD CONSTRAINT site_location_visit_basal_area_fk FOREIGN KEY (site_location_visit_id) REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE genetic_vouchers ADD CONSTRAINT site_location_visit_genetic_vouchers_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE LAI ADD CONSTRAINT site_location_visit_lai_fk FOREIGN KEY (site_location_visit_id) REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE visit_surface_soil_conditions ADD CONSTRAINT site_location_visit_visit_surface_soil_conditions_fk
FOREIGN KEY (site_location_visit_id)
REFERENCES site_location_visit (site_location_visit_id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_coarsefragsize_soil_characterisation_fk
FOREIGN KEY (coarse_frags_size)
REFERENCES lut_coarse_frag_size (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_coarsefragshape_soil_characterisation_fk
FOREIGN KEY (coarse_frags_shape)
REFERENCES lut_coarse_frag_shape (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE soil_characterisation ADD CONSTRAINT lut_coarsefragabund_soil_characterisation_fk
FOREIGN KEY (coarse_frags_abundance)
REFERENCES lut_coarse_frag_abund (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE basal_area ADD CONSTRAINT lut_basalpoint_basal_area_fk FOREIGN KEY (point_ID) REFERENCES lut_basal_point (point_ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE veg_vouchers ADD CONSTRAINT herbarium_sheet_veg_vouchers_fk FOREIGN KEY (herbarium_sheet_ID) REFERENCES herbarium_sheet (herbarium_sheet_ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE herbarium_determination ADD CONSTRAINT veg_vouchers_herbarium_determination_fk
FOREIGN KEY (veg_barcode)
REFERENCES veg_vouchers (veg_barcode) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE basal_area ADD CONSTRAINT veg_vouchers_basal_area_fk FOREIGN KEY (veg_barcode) REFERENCES veg_vouchers (veg_barcode) ON DELETE NO ACTION ON UPDATE NO ACTION;
