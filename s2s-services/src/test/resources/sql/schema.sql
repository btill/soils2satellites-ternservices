CREATE TABLE lut_landform_element (
                code VARCHAR(1024) NOT NULL,
                landform_element VARCHAR(1024),
                description VARCHAR(1024),
                CONSTRAINT lut_landform_element_pk PRIMARY KEY (code) );


CREATE TABLE lut_landform_pattern (
                id VARCHAR(1024) NOT NULL,
                landform_pattern VARCHAR(1024),
                description VARCHAR(1024),
                typical_elements VARCHAR(1024),
                common_elements VARCHAR(1024),
                occasional_elements VARCHAR(1024),
                compare_with VARCHAR(1024),
                CONSTRAINT lut_landform_pattern_pkey PRIMARY KEY (id) );


CREATE TABLE lut_surface_soil_cond (
                id VARCHAR(1024) NOT NULL,
                condition VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_surface_soil_cond_pkey PRIMARY KEY (id) );


CREATE TABLE lut_substrate (
                substrate VARCHAR(1024) NOT NULL,
                description VARCHAR(1024),
                CONSTRAINT lut_substrate_pkey PRIMARY KEY (substrate) );


CREATE TABLE lut_state (
                state VARCHAR(1024) NOT NULL,
                description VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_state_pkey PRIMARY KEY (state) );


CREATE TABLE lut_soil_tex_qual (
                id VARCHAR(1024) NOT NULL,
                qualification VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_soil_tex_qual_pkey PRIMARY KEY (id) );


CREATE TABLE lut_soil_tex_mod (
                id VARCHAR(1024) NOT NULL,
                modifier VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_soil_tex_mod_pkey PRIMARY KEY (id) );


CREATE TABLE lut_soil_tex_grade (
                code VARCHAR(1024) NOT NULL,
                name VARCHAR(1024) NOT NULL,
                bolus_description VARCHAR(1024) NOT NULL,
                clay_content VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_soil_tex_grade_pkey PRIMARY KEY (code) );


CREATE TABLE lut_soil_obs_type (
                id VARCHAR(1024) NOT NULL,
                type VARCHAR(1024) NOT NULL,
                description VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_soil_obs_type_pkey PRIMARY KEY (id) );


CREATE TABLE lut_seg_size (
                id VARCHAR(1024) NOT NULL,
                size VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_seg_size_pkey PRIMARY KEY (id) );


CREATE TABLE lut_seg_nature (
                id VARCHAR(1024) NOT NULL,
                nature VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_seg_nature_pkey PRIMARY KEY (id) );


CREATE TABLE lut_seg_form (
                id VARCHAR(1024) NOT NULL,
                form VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_seg_form_pkey PRIMARY KEY (id) );


CREATE TABLE lut_seg_abundance (
                id VARCHAR(1024) NOT NULL,
                abundance VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_seg_abundance_pkey PRIMARY KEY (id) );


CREATE TABLE lut_pedality_type (
                id VARCHAR(1024) NOT NULL,
                pedality_type VARCHAR(1024) NOT NULL,
                description VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_pedality_type_pkey PRIMARY KEY (id) );


CREATE TABLE lut_pedality_grade (
                id VARCHAR(1024) NOT NULL,
                pedality VARCHAR(1024) NOT NULL,
                grade VARCHAR(1024) NOT NULL,
                description VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_pedality_grade_pkey PRIMARY KEY (id) );


CREATE TABLE lut_pedality_fabric (
                id VARCHAR(1024) NOT NULL,
                pedality_fabric VARCHAR(1024) NOT NULL,
                description VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_pedality_fabric_pkey PRIMARY KEY (id) );


CREATE TABLE lut_mottle_size (
                id VARCHAR(1024) NOT NULL,
                size VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_mottle_size_pkey PRIMARY KEY (id) );


CREATE TABLE lut_mottle_colour (
                id VARCHAR(1024) NOT NULL,
                colour VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_mottle_colour_pkey PRIMARY KEY (id) );


CREATE TABLE lut_mottle_abund (
                id VARCHAR(1024) NOT NULL,
                abundance VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_mottle_abund_pkey PRIMARY KEY (id) );


CREATE TABLE lut_microrelief (
                id VARCHAR(1024) NOT NULL,
                type VARCHAR(1024),
                description VARCHAR(1024),
                CONSTRAINT lut_microrelief_pkey PRIMARY KEY (id) );


CREATE TABLE lut_ibra (
                bioregion_name VARCHAR(1024) NOT NULL,
                bioregion_description VARCHAR(1024),
                CONSTRAINT lut_ibra_pkey PRIMARY KEY (bioregion_name) );


CREATE TABLE lut_growth_form (
                growth_form VARCHAR(1024) NOT NULL,
                definition VARCHAR(1024),
                CONSTRAINT lut_growth_form_pkey PRIMARY KEY (growth_form) );


CREATE TABLE lut_erosion_type (
                id VARCHAR(1024) NOT NULL,
                erosion_type VARCHAR(1024),
                CONSTRAINT lut_erosion_type_pkey PRIMARY KEY (id) );


CREATE TABLE lut_erosion_state (
                id VARCHAR(1024) NOT NULL,
                state VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_erosion_state_pkey PRIMARY KEY (id) );


CREATE TABLE lut_erosion_abund (
                id VARCHAR(1024) NOT NULL,
                abundance VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_erosion_abund_pkey PRIMARY KEY (id) );


CREATE TABLE lut_effervescence (
                id VARCHAR(1024) NOT NULL,
                effervescence VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_efferv_pkey PRIMARY KEY (id) );


CREATE TABLE lut_drainage (
                id INTEGER NOT NULL,
                drainage VARCHAR(1024) NOT NULL,
                description VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_drainage_pkey PRIMARY KEY (id) );


CREATE TABLE lut_disturbance (
                id VARCHAR(1024) NOT NULL,
                disturbance VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_disturbance_pkey PRIMARY KEY (id) );


CREATE TABLE lut_datum (
                datum VARCHAR(1024) NOT NULL,
                description VARCHAR(1024),
                year_of_acceptance VARCHAR(1024),
                reference_spheroid VARCHAR(1024),
                equatorial_radius_in_m VARCHAR(1024),
                inverse_flattening VARCHAR(1024),
                description_geoscience_australia VARCHAR(1024),
                CONSTRAINT lut_datum_pkey PRIMARY KEY (datum) );


CREATE TABLE lut_mga_zone (
                mga_zone INTEGER NOT NULL,
                datum VARCHAR(1024) NOT NULL,
                projection VARCHAR(1024) NOT NULL,
                eastern_boundary VARCHAR(1024) NOT NULL,
                western_boundary VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_mga_zone_pkey PRIMARY KEY (mga_zone) );


CREATE SEQUENCE site_location_site_location_id_seq;

CREATE TABLE site_location (
                site_location_id INTEGER GENERATED BY DEFAULT AS SEQUENCE site_location_site_location_id_seq,
                site_location_name VARCHAR(1024),
                established_date TIMESTAMP,
                description VARCHAR(1024),
                bioregion_name VARCHAR(1024) NOT NULL,
                property VARCHAR(1024),
                paddock VARCHAR(1024),
                mga_zone INTEGER NOT NULL,
                plot_is_permanently_marked BOOLEAN,
                plot_is_aligned_to_grid BOOLEAN,
                landform_pattern VARCHAR(1024) NOT NULL,
                landform_element VARCHAR(1024) NOT NULL,
                site_slope INTEGER,
                site_aspect INTEGER,
                plot_is_100m_by_100m BOOLEAN,
                plot_dimensions VARCHAR(1024),
                comments VARCHAR(1024),
                app_lat VARCHAR(1024),
                app_long VARCHAR(1024),
                bool_test BOOLEAN,
                outcrop_lithology VARCHAR(1024),
                other_outcrop_lithology VARCHAR(1024),
                CONSTRAINT site_location_pkey PRIMARY KEY (site_location_id) );

CREATE SEQUENCE site_location_point_id_seq;

CREATE TABLE site_location_point (
                id INTEGER GENERATED BY DEFAULT AS SEQUENCE  site_location_point_id_seq,
                site_location_id INTEGER NOT NULL,
                point VARCHAR(1024),
                easting DOUBLE PRECISION,
                northing DOUBLE PRECISION,
                latitude VARCHAR(1024),
                longitude VARCHAR(1024),
                zone INTEGER,
                threedcq DOUBLE PRECISION,
                CONSTRAINT site_location_point_pkey PRIMARY KEY (id) );

CREATE SEQUENCE site_location_visit_site_location_visit_id_seq;

CREATE TABLE site_location_visit (
                site_location_visit_id INTEGER GENERATED BY DEFAULT AS SEQUENCE public.site_location_visit_site_location_visit_id_seq,
                site_location_id INTEGER NOT NULL,
                visit_start_date TIMESTAMP,
                visit_end_date TIMESTAMP,
                photopoints_exists BOOLEAN,
                leaf_area_index_exists BOOLEAN,
                basal_wedge_has_been_collected BOOLEAN,
                visit_notes VARCHAR(1024),
                location_description VARCHAR(1024),
                erosion_type VARCHAR(1024) NOT NULL,
                erosion_abundance VARCHAR(1024) NOT NULL,
                erosion_state VARCHAR(1024) NOT NULL,
                microrelief VARCHAR(1024) NOT NULL,
                drainage_type INTEGER NOT NULL,
                disturbance VARCHAR(1024) NOT NULL,
                climatic_condition VARCHAR(1024),
                vegetation_condition VARCHAR(1024),
                observer_veg VARCHAR(1024),
                observer_soil VARCHAR(1024),
                ok_to_publish BOOLEAN,
                A_S_C VARCHAR(1024),
                described_by VARCHAR(1024),
                surface_coarse_frags_abundance VARCHAR(1024),
                surface_coarse_frags_size VARCHAR(1024),
                surface_coarse_frags_type VARCHAR(1024),
                surface_coarse_frags_lithology VARCHAR(1024),
                pit_marker_easting DOUBLE PRECISION,
                pit_marker_northing DOUBLE PRECISION,
                pit_marker_mga_zones INTEGER,
                pit_marker_datum VARCHAR(1024),
                pit_marker_location_method VARCHAR(1024),
                soil_observation_type VARCHAR(1024) NOT NULL,
                CONSTRAINT site_location_visit_pkey PRIMARY KEY
(site_location_visit_id)
);

CREATE SEQUENCE visit_surface_soil_conditions_id_seq;

CREATE TABLE visit_surface_soil_conditions (
                ID INTEGER GENERATED BY DEFAULT AS SEQUENCE public.visit_surface_soil_conditions_id_seq,
                site_location_visit_id INTEGER NOT NULL,
                surface_soil_condition_id VARCHAR(1024) NOT NULL,
                CONSTRAINT visit_surface_soil_conditions_pkey PRIMARY KEY
(ID)
);

CREATE SEQUENCE structural_summary_id_seq;

CREATE TABLE structural_summary (
                id INTEGER GENERATED BY DEFAULT AS SEQUENCE public.structural_summary_id_seq,
                site_location_visit_id INTEGER NOT NULL,
                phenology_comment VARCHAR(1024),
                upper_1_dominant VARCHAR(1024),
                upper_2_dominant VARCHAR(1024),
                upper_3_dominant VARCHAR(1024),
                mid_1_dominant VARCHAR(1024),
                mid_2_dominant VARCHAR(1024),
                mid_3_dominant VARCHAR(1024),
                ground_1_dominant VARCHAR(1024),
                ground_2_dominant VARCHAR(1024),
                ground_3_dominant VARCHAR(1024),
                description VARCHAR(1024),
                mass_flowering_event INTEGER,
                CONSTRAINT structural_summary_pkey PRIMARY KEY (id) );


CREATE SEQUENCE soil_subsite_observations_id_seq;

CREATE TABLE soil_subsite_observations (
                id INTEGER GENERATED BY DEFAULT AS SEQUENCE public.soil_subsite_observations_id_seq,
                site_location_visit_id INTEGER NOT NULL,
                subsite_ID VARCHAR(1024),
                zone INTEGER,
                easting INTEGER,
                northing INTEGER,
                zero_to_ten_barcode VARCHAR(1024),
                ten_to_twenty_barcode VARCHAR(1024),
                twenty_to_thirty_barcode VARCHAR(1024),
                comments VARCHAR(1024),
                metagenomics_are_available BOOLEAN,
                metagenomic_barcode VARCHAR(1024),
                photo_no VARCHAR(1024),
                photo_link VARCHAR(1024),
                observer VARCHAR(1024),
                CONSTRAINT soil_subsite_observations_pkey PRIMARY KEY (id) );

CREATE SEQUENCE soil_bulk_density_id_seq;

CREATE TABLE soil_bulk_density (
                id INTEGER GENERATED BY DEFAULT AS SEQUENCE public.soil_bulk_density_id_seq,
                site_location_visit_id INTEGER NOT NULL,
                sample_ID VARCHAR(1024),
                paper_bag_weight DOUBLE PRECISION,
                oven_dried_weight_in_bag DOUBLE PRECISION,
                ring_weight DOUBLE PRECISION,
                gravel_weight DOUBLE PRECISION,
                ring_volume DOUBLE PRECISION,
                gravel_volume DOUBLE PRECISION,
                fine_earth_weight_in_bag DOUBLE PRECISION,
                fine_earth_weight DOUBLE PRECISION,
                fine_earth_volume DOUBLE PRECISION,
                fine_earth_bulk_density DOUBLE PRECISION,
                gravel_bulk_density DOUBLE PRECISION,
                CONSTRAINT soil_bulk_density_pkey PRIMARY KEY (id) );

CREATE SEQUENCE observer_id_seq;
                
CREATE TABLE observer (
                id INTEGER GENERATED BY DEFAULT AS SEQUENCE public.observer_id_seq,
                site_location_visit_id INTEGER NOT NULL,
                observer_name VARCHAR(1024),
                CONSTRAINT observer_pkey PRIMARY KEY (id) );


CREATE SEQUENCE photopoints_id_seq;

CREATE TABLE photopoints (
                id INTEGER GENERATED BY DEFAULT AS SEQUENCE public.photopoints_id_seq,
                site_location_visit_id INTEGER NOT NULL,
                completion_date_time TIMESTAMP,
                photo_storage_location VARCHAR(1024),
                photopoint_lat_1 VARCHAR(1024),
                photopoint_long_1 VARCHAR(1024),
                photopoint_lat_2 VARCHAR(1024),
                photopoint_long_2 VARCHAR(1024),
                photopoint_lat_3 VARCHAR(1024),
                photopoint_long_3 VARCHAR(1024),
                photopoint_centre_peg_lat VARCHAR(1024),
                photopoint_centre_peg_long VARCHAR(1024),
                CONSTRAINT photopoints_pkey PRIMARY KEY (id) );


CREATE SEQUENCE point_intercept_id_seq;

CREATE TABLE point_intercept (
                id INTEGER GENERATED BY DEFAULT AS SEQUENCE point_intercept_id_seq,
                site_location_visit_id INTEGER NOT NULL,
                veg_barcode VARCHAR(1024),
                method VARCHAR(1024),
                point_number DOUBLE PRECISION,
                substrate VARCHAR(1024) NOT NULL,
                in_canopy_sky BOOLEAN,
                dead BOOLEAN,
                growth_form VARCHAR(1024),
                height DOUBLE PRECISION,
                transect VARCHAR(1024),
                CONSTRAINT point_intercept_pkey PRIMARY KEY (id) );


CREATE TABLE lut_coarse_frag_size (
                id VARCHAR(1024) NOT NULL,
                size VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_coarse_frag_size_pkey PRIMARY KEY (id) );


CREATE TABLE lut_coarse_frag_shape (
                id VARCHAR(1024) NOT NULL,
                type VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_coarse_frag_shape_pkey PRIMARY KEY (id) );


CREATE TABLE lut_coarse_frag_abund (
                id VARCHAR(1024) NOT NULL,
                type VARCHAR(1024) NOT NULL,
                CONSTRAINT lut_coarse_frag_abund_pkey PRIMARY KEY (id) );


CREATE TABLE soil_characterisation (
                id INTEGER NOT NULL,
                site_location_visit_id INTEGER NOT NULL,
                layer_number VARCHAR(1024),
                upper_depth DOUBLE PRECISION,
                lower_depth DOUBLE PRECISION,
                horizon VARCHAR(1024),
                texture_grade VARCHAR(1024) NOT NULL,
                texture_qualifier VARCHAR(1024),
                texture_modifier VARCHAR(1024),
                colour_when_moist VARCHAR(1024),
                colour_when_dry VARCHAR(1024),
                mottles_colour VARCHAR(1024),
                mottles_abundance VARCHAR(1024),
                mottles_size VARCHAR(1024),
                coarse_frags_abundance VARCHAR(1024),
                coarse_frags_size VARCHAR(1024),
                coarse_frags_shape VARCHAR(1024),
                coarse_frags_lithology VARCHAR(1024),
                segregations_abundance VARCHAR(1024),
                segregations_size VARCHAR(1024),
                segregations_nature VARCHAR(1024) NOT NULL,
                segregations_form VARCHAR(1024),
                comments VARCHAR(1024),
                collected_by VARCHAR(1024),
                smallest_size_1 INTEGER,
                smallest_size_2 INTEGER,
                next_size_1 INTEGER,
                next_size_2 INTEGER,
                smallest_size_type_1 VARCHAR(1024),
                smallest_size_type_2 VARCHAR(1024),
                next_size_type_1 VARCHAR(1024),
                next_size_type_2 VARCHAR(1024),
                code VARCHAR(1024),
                pedality_fabric VARCHAR(1024) NOT NULL,
                pedality_type VARCHAR(1024),
                pedality_grade VARCHAR(1024) NOT NULL,
                ph VARCHAR(1024),
                ec VARCHAR(1024),
                effervescence VARCHAR(1024),
                CONSTRAINT soil_characterisation_pkey PRIMARY KEY (id) );

CREATE TABLE lut_basal_point (
                point_ID VARCHAR(1024),
                point_name VARCHAR(1024) NOT NULL,
                distance_from_sw_corner VARCHAR(1024) NOT NULL,
                notes VARCHAR(1024),
                CONSTRAINT lut_basal_point_pkey PRIMARY KEY (point_ID) );


CREATE SEQUENCE herbarium_sheet_herbarium_sheet_id_seq;

CREATE TABLE herbarium_sheet (
                herbarium_sheet_ID INTEGER GENERATED BY DEFAULT AS SEQUENCE herbarium_sheet_herbarium_sheet_id_seq,
                herbarium_sheet_name VARCHAR(1024),
                lodgement_date TIMESTAMP,
                return_date TIMESTAMP,
                comments VARCHAR(1024),
                state VARCHAR(1024) NOT NULL,
                follow_up_date TIMESTAMP,
                follow_up_text VARCHAR(1024),
                CONSTRAINT herbarium_sheet_pkey PRIMARY KEY
(herbarium_sheet_ID)
);


CREATE TABLE veg_vouchers (
                veg_barcode VARCHAR(1024) NOT NULL,
                site_location_visit_id INTEGER NOT NULL,
                field_name VARCHAR(1024),
                herbarium_sheet_ID INTEGER,
                CONSTRAINT veg_vouchers_pk PRIMARY KEY (veg_barcode) );


CREATE TABLE herbarium_determination (
                veg_barcode VARCHAR(1024) NOT NULL,
                herbarium_determination VARCHAR(1024),
                state_accession_number VARCHAR(1024),
                determiner VARCHAR(1024),
                determination_date TIMESTAMP,
                CONSTRAINT herbarium_determination_pkey PRIMARY KEY
(veg_barcode)
);


CREATE TABLE genetic_vouchers (
                veg_barcode VARCHAR(1024) NOT NULL,
                site_location_visit_id INTEGER NOT NULL,
                sample_for_dna VARCHAR(1024),
                sample_for_isotope VARCHAR(1024),
                tea_bag_is_synthetic VARCHAR(1024),
                primary_gen_barcode VARCHAR(1024),
                secondary_gen_barcode_1 VARCHAR(1024),
                secondary_gen_barcode_2 VARCHAR(1024),
                secondary_gen_barcode_3 VARCHAR(1024),
                secondary_gen_barcode_4 VARCHAR(1024),
                CONSTRAINT genetic_vouchers_pkey PRIMARY KEY (veg_barcode) );


CREATE SEQUENCE basal_area_id_seq;

CREATE TABLE basal_area (
                id INTEGER GENERATED BY DEFAULT AS SEQUENCE basal_area_id_seq,
                site_location_visit_id INTEGER NOT NULL,
                veg_barcode VARCHAR(1024) NOT NULL,
                point_ID VARCHAR(1024) NOT NULL,
                hits INTEGER NOT NULL,
                basal_area_factor DOUBLE PRECISION,
                basal_area DOUBLE PRECISION,
                CONSTRAINT basal_area_pkey PRIMARY KEY (ID) );


CREATE TABLE LAI (
                id INTEGER NOT NULL,
                site_location_visit_id INTEGER NOT NULL,
                completion_date_time TIMESTAMP,
                location_of_lai_data VARCHAR(1024),
                CONSTRAINT lai_pkey PRIMARY KEY (id) );
