package au.org.ecoinformatics.s2s.test.util;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.org.ecoinformatics.s2s.json.HerbariumPointInterceptCount;
import au.org.ecoinformatics.s2s.json.SamplingUnit;
import au.org.ecoinformatics.s2s.json.SearchTrait;
import au.org.ecoinformatics.s2s.util.JsonUtilsBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-test-persistence.xml"})
public class JsonConversionUtilTest {

	@Autowired
	private JsonUtilsBean jsonUtilsBean;
	
	@Test
	public void testDateJsonUtilsBean() throws Exception {
		String dateString = "\"2011-05-14\"";
		Date dateValue = this.jsonUtilsBean.readValue(dateString, Date.class);
		Assert.assertEquals(dateString, this.jsonUtilsBean.writeValueAsString(dateValue));
	}
	
	@Test
	public void testSamplingUnitJson() throws Exception {
		Assert.assertEquals("{\"description\":\"Photopoint\",\"id\":3}", this.jsonUtilsBean.writeValueAsString(SamplingUnit.PHOTOPOINT));
	}
	
	@Test
	public void testPojoJson() throws Exception {
		System.out.println(this.jsonUtilsBean.writeValueAsString(new HerbariumPointInterceptCount()));
	}
	
	@Test
	public void testArrayJson() throws Exception {
		Integer[] idArray = {1, 3, 6, 10, 66};
		Assert.assertEquals("[1,3,6,10,66]", this.jsonUtilsBean.writeValueAsString(idArray));
	}
	
	@Test
	public void testJsonSearchTrait() throws Exception {
		Assert.assertEquals("{\"id\":5,\"name\":\"Soil Texture\",\"criteria\":\"soilTexture\",\"searchDataType\":\"String\"}", this.jsonUtilsBean.writeValueAsString(SearchTrait.SOIL_TEXTURE));
	}
}
