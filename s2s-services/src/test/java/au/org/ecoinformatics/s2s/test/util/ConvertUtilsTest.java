package au.org.ecoinformatics.s2s.test.util;

import junit.framework.Assert;

import org.junit.Test;

import au.org.ecoinformatics.s2s.domain.LutEffervescence;
import au.org.ecoinformatics.s2s.domain.SoilCharacterisation;
import au.org.ecoinformatics.s2s.util.ConvertUtils;

public class ConvertUtilsTest {

	@Test
	public void testConvertUtilsGetProperty() throws Exception {
		SoilCharacterisation sc = new SoilCharacterisation();
		sc.setLutEffervescence(null);
		Assert.assertNull(ConvertUtils.getProperty(sc, "lutEffervescence.effervescence"));
		sc.setLutEffervescence(new LutEffervescence());
		Assert.assertNull(ConvertUtils.getProperty(sc, "lutEffervescence.effervescence"));
	}
}
