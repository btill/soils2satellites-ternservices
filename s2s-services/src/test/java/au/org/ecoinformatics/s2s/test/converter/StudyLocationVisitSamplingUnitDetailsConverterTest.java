package au.org.ecoinformatics.s2s.test.converter;

import java.util.Set;

import junit.framework.Assert;

import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import au.org.ecoinformatics.s2s.converter.studylocationvisit.StudyLocationVisitConverter;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.repository.SiteLocationVisitRepository;
import au.org.ecoinformatics.s2s.test.util.TestObjectUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-test-persistence.xml"})
public class StudyLocationVisitSamplingUnitDetailsConverterTest {

	@Autowired
	private SiteLocationVisitRepository siteLocationVisitRepository;
	@Autowired
	private StudyLocationVisitConverter studyLocationVisitConverter;
	
	@Test
	@Transactional
	public void testStudyLocationVisitConverter() throws Exception {
		SiteLocationVisit slv = this.siteLocationVisitRepository.findOne(7528);
		StudyLocationVisit studyLV = this.studyLocationVisitConverter.convert(slv);
		Set<String> properties = TestObjectUtils.getProperties(studyLV, "studyLocationId", "studyLocationName", "samplingUnits", "studyLocationVisitId", "observers", "visitStartDate", "visitEndDate");
		for(String property : properties) {
			Object newValue = BeanUtils.getProperty(studyLV, property);
			Object oldValue = BeanUtils.getProperty(slv, property);
			Assert.assertEquals("values not same: " + property, oldValue, newValue);
		}
		Assert.assertEquals(slv.getSiteLocation().getSiteLocationName(), studyLV.getStudyLocationName());
		Assert.assertEquals(slv.getSiteLocationVisitId(), studyLV.getStudyLocationVisitId());
		Assert.assertEquals(slv.getVisitStartDate().getTime(), studyLV.getVisitStartDate().getTime());
		Assert.assertEquals(slv.getVisitEndDate().getTime(), studyLV.getVisitEndDate().getTime());
	}
}
