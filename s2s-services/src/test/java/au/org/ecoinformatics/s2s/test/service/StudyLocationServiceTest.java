package au.org.ecoinformatics.s2s.test.service;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.org.ecoinformatics.s2s.json.StudyLocation;
import au.org.ecoinformatics.s2s.json.StudyLocationName;
import au.org.ecoinformatics.s2s.service.StudyLocationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-test-persistence.xml"})
public class StudyLocationServiceTest {

	@Autowired
	private StudyLocationService studyLocationService;
	
	@Test
	public void testGetStudyLocationNameList() throws Exception {
		List<StudyLocationName> slnList = this.studyLocationService.getStudyLocationNameList();
		Assert.assertEquals(117, slnList.size());
	}
	
	@Test
	public void testGetStudyLocations() throws Exception {
		List<StudyLocation> slList = this.studyLocationService.getStudyLocations();
		Assert.assertEquals(117, slList.size());
	}
	
	@Test
	public void testGetStudyLocation() throws Exception {
		StudyLocation sl = this.studyLocationService.getStudyLocation(10845);
		Assert.assertNotNull(sl);
	}
	
	@Test
	public void testGetStudyLocationId() throws Exception {
		Integer id = this.studyLocationService.getStudyLocationId("5846");
		Assert.assertEquals(new Integer(5846), id);
		id = this.studyLocationService.getStudyLocationId("NTAGFU0037");
		Assert.assertEquals(new Integer(10845), id);
	}
}
