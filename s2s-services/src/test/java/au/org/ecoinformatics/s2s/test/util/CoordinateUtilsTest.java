package au.org.ecoinformatics.s2s.test.util;

import org.junit.Assert;
import org.junit.Test;

import au.org.ecoinformatics.s2s.util.CoordinateUtils;


public class CoordinateUtilsTest {

	@Test
	public void testConvertLatLonMinuteDecimalSecondCoordToDecimal() {
		Double s = CoordinateUtils.convertMinuteDecimalSecondToDecimal("-22.16'59.48\"");
		Assert.assertEquals("-22.283189", s.toString()); //Expected result retrieved from online calculator
		
		Double s2 = CoordinateUtils.convertMinuteDecimalSecondToDecimal("133.14'52.00\"");
		Assert.assertEquals("133.247778", s2.toString());
		
		Double s3 = CoordinateUtils.convertMinuteDecimalSecondToDecimal("-40.25'31.22\"");
		Assert.assertEquals("-40.425339", s3.toString()); //Expected result retrieved from online calculator
		
		Double s4 = CoordinateUtils.convertMinuteDecimalSecondToDecimal("140.50'44.02\"");
		Assert.assertEquals("140.845561", s4.toString()); //Expected result retrieved from online calculator
		
		Double s5 = CoordinateUtils.convertMinuteDecimalSecondToDecimal("-34.88076551");
		Assert.assertEquals("-34.880766", s5.toString());
	}
	
	@Test
	public void testCoordinateDecimalFormat() {
		Assert.assertFalse(CoordinateUtils.isDecimalFormat("-22.16'59.48\""));
		Assert.assertTrue(CoordinateUtils.isDecimalFormat("-22.16009"));
	}
}
