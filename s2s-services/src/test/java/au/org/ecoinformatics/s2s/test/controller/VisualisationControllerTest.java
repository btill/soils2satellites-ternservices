package au.org.ecoinformatics.s2s.test.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.request.MockMvcRequestBuilders;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import au.org.ecoinformatics.s2s.web.VisualisationController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/servlet-test-context.xml"})
public class VisualisationControllerTest {
	
	@Autowired
	private VisualisationController visualisationController;
	private MockMvc mockMvc = null;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.visualisationController).build();
    }
    
    @After
    public void tearDown() {
    	this.mockMvc =  null;
    }
    
    @Test
    @Ignore
    public void testTaveiControllerGetHerbariumPointIntercepts() throws Exception {
    	ResultActions updateActions = this.mockMvc.perform(MockMvcRequestBuilders.get("/getHerbariumPointIntercepts?studyLocation=8677&visitStartDate=15072012"));
    	System.out.println("Update Output: " + updateActions.andReturn().getResponse().getContentAsString());
    }

}
