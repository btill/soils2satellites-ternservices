package au.org.ecoinformatics.s2s.test.util;

import junit.framework.Assert;

import org.junit.Test;

import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.SamplingUnit;

public class SamplingUnitTest {

	@Test
	public void testFindSamplingUnit() throws Exception {
		SamplingUnit su = SamplingUnit.valueOf(4);
		Assert.assertEquals(SamplingUnit.STRUCTURAL_SUMMARY, su);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFindSamplingUnitError() {
		SamplingUnit.valueOf(99);
	}
	
	@Test
	public void testSamplingUnitVisitor() throws Exception {
		for(SamplingUnit su : SamplingUnit.values()) {
			Assert.assertFalse(su.hasSamplingUnitData(new SiteLocationVisit()));
		}
	}
}
