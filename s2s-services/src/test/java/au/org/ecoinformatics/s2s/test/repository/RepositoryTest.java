package au.org.ecoinformatics.s2s.test.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import au.org.ecoinformatics.s2s.converter.datatype.DistinctSpeciesCountConverter;
import au.org.ecoinformatics.s2s.domain.Observer;
import au.org.ecoinformatics.s2s.domain.SiteLocation;
import au.org.ecoinformatics.s2s.domain.SiteLocationPoint;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.DistinctSpeciesCount;
import au.org.ecoinformatics.s2s.repository.HerbariumDeterminationRepository;
import au.org.ecoinformatics.s2s.repository.LutLandformElementRepository;
import au.org.ecoinformatics.s2s.repository.ObserverRepository;
import au.org.ecoinformatics.s2s.repository.SiteLocationPointRepository;
import au.org.ecoinformatics.s2s.repository.SiteLocationRepository;
import au.org.ecoinformatics.s2s.repository.SiteLocationVisitRepository;
import au.org.ecoinformatics.s2s.repository.SoilCharacterisationRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-test-persistence.xml"})
public class RepositoryTest {

	@Autowired
	private SiteLocationRepository siteLocationRepository;
	@Autowired
	private SiteLocationVisitRepository siteLocationVisitRepository;
	@Autowired
	private SiteLocationPointRepository siteLocationPointRepository;
	@Autowired
	private HerbariumDeterminationRepository herbariumDeterminationRepository;
	@Autowired
	private SoilCharacterisationRepository soilCharacterisationRepository;
	@Autowired
	private LutLandformElementRepository lutLandformElementRepository;
	@Autowired
	private ObserverRepository observerRepository;
	
	@Autowired
	private DistinctSpeciesCountConverter distinctSpeciesCountConverter;
	
	@Test
	@Transactional
	public void testSiteLocationVisitDateSorting() throws Exception {
		List<SiteLocation> slList = this.siteLocationRepository.findAll();
		for(SiteLocation sl : slList) {
			System.out.println(sl.getSiteLocationId() + " - " + sl.getSiteLocationName());
		}
		List<SiteLocationVisit> slv = this.siteLocationVisitRepository.findFirstSiteLocationVisit(10850);
		Assert.assertNotNull(slv);
		Assert.assertTrue(slv.size() == 1);
		
		List<SiteLocationVisit> slv1 = this.siteLocationVisitRepository.findLastSiteLocationVisit(10850);
		Assert.assertNotNull(slv1);
		Assert.assertTrue(slv1.size() == 1);
		
		List<SiteLocationVisit> slvList = this.siteLocationVisitRepository.findSiteLocationVisits(10850);
		Assert.assertFalse(slvList.isEmpty());
		System.out.println(slvList.size());
	}
	
	@Test
	@Transactional
	public void testObserverRepository() throws Exception {
		SiteLocationVisit slv1000 = this.siteLocationVisitRepository.findOne(7509);
		Observer o1 = new Observer();
		o1.setObserverName("test-observer");
		o1.setSiteLocationVisit(slv1000);
		Set<Observer> oSet = new HashSet<Observer>();
		oSet.add(o1);
		slv1000.setObservers(oSet);
		this.observerRepository.save(o1);
		
		SiteLocation sl = this.siteLocationRepository.findOne(slv1000.getSiteLocation().getSiteLocationId());
		List<Observer>  observers = this.observerRepository.findDistinctObservers(sl.getSiteLocationId());
		Assert.assertEquals(1, observers.size());
		Assert.assertEquals(o1, observers.get(0));
	}
	
	@Test
	public void testSiteLocationRepository() throws Exception {
		List<SiteLocation> siteLocations = this.siteLocationRepository.findAll();
		Assert.assertEquals(117, siteLocations.size());
		Assert.assertEquals(new Integer(10811), this.siteLocationRepository.findSiteLocationId("NTAGFU0002").get(0));
	} 
	
	@Test
	@Transactional
	public void testSiteLocationPointRepository() throws Exception {
		List<SiteLocationPoint> points  = this.siteLocationPointRepository.findAll();
		Assert.assertEquals(583, points.size());
		List<SiteLocationPoint> sitePoints = this.siteLocationPointRepository.findSiteLocationMarkerPoints();
		for(SiteLocationPoint marker : sitePoints) {
			System.out.println(marker.getSiteLocation().getSiteLocationId() + " - " + marker.getSiteLocation().getSiteLocationName() + " - Lat, Long " + marker.getLatitude() + ", " + marker.getLongitude());
		}
		Assert.assertEquals(57, sitePoints.size());
	}
	
	@Test
	public void testDistinctSpecies() throws Exception {	
		Assert.assertEquals(57, this.herbariumDeterminationRepository.findSiteDistinctSpecies(10812).size());
		Assert.assertEquals(0, this.herbariumDeterminationRepository.findSiteDistinctSpecies(10808).size());
		Assert.assertEquals(32, this.herbariumDeterminationRepository.findSiteDistinctSpecies(10776).size());
		Assert.assertEquals(44, this.herbariumDeterminationRepository.findSiteDistinctSpecies(10778).size());
		Assert.assertEquals(29, this.herbariumDeterminationRepository.findSiteDistinctSpecies(10777).size());
		Assert.assertEquals(50, this.herbariumDeterminationRepository.findSiteDistinctSpecies(10795).size());
	}
	
	@Test
	public void testSoilCharacterisation() throws Exception {
		Assert.assertEquals(5, this.soilCharacterisationRepository.findForSiteLocation(10774).size());
		Assert.assertEquals(0, this.soilCharacterisationRepository.findForSiteLocation(10773).size());
		Assert.assertEquals(1, this.soilCharacterisationRepository.findForSiteLocation(10796).size());
		Assert.assertEquals(4, this.soilCharacterisationRepository.findForSiteLocation(10783).size());
		Assert.assertEquals(2, this.soilCharacterisationRepository.findForSiteLocation(10775).size());
		Assert.assertEquals(3, this.soilCharacterisationRepository.findForSiteLocation(10790).size());
	}
	
	@Test
	public void testDistinctSpeciesCount() throws Exception {
		List<Integer> siteLocationIds = new ArrayList<Integer>();
		for(SiteLocation sl : this.siteLocationRepository.findAll()) {
			siteLocationIds.add(sl.getSiteLocationId());
			System.out.println("SiteLocationId: " + sl.getSiteLocationId() + " Count: " + this.herbariumDeterminationRepository.findDistinctSpeciesCount(sl.getSiteLocationId()));
		}
		List<Object[]> dscList = this.herbariumDeterminationRepository.findDistinctSpeciesCountList(siteLocationIds);
		for(Object[] tuple : dscList) {
			DistinctSpeciesCount dsc = this.distinctSpeciesCountConverter.convert(tuple);
			System.out.println(dsc.toString());
		}
	}
	
	@Test
	public void testFindLutLandFormElements() throws Exception {
		List<Integer> siteLocationIds = new ArrayList<Integer>();
		for(SiteLocation sl : this.siteLocationRepository.findAll()) {
			siteLocationIds.add(sl.getSiteLocationId());
		}
		List<Object[]> objectList = this.lutLandformElementRepository.findLutLandFormElements(siteLocationIds);
		for(Object[] tuple : objectList) {
			System.out.println(Arrays.toString(tuple));
		}
	}
	
	@Test
	public void testFindLutLandFormElementsTotals() throws Exception {
		List<Integer> siteLocationIds = new ArrayList<Integer>();
		for(SiteLocation sl : this.siteLocationRepository.findAll()) {
			siteLocationIds.add(sl.getSiteLocationId());
		}
		System.out.println(siteLocationIds.size());
		List<Object[]> objectList = this.lutLandformElementRepository.findLutLandFormElementTotals(siteLocationIds);
		for(Object[] tuple : objectList) {
			System.out.println(Arrays.toString(tuple));
		}
	}
	
	@Test
	@Transactional
	public void testFindHerbariumSiteLocationPointIntercepts() throws Exception {
		for(SiteLocationVisit slv : this.siteLocationVisitRepository.findAll()) {
			List<Object[]> objectList = this.herbariumDeterminationRepository.findHerbariumSiteLocationPointIntercepts(slv.getSiteLocation().getSiteLocationId(), slv.getVisitStartDate());
			for(Object[] tuple : objectList) {
				System.out.println(Arrays.toString(tuple));
			}
		}
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2012, 8, 22); //, 0, 0, 0);
		List<Object[]> data = this.herbariumDeterminationRepository.findHerbariumSiteLocationPointIntercepts(10859, cal.getTime());
		Assert.assertEquals(402, data.size());
	}
	
	@Test
	public void testFindHerbariumPointInterceptCount() throws Exception {
		List<Object[]> objectList = this.herbariumDeterminationRepository.findHerbariumPointInterceptCount();
		for(Object[] tuple : objectList) {
			System.out.println(Arrays.toString(tuple));
		}
	}
	
	@Test
	public void testFindHerbariumSpeciesCount() throws Exception {
		for(SiteLocationVisit slv : this.siteLocationVisitRepository.findAll()) {
			List<Object[]> objectList = this.herbariumDeterminationRepository.findHerbariumSpeciesCount(slv.getSiteLocationVisitId());
			for(Object[] tuple : objectList) {
				System.out.println(Arrays.toString(tuple));
			}
		}
	}
}
