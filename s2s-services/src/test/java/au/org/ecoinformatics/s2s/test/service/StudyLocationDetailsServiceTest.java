package au.org.ecoinformatics.s2s.test.service;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.org.ecoinformatics.s2s.json.StudyLocationDetails;
import au.org.ecoinformatics.s2s.service.StudyLocationDetailsService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-test-persistence.xml"})
public class StudyLocationDetailsServiceTest {

	@Autowired
	private StudyLocationDetailsService studyLocationDetailsService;
	
	@Test
	public void testGetStudyLocationDetails() throws Exception {
		StudyLocationDetails sld = this.studyLocationDetailsService.getStudyLocationDetails(10795);
		Assert.assertNotNull(sld);
		Assert.assertEquals(new Integer(10795), sld.getStudyLocationId());
	}
	
}
