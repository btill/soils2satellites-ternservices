package au.org.ecoinformatics.s2s.test.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.request.MockMvcRequestBuilders;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.repository.SiteLocationVisitRepository;
import au.org.ecoinformatics.s2s.search.fiql.CustomFiqlParser;
import au.org.ecoinformatics.s2s.service.SearchService;
import au.org.ecoinformatics.s2s.web.SearchController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/servlet-test-context.xml"})
public class SearchControllerTest {
	
	@Autowired
	private SearchController searchController;
	@Autowired
	private SiteLocationVisitRepository siteLocationVisitRepository;
	@Autowired
	private SearchService searchService;
	
	private CustomFiqlParser<SiteLocationVisit> fiqlParser = new CustomFiqlParser<SiteLocationVisit>(SiteLocationVisit.class);
	private MockMvc mockMvc = null;
	
	@PersistenceContext
    private EntityManager entityManager;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.searchController).build();
    }
    
    @After
    public void tearDown() {
    	this.mockMvc =  null;
    }
    
    @Test
//    @Ignore
    public void testSearchController() throws Exception {
    	ResultActions updateActions = this.mockMvc.perform(MockMvcRequestBuilders.get("/search?_s=(studyLocationName==NTA*,studyLocationName==SA*);visitDate=gt=2012-06-06"));
    	System.out.println("Update Output: " + updateActions.andReturn().getResponse().getContentAsString());
    }
    
    @Test
//  @Ignore
    public void testSearchControllerLatLong() throws Exception {
    	ResultActions updateActions = this.mockMvc.perform(MockMvcRequestBuilders.get("/search?_s=latitude=ge=-32.5"));
    	System.out.println("Update Output: " + updateActions.andReturn().getResponse().getContentAsString());
    }
    
    @Test
//    @Ignore
    public void testSearchControllerSpeciesName() throws Exception {
    	ResultActions updateActions = this.mockMvc.perform(MockMvcRequestBuilders.get("/search?_s=speciesName==S*"));
    	System.out.println("Update Output: " + updateActions.andReturn().getResponse().getContentAsString());
    }
    
    @Test
    public void testComplexQuery() throws Exception {
//    	CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
//    	CriteriaQuery<SiteLocationVisit> c = cb.createQuery(SiteLocationVisit.class);
//    	Root<SiteLocationVisit> slv = c.from(SiteLocationVisit.class);
//    	Predicate condition = cb.like(slv.get("vegVouchers.herbariumDetermination.herbariumDetermination").as(String.class), "S*");
//    	c.where(condition);
//    	TypedQuery<SiteLocationVisit> q = this.entityManager.createQuery(c);
//    	System.out.println(q.getResultList().size());
    	
    	ResultActions updateActions = this.mockMvc.perform(MockMvcRequestBuilders.get("/search?_s=speciesName==S*;visitDate=gt=2012-06-06"));
    	System.out.println("Update Output: " + updateActions.andReturn().getResponse().getContentAsString());
    }
    
    @Test
    public void testHDSLV() throws Exception {
//    	SearchCondition<SiteLocationVisit> sc = fiqlParser.parse("(vegVouchers.herbariumDetermination.herbariumDetermination==S*)");
//    	SearchConditionVisitor<SiteLocationVisit, String> visitor = new SQLPrinterVisitor<SiteLocationVisit>();
//    	visitor.visit(sc);
//    	System.out.println(visitor.getQuery());
//    	System.out.println(visitor.getQuery()
    	
    	List<StudyLocationVisit> searchResults = this.searchService.searchSiteVisits("speciesName==S*,hasMetagenomics==true,soilTexture==C*");
    	System.out.println(searchResults.size());
    	
    	String fiqlString = fiqlParser.parseToString("(vegVouchers.herbariumDetermination.herbariumDetermination==N*,siteLocation.siteLocationName==SA*);visitStartDate=gt=2012-06-06");
    	System.out.println(fiqlString);
    	List<SiteLocationVisit> slvList = this.siteLocationVisitRepository.findSiteLocationVisitsHD("S%");
    	System.out.println(slvList.size());
    }

}
