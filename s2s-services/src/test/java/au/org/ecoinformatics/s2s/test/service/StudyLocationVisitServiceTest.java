package au.org.ecoinformatics.s2s.test.service;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.org.ecoinformatics.s2s.json.StudyLocationVisit;
import au.org.ecoinformatics.s2s.service.StudyLocationVisitService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-test-persistence.xml"})
public class StudyLocationVisitServiceTest {

	@Autowired
	private StudyLocationVisitService studyLocationVisitService;
	
	@Test
	public void testGetStudyLocationVisits() throws Exception {
		List<StudyLocationVisit> slList = this.studyLocationVisitService.getStudyLocationVisits(10812);
		Assert.assertEquals(1, slList.size());
		StudyLocationVisit slv = this.studyLocationVisitService.getStudyLocationVisit(slList.get(0).getStudyLocationVisitId());
		Assert.assertEquals(slList.get(0).getStudyLocationName(), slv.getStudyLocationName());
		Assert.assertEquals(slList.get(0).getStudyLocationId(), slv.getStudyLocationId());
		Assert.assertEquals(slList.get(0).getStudyLocationVisitId(), slv.getStudyLocationVisitId());
		Assert.assertEquals(slList.get(0).getVisitEndDate(), slv.getVisitEndDate());
		Assert.assertEquals(slList.get(0).getVisitStartDate(), slv.getVisitStartDate());
		Assert.assertEquals(slList.get(0).getObservers().size(), slv.getObservers().size());
	}
	
}
