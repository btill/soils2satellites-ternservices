package au.org.ecoinformatics.s2s.test.converter;

import java.sql.Timestamp;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.org.ecoinformatics.s2s.converter.datatype.DistinctSpeciesCountConverter;
import au.org.ecoinformatics.s2s.converter.datatype.HerbariumPointInterceptConverter;
import au.org.ecoinformatics.s2s.converter.datatype.HerbariumPointInterceptCountConverter;
import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationDetailsConverter;
import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationDetailsPointConverter;
import au.org.ecoinformatics.s2s.converter.studylocation.StudyLocationPointConverter;
import au.org.ecoinformatics.s2s.converter.studylocationvisit.StudyLocationVisitConverter;
import au.org.ecoinformatics.s2s.domain.LutIbra;
import au.org.ecoinformatics.s2s.domain.LutLandformElement;
import au.org.ecoinformatics.s2s.domain.LutLandformPattern;
import au.org.ecoinformatics.s2s.domain.SiteLocation;
import au.org.ecoinformatics.s2s.domain.SiteLocationPoint;
import au.org.ecoinformatics.s2s.domain.SiteLocationVisit;
import au.org.ecoinformatics.s2s.json.DistinctSpeciesCount;
import au.org.ecoinformatics.s2s.json.HerbariumPointIntercept;
import au.org.ecoinformatics.s2s.json.HerbariumPointInterceptCount;
import au.org.ecoinformatics.s2s.json.StudyLocationDetails;
import au.org.ecoinformatics.s2s.json.StudyLocationPoint;
import au.org.ecoinformatics.s2s.json.StudyLocationVisit;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-test-persistence.xml"})
public class ConverterTest {

	@Autowired
	private StudyLocationPointConverter studyLocationPointConverter;
	@Autowired
	private StudyLocationDetailsConverter studyLocationSummaryConverter;
	@Autowired
	private StudyLocationDetailsPointConverter studyLocationSummaryPointConverter;
	@Autowired
	private DistinctSpeciesCountConverter distinctSpeciesCountConverter;
	@Autowired
	private HerbariumPointInterceptConverter herbariumPointInterceptConverter;
	@Autowired
	private HerbariumPointInterceptCountConverter herbariumPointInterceptCountConverter;
	@Autowired
	private StudyLocationVisitConverter studyLocationVisitConverter;
	
	@Test
	public void testStudyLocationPointConverter() throws Exception {
		SiteLocation siteLocation = new SiteLocation();
		siteLocation.setSiteLocationName("testlocale");	
		SiteLocationPoint point = new SiteLocationPoint();
		point.setEasting(2.0);
		point.setLatitude(-24.1280189999999983);
		point.setLongitude(133.391188999999997);
		point.setNorthing(3.0);
		point.setPoint("testpoint");
		point.setZone(9);
		point.setSiteLocation(siteLocation);
		
		Double latitude = point.getLatitude();
		Double longtitude = point.getLongitude();
		String wkt = "POINT (" + longtitude + ", " + latitude + ")";
		
		StudyLocationPoint convertedPoint = this.studyLocationPointConverter.convert(point);
		Assert.assertEquals(point.getEasting(), convertedPoint.getEasting());
		Assert.assertEquals(latitude, convertedPoint.getLatitude());
		Assert.assertEquals(longtitude, convertedPoint.getLongitude());
		Assert.assertEquals(point.getNorthing(), convertedPoint.getNorthing());
		Assert.assertEquals(point.getSiteLocation().getSiteLocationName(), convertedPoint.getStudyLocationName());
		Assert.assertEquals(point.getZone(), convertedPoint.getZone());
		Assert.assertEquals(wkt, convertedPoint.getWkt());
	}
	
	@Test
	public void testStudyLocationSummaryConverter() throws Exception {
		LutIbra ibra = new LutIbra();
		ibra.setBioregionName("testbioname");
		
		LutLandformElement element = new LutLandformElement();
		element.setLandformElement("testelement");
		LutLandformPattern pattern = new LutLandformPattern();
		pattern.setLandformPattern("testpattern");
		
		SiteLocation siteLocation = new SiteLocation();
		siteLocation.setSiteLocationName("testlocale");
		siteLocation.setLutIbra(ibra);
		siteLocation.setLutLandformElement(element);
		siteLocation.setLutLandformPattern(pattern);
		siteLocation.setSiteLocationId(9999);
		
		StudyLocationDetails details = this.studyLocationSummaryConverter.convert(siteLocation);
		Assert.assertEquals(siteLocation.getSiteLocationName(), details.getStudyLocationName());
		Assert.assertEquals(siteLocation.getLutIbra().getBioregionDescription(), details.getBioregionName());
		Assert.assertEquals(siteLocation.getLutLandformElement().getLandformElement(), details.getLandformElement());
		Assert.assertEquals(siteLocation.getLutLandformPattern().getLandformPattern(), details.getLandformPattern());
		Assert.assertEquals(siteLocation.getSiteLocationId(), details.getStudyLocationId());
	}
	
	@Test
	public void testStudyLocationSummaryPointConverter() throws Exception {
		StudyLocationPoint point = new StudyLocationPoint();
		point.setEasting(2.03);
		point.setLatitude(23.876543);
		point.setLongitude(99.123456);
		point.setNorthing(4.7);
		point.setZone(7);
		StudyLocationDetails details = this.studyLocationSummaryPointConverter.convert(point);
		Assert.assertEquals(point.getEasting(), details.getEasting());
		Assert.assertEquals(point.getLatitude(), details.getLatitude());
		Assert.assertEquals(point.getLongitude(), details.getLongitude());
		Assert.assertEquals(point.getNorthing(), details.getNorthing());
		Assert.assertEquals(point.getZone(), details.getMgaZone());
	}
	
	@Test
	public void testDistinctSpeciesCountConverter() throws Exception {
		Object[] values = new Object[]{new Integer(1092), "NTABRT0001", new Long(33)};
		DistinctSpeciesCount dsc = this.distinctSpeciesCountConverter.convert(values);
		Assert.assertEquals(values[0], dsc.getSiteLocationId());
		Assert.assertEquals(values[1], dsc.getSiteLocationName());
		Assert.assertEquals(values[2], values[2]);
		
	}
	
	@Test
	public void testHerbariumPointInterceptConverter() throws Exception {
		Object[] values = new Object[]{new Integer(234234), "new site", new Integer(88), new Date(), new Integer(577), "plant" };
		HerbariumPointIntercept hpi = this.herbariumPointInterceptConverter.convert(values);
		Assert.assertEquals((Integer) values[0], hpi.getSiteLocationId());
		Assert.assertEquals((String) values[1], hpi.getSiteLocationName());
		Assert.assertEquals((Integer) values[2], hpi.getSiteLocationVisitId());
		Assert.assertEquals((Date) values[3], hpi.getVisitStartDate());
		Assert.assertEquals((Integer) values[4], hpi.getPointInterceptId());
		Assert.assertEquals((String) values[5], hpi.getHerbariumDetermination());
	}
	
	@Test
	public void testHerbariumPointInterceptCountConverter() throws Exception {
		Object[] values = new Object[]{new Integer(234234), "new site", new Integer(88), new Long(577), "plant" };
		HerbariumPointInterceptCount hpic = this.herbariumPointInterceptCountConverter.convert(values);
		Assert.assertEquals((Integer) values[0], hpic.getSiteLocationId());
		Assert.assertEquals((String) values[1], hpic.getSiteLocationName());
		Assert.assertEquals((Integer) values[2], hpic.getSiteLocationVisitId());
		Assert.assertEquals((Long) values[3], hpic.getPointInterceptCount());
		Assert.assertEquals((String) values[4], hpic.getHerbariumDetermination());
	}
	
	@Test
	public void testStudyLocationVisitConverter() throws Exception {
		Date now = new Date();
		SiteLocation location = new SiteLocation();
		location.setSiteLocationId(222);
		location.setSiteLocationName("test-site");
		SiteLocationVisit siteVisit = new SiteLocationVisit();
		siteVisit.setSiteLocation(location);
		siteVisit.setSiteLocationVisitId(999);
		siteVisit.setVisitStartDate(new Timestamp(now.getTime()));
		siteVisit.setVisitEndDate(new Timestamp(now.getTime()));
		siteVisit.setObserverSoil("soilObserver");
		siteVisit.setObserverVeg("vegObserver");
		
		StudyLocationVisit slv = this.studyLocationVisitConverter.convert(siteVisit);
		Assert.assertEquals(location.getSiteLocationId(), slv.getStudyLocationId());
		Assert.assertEquals(siteVisit.getSiteLocationVisitId(), slv.getStudyLocationVisitId());
		Assert.assertEquals(now, slv.getVisitEndDate());
		Assert.assertEquals(now, slv.getVisitStartDate());
		Assert.assertEquals(2, slv.getObservers().size());
		Assert.assertEquals("soilObserver", slv.getObservers().get(0).getObserverName());
		Assert.assertEquals("vegObserver", slv.getObservers().get(1).getObserverName());
	}
}
