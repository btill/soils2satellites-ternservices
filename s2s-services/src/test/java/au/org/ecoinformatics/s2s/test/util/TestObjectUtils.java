package au.org.ecoinformatics.s2s.test.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;

public final class TestObjectUtils {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Set<String> getProperties(Object bean, String ... ignoreProperties) throws Exception {
		Map props = BeanUtils.describe(bean);
		Set<String> propNames = props.keySet();
		propNames.remove("class");
		for(String ignoreProperty : ignoreProperties) {
			propNames.remove(ignoreProperty);
		}
		return propNames;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T copy(T orig) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(orig);
        out.flush();
        out.close();
        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));
        return (T) in.readObject();
    }
}
