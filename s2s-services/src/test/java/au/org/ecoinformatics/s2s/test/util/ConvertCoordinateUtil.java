package au.org.ecoinformatics.s2s.test.util;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import au.org.ecoinformatics.s2s.domain.SiteLocationPoint;
import au.org.ecoinformatics.s2s.repository.SiteLocationPointRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-edit-persistence.xml"})
public class ConvertCoordinateUtil {

	@Autowired
	private SiteLocationPointRepository siteLocationPointRepository;
	
	@Test
	@Ignore
	public void testGetStudyLocationDetails() throws Exception {
		List<SiteLocationPoint> points = this.siteLocationPointRepository.findAll();
		for(SiteLocationPoint point : points) {
//			String latitude = point.getLatitude();
//			String longitude = point.getLongitude();
//			boolean latDecimal = CoordinateUtils.isDecimalFormat(latitude);
//			boolean longDecimal = CoordinateUtils.isDecimalFormat(longitude);
//			if(latDecimal && longDecimal) {
//				if(point.getLongDouble() != null && point.getLatDouble() != null) {
//					continue;
//				}
//				point.setLatDouble(Double.parseDouble(latitude));
//				point.setLongDouble(Double.parseDouble(longitude));
//				save(point);
//				continue;
//			}
//			point.setLatitude(CoordinateUtils.convertMinuteDecimalSecondToDecimal(latitude).toString());
//			point.setLongitude(CoordinateUtils.convertMinuteDecimalSecondToDecimal(longitude).toString());
			save(point);
		}
	}
	
	@Transactional
	private void save(SiteLocationPoint point) {
		this.siteLocationPointRepository.save(point);
		this.siteLocationPointRepository.flush();
	}
	
}
