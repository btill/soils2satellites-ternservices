package au.org.ecoinformatics.s2s.test.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.org.ecoinformatics.s2s.json.SamplingUnit;
import au.org.ecoinformatics.s2s.json.StudyLocationVisitSamplingUnitDetails;
import au.org.ecoinformatics.s2s.service.StudyLocationVisitDetailsService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/s2s-test-persistence.xml"})
public class StudyLocationVisitDetailsServiceTest {

	@Autowired
	private StudyLocationVisitDetailsService studyLocationVisitDetailsService;
	
	@Test
	public void testGetStudyLocationSamplingUnitDetails() throws Exception {
		for(SamplingUnit su : SamplingUnit.values()) {
			System.out.println(su.name());
			StudyLocationVisitSamplingUnitDetails details = this.studyLocationVisitDetailsService.getStudyLocationSamplingUnitDetails(7510, su);
			System.out.println(su.name() + " - " + details.getSamplingUnitData().isEmpty());
		}
		
//		StudyLocationVisit slv = this.studyLocationVisitService.getStudyLocationVisit(slList.get(0).getStudyLocationVisitId());
//		Assert.assertEquals(slList.get(0).getStudyLocationName(), slv.getStudyLocationName());
//		Assert.assertEquals(slList.get(0).getStudyLocationId(), slv.getStudyLocationId());
//		Assert.assertEquals(slList.get(0).getStudyLocationVisitId(), slv.getStudyLocationVisitId());
//		Assert.assertEquals(slList.get(0).getVisitEndDate(), slv.getVisitEndDate());
//		Assert.assertEquals(slList.get(0).getVisitStartDate(), slv.getVisitStartDate());
//		Assert.assertEquals(slList.get(0).getObservers().size(), slv.getObservers().size());
	}
	
}
